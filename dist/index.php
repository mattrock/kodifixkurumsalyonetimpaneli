<link rel="stylesheet" href="dropzone.css">
<link rel="stylesheet" href="basic.css">
<script src="dropzone.js"></script>


<form action="/file-upload"
      class="dropzone"
      id="my-awesome-dropzone"></form>
	  
	  
	  
<script>

Dropzone.options.myAwesomeDropzone = { 

    autoProcessQueue: false,
    uploadMultiple: true,
    parallelUploads: 25,
    maxFiles: 25,

    
    init: function() {
        var myDropzone = this;
         

        $("#submit-all").click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            myDropzone.processQueue();
        }); 
    }
}


</script>