<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class yonetimpaneli extends CI_Controller {


public function __construct(){
		
		parent:: __construct();{
			date_default_timezone_set('Europe/Istanbul');
			$this->security();
			$this->load->helper("security");
		}
		
	}
	
	
	function  security(){
		$kontrol = $this->session->userdata("kontrol");
		if(!isset($kontrol) || $kontrol != 1){
			redirect("yonetim");
		}
		
	}
	


public function index($id = null){
	
	$this->load->model("sql");
	$uyebilgi = $this->session->userdata("uyebilgi");
	
	$data["anketlist"] = $this->sql->anketcek();
	
	
	$data["nakliyat"] = $this->sql->nakliyatrandevutoplam();
	$data["psikomax"] = $this->sql->psikomaxtoplam();
	$data["hasare"] = $this->sql->hasaretoplam();
	$data["haliyikama"] = $this->sql->haliyikamarandevutoplam();
	$data["montaj"] = $this->sql->montajrandevutoplam();
	$data["alarm"] = $this->sql->alarmrandevutoplam();
	$data["toplammusteri"] = $this->sql->toplammusteri();
	$data["musteri"] = $this->sql->musterianalitik();
	
	$data["totalr"] = $this->sql->totalrandevu();
	$data["totalrs"] = $this->sql->toplamsigortamusterisayisi();
	$data["ankett"] = $this->sql->anketlistesitoplam();
	
	$simdikitarih = date("Y-m-d");
	$biraysonraki = date("Y-m-d",strtotime("+1 months"));
	
	
	$data["aylikrandevu"] = $this->sql->aylikrandevusayisi($simdikitarih,$biraysonraki);
	
	if($id == "alarmrandevux"){
		$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 5;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->alarmrandevutoplam1(@$uyebilgi->id);  
		$config['base_url'] = site_url()."yonetimpaneli/m/alarmrandevux"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->alarmrandevupage1($config['per_page'], $offset,@$uyebilgi->id);
		
	}
	
	if($id == "haliyikamax"){
		
		$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 5;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->haliyikamarandevutoplam1(@$uyebilgi->id);  
		$config['base_url'] = site_url()."yonetimpaneli/m/haliyikamax"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->haliyikamarandevupage1($config['per_page'], $offset,@$uyebilgi->id);
	}
	
	if($id == "hasaretrandevux"){
		
		$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 5;  
		$config['per_page'] =10;   	
		$config['total_rows'] = $this->sql->hasaretoplam1(@$uyebilgi->id);  
		$config['base_url'] = site_url()."yonetimpaneli/m/hasaretrandevux"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->hasaretrandevupage1($config['per_page'], $offset,@$uyebilgi->id);
	}
	
	if($id == "montajrandevux"){
		
		$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 5;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->montajrandevutoplam1(@$uyebilgi->id);  
		$config['base_url'] = site_url()."yonetimpaneli/m/montajrandevux"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->montajrandevupage1($config['per_page'], $offset,@$uyebilgi->id);
	}
	
	if($id == "nakliyatrandevux"){
		
		$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 5;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->nakliyatrandevutoplam1(@$uyebilgi->id);  
		$config['base_url'] = site_url()."yonetimpaneli/m/nakliyatrandevux"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->nakliyatrandevupage1($config['per_page'], $offset,@$uyebilgi->id);
	}
	
	if($id == "pskimoaxrandevux"){
		
		$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 5;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->psikomaxtoplam1(@$uyebilgi->id);  
		$config['base_url'] = site_url()."yonetimpaneli/m/pskimoaxrandevux"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;  
  
		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->psikomaxrandevupage1($config['per_page'], $offset,@$uyebilgi->id);
	}
	
	
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/index",@$data);
	$this->load->view("yonetim/includes/footer");
}





public function uyelistesi(){
	
	
	    $this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->toplamuyesayisi();  
		$config['base_url'] = site_url()."yonetimpaneli/uyelistesi"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->uyeceklistele($config['per_page'], $offset);
		 
	
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/uye_listesi",$data);
	$this->load->view("yonetim/includes/footer");
}




public function musterilistesi(){
	
	$this->load->model("sql");
	
	
	    $this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->musterlistesicek();  
		$config['base_url'] = site_url()."yonetimpaneli/musterilistesi"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["musterilistesi"] = $this->sql->musterimiz($config['per_page'], $offset);
		
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/musteri_listesi",$data);
	$this->load->view("yonetim/includes/footer");
}


public function firmalistesi(){
	
	$this->load->model("sql");
	
	
	if($_POST){
		
		$firmaismi = $this->input->post("firmaismi");
		$firmamail = $this->input->post("firmamail");
		$firmatelefon = $this->input->post("firmatelefon");
		
		$data = array(
		"firma_ismi" => $firmaismi, 
		"mail" => $firmamail,
		"telefon" => $firmatelefon
		);
		
		$check = $this->sql->sigortafirmacheck($firmamail);
	 
	 if(!empty($check)){
		 $this->session->set_flashdata('alert','<div class="alert alert-danger"> Aynı mail de zaten bir firma var.
                                     </div>');
									 redirect('yonetimpaneli/firmalistesi');
	 }
		
		$result = $this->sql->firmaekle($data);
		
		if($result){
		                             $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi !
                                     </div>');
									 redirect('yonetimpaneli/firmalistesi');
		}
		
	}
	
	$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->firmalistesicount();  
		$config['base_url'] = site_url()."yonetimpaneli/firmalistesi"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->firmalistesi($config['per_page'], $offset);
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/firmalistesi",$data);
	$this->load->view("yonetim/includes/footer");
}


public function firmaduzenle($id){
	
	$this->load->model("sql");
	
	$data["uye"] = $this->sql->idiyegorefirma($id);
	
	
	
	if($_POST){
		
		$firmaismi = $this->input->post("firmaismi");
		$firmamail = $this->input->post("firmamail");
		$firmatelefon = $this->input->post("firmatelefon");
		
		$data = array(
		"firma_ismi" => $firmaismi, 
		"mail" => $firmamail,
		"telefon" => $firmatelefon
		);
		
		$result = $this->sql->idiyegorefirmaupdate($id,$data);
		
		if($result){
		                             $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi !
                                     </div>');
									 redirect('yonetimpaneli/firmaduzenle/'.$id);
		}
		
	}
	
	
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/firmaduzenle",$data);
	$this->load->view("yonetim/includes/footer");
}


public function firmaara(){

$this->load->model("sql");

$key = $this->input->post("key");
	
	$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->firmaaracount($key);  
		$config['base_url'] = site_url()."yonetimpaneli/firmalistesi"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->firmaaralist($config['per_page'], $offset,$key);
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/firmalistesi",$data);
	$this->load->view("yonetim/includes/footer");

	
}





public function musteriarama(){
	
	$key = $this->input->post("key");
	
	$this->load->model("sql");
	
	
	$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->musteriaramacek($key);  
		$config['base_url'] = site_url()."yonetimpaneli/musteriarama"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["musterilistesi"] = $this->sql->musteriaramalist($config['per_page'], $offset,$key);
		
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/musteri_listesi",$data);
	$this->load->view("yonetim/includes/footer");
	
}

public function musteriekle(){
	
	$this->load->model("sql");
	
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	$data["iller"] = $this->sql->iller();
	
	if($_POST){
	   $musteriadi = $this->input->post("musteriadi");
	   $musterisoyadi = $this->input->post("musterisoyadi");
	   $tckimlikno = $this->input->post("tckimlikno");
	   $policeno = $this->input->post("policeno");
	   $mail = $this->input->post("mail");
	   $telefon = $this->input->post("telefon");
	   $sigortasirketi = $this->input->post("sigortasirketi");
	   $il = $this->input->post("il");
	   $ilce = $this->input->post("ilce");
	   $adres = $this->input->post("adres");
	   
	   $check = $this->sql->musterikontrol($mail,$tckimlikno);
	 
	 if(!empty($check)){
		 $this->session->set_flashdata('alert','<div class="alert alert-danger"> Aynı Tc kimlik veya mail de zaten bir kullanıcı var.
                                     </div>');
									 redirect('yonetimpaneli/musteriekle');
	 }
	   
	   $data = array(
	   "musteri_adi" => $musteriadi,
	   "musteri_soyadi" => $musterisoyadi,
	   "tc_kimlik_no" => $tckimlikno,
	   "police_no" => $policeno,
	   "sigorta_sirketi" => $sigortasirketi,
	   "telefon" => $telefon,
	   "mail" => $mail,
	   "il" => $il,
	   "ilce" => $ilce,
	   "adres" => $adres,
	   );
	   
	   $result = $this->sql->musteriekledb($data);
	   
	   if($result){
		   $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi !
                                     </div>');
									 redirect('yonetimpaneli/musteriekle');
	   }
	   
	   
	}
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/musteri_ekle",$data);
	$this->load->view("yonetim/includes/footer");
}

public function musteriduzenle($id){
	$this->load->model("sql");
	$data["musteri"] = $this->sql->musteriuyecek($id);
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	$data["iller"] = $this->sql->iller();
	
	if($_POST){
		
		$musteriadi = $this->input->post("musteriadi");
		$musterisoyadi = $this->input->post("musterisoyadi");
		$tckimlikno = $this->input->post("tckimlikno");
		$policeno = $this->input->post("policeno");
		$mail = $this->input->post("mail");
		$telefon = $this->input->post("telefon");
		$sigortasirketi = $this->input->post("sigortasirketi");
		$il = $this->input->post("il");
		$ilce = $this->input->post("ilce");
		$adres = $this->input->post("adres");
		
		
		
		
		$data = array(
	   "musteri_adi" => $musteriadi,
	   "musteri_soyadi" => $musterisoyadi,
	   "tc_kimlik_no" => $tckimlikno,
	   "police_no" => $policeno,
	   "sigorta_sirketi" => $sigortasirketi,
	   "telefon" => $telefon,
	   "mail" => $mail,
	   "il" => $il,
	   "ilce" => $ilce,
	   "adres" => $adres,
	   );
		
		
		$result = $this->sql->musteriduzenledb($id,$data);
		
		 if($result){
		   $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/musterilistesi');
	   }
		
		
	}
	
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/musteri_duzenle",$data);
	$this->load->view("yonetim/includes/footer");
	
}

public function musterisilen($id){
	$this->load->model("sql");
	$result = $this->sql->musterisilen($id);
	
	if($result){
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Silindi !
                                     </div>');
									 redirect('yonetimpaneli/musterilistesi');
	}
}

public function musterimemnuniyeti(){
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/musteri_memnuniyeti");
	$this->load->view("yonetim/includes/footer");
}

public function sigortamusterilistesi(){
	
	$this->load->model("sql");
	    
		$uyebilgi = $this->session->userdata("uyebilgi");
		
		
		
		if(@$uyebilgi->uyetipi == "sigortafirmasi"){
			$this->sql->toplamsigortamusterittt(@$uyebilgi->id);
			
			
			$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   	
		$config['total_rows'] = $this->sql->toplamsigortamusterittt($uyebilgi->id);  
		$config['base_url'] = site_url()."yonetimpaneli/sigortamusterilistesi"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->toplamsigortamusteriliste($config['per_page'], $offset,$uyebilgi->id);
			
		}
		
	    else{
	  	$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->toplamusterii();  
		$config['base_url'] = site_url()."yonetimpaneli/sigortamusterilistesi"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->musteriilistesi($config['per_page'], $offset);
	    }
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/sigorta_musteri_listesi",$data);
	$this->load->view("yonetim/includes/footer");
}

public function sigortamusteriara(){

        $this->load->model("sql");
	    $key = $this->input->post("key");
	  
	  	$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->sigortamustericount($key);  
		$config['base_url'] = site_url()."yonetimpaneli/sigortamusteriara"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->sigortamusterilist($config['per_page'], $offset,$key);
	    
	   
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/sigorta_musteri_listesi",$data);
	$this->load->view("yonetim/includes/footer");


}

public function sigortafirmasiekle(){
    $this->load->model("sql");
	
	
	if($_POST){
		
		$sigorta_firmasi  = $this->input->post("sigortasirketi");
		$mail  = $this->input->post("mail");
		$sifre  = $this->input->post("sifre");
		
		
		
		$data = array(
		"sigorta_adi" => $sigorta_firmasi,
		);
		
		$result = $this->sql->sigortasirketlistesinekle($data);
		
		
		  if($result){
			  
			$this->load->library('email');
	
			$confing =array(
			'protocol'=>'smtp',
			'smtp_host'=>"smtp.gmail.com",
			'smtp_port'=>465,
			'smtp_user'=>"infomaxassist@gmail.com",
			'smtp_pass'=>"frksnl0629.",
			'smtp_crypto'=>'ssl',              
			'mailtype'=>'html'  
			);
	
			$this->email->initialize($confing);
			$this->email->set_newline("\r\n");
			$this->email->from('infomaxassist@gmail.com');
			$this->email->to($mail);
			$this->email->subject('Maxassist Sigorta Hesabınız Başarıyla Oluşturuldu !');
			$this->email->message('<div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Tebrikler Maxassist Sigorta Yönetim Hesabınız başarıyla oluşturuldu aşağıdaki bilgiler kullanarak hesabınıza giriş yapabilirsiniz.</div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Mail Adresiniz = '.$mail.' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Şifreniz = '.$sifre.' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;">Sistemimize Giriş için <a href="http://www.maxassist.com.tr/homeguard/yonetim">buraya</a> tıklayabilirsiniz. </div>');

				if(!$this->email->send()) {
								echo "hata";
				}
				else{
					echo "Gönderildi.";
				}
					
				
			  
			  
				   $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi !
                                     </div>');
									 redirect('yonetimpaneli/sigortafirmalistesi');
			   }
		
		
	}
	
	$data["iller"] = $this->sql->iller();
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/sigorta_firmasi_ekle",$data);
	$this->load->view("yonetim/includes/footer");
	
	
	
	
	
	
}

public function randevuverme(){
	$this->load->model("sql");
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/randevu_verme");
	$this->load->view("yonetim/includes/footer");
	
}


public function sigortafirmalistesi(){
     	$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->toplamfirma();  
		$config['base_url'] = site_url()."yonetimpaneli/sigortafirmalistesi"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->firmalistele($config['per_page'], $offset);
		
		
		
  
  
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/sigorta_firmasi_listele",$data);
	$this->load->view("yonetim/includes/footer");
	

	
}

public function sigortafirmarama(){
	
	  $key = $this->input->post("key");
	
     	$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->sigortafirmaramacount($key);  
		$config['base_url'] = site_url()."yonetimpaneli/sigortafirmarama"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->sigortafirmarama($config['per_page'], $offset,$key);
		
		
		
  
  
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/sigorta_firmasi_listele",$data);
	$this->load->view("yonetim/includes/footer");
	

	
}


public function sigortamusterisiekle(){
	
    $this->load->model("sql");
   
	
	
		if($_POST){
		
		$adi = $this->input->post("adi");
		$soyadi = $this->input->post("soyadi");
		$mail = $this->input->post("mail");
		$sifre = $this->input->post("sifre");
		$sirket = $this->input->post("sirket");
		
		$check = $this->sql->sigortamustericheck($mail);
	 
	 if(!empty($check)){
		 $this->session->set_flashdata('alert','<div class="alert alert-danger"> Aynı mail de zaten bir kullanıcı var.
                                     </div>');
									 redirect('yonetimpaneli/sigortamusterilistesi');
	 }
		
	    
			  $data = array(
			  
			  "musteri_adi"         => $adi,
			  "musteri_soyadi"      => $soyadi,
			  "mail"                => $mail,
			  "sifre"               => $sifre,
			  "sigorta_sirketi"     => $sirket,
			  );
		
		 
		$result = $this->sql->sigortamusterisiekle($data);
		
		if($result){
				   $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi !
                                     </div>');
									 redirect('yonetimpaneli/sigortamusterilistesi');
		}
	
	
	
	
	
	
	
       }
	   
    $data["liste"] = $this->sql->sigortafirmalistesicek();	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/sigorta_musteri_ekle",$data);
	$this->load->view("yonetim/includes/footer");
	   
 }

public function randevulistesi($id = null){
	
	if($id == "alarmrandevux"){
		$data["linked"] = "/homeguard/yonetimpaneli/randevuarama/alarmrandevux";
		$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 4;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->alarmrandevutoplam();  
		$config['base_url'] = site_url()."yonetimpaneli/randevulistesi/alarmrandevux"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->alarmrandevupage($config['per_page'], $offset);
	}
	
	if($id == "haliyikamax"){
		$data["linked"] = "/homeguard/yonetimpaneli/randevuarama/haliyikamax";
		$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 4;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->haliyikamarandevutoplam();  
		$config['base_url'] = site_url()."yonetimpaneli/randevulistesi/haliyikamax"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->haliyikamarandevupage($config['per_page'], $offset);
	}
	
	if($id == "hasaretrandevux"){
		$data["linked"] = "/homeguard/yonetimpaneli/randevuarama/hasaretrandevux";
		$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 4;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->hasaretoplam();  
		$config['base_url'] = site_url()."yonetimpaneli/randevulistesi/hasaretrandevux"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->hasaretrandevupage($config['per_page'], $offset);
	}
	
	if($id == "montajrandevux"){
		$data["linked"] = "/homeguard/yonetimpaneli/randevuarama/montajrandevux";
		$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 4;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->montajrandevutoplam();  
		$config['base_url'] = site_url()."yonetimpaneli/randevulistesi/montajrandevux"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->montajrandevupage($config['per_page'], $offset);
	}
	
	if($id == "nakliyatrandevux"){
		$data["linked"] = "/homeguard/yonetimpaneli/randevuarama/nakliyatrandevux";
		$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 4;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->nakliyatrandevutoplam();  
		$config['base_url'] = site_url()."yonetimpaneli/randevulistesi/nakliyatrandevux"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->nakliyatrandevupage($config['per_page'], $offset);
	}
	
	if($id == "pskimoaxrandevux"){
		$data["linked"] = "/homeguard/yonetimpaneli/randevuarama/pskimoaxrandevux";
		$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 4;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->psikomaxtoplam();  
		$config['base_url'] = site_url()."yonetimpaneli/randevulistesi/pskimoaxrandevux"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;  
  
		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->psikomaxrandevupage($config['per_page'], $offset);
	}
	
	
	
	$this->load->model("sql");
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/randevu_listesi",$data);
	$this->load->view("yonetim/includes/footer");
}

 

public function randevuarama($id = null){
	
	    $key = $this->input->post("key");
	
		$this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 4;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->tumrandevulartoplamarama($key);  
		$config['base_url'] = site_url()."yonetimpaneli/randevuarama/alarmrandevux"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;  
        
		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->tumrandevuarama($config['per_page'], $offset,$key);
	    
	 
	
	$this->load->model("sql");
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/randevu_listesi",$data);
	$this->load->view("yonetim/includes/footer");
}




public function alarmrandevu(){
	$this->load->model("sql");
	$data["iller"] = $this->sql->iller();
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	$data["firmalistcek"] = $this->sql->firmalistcek();
	
	if($_POST){
		
		$id = $this->input->post("id");
		$il = $this->input->post("il");
		$ilce = $this->input->post("ilce");
		$adres = $this->input->post("adres");
		$tckimlik = $this->input->post("tckimlik");
		$police = $this->input->post("police");
		$sigortasirketi = $this->input->post("sigortasirketi");
		$mail = $this->input->post("mail");
		$tarih = $this->input->post("date");
		$musteriadi = $this->input->post("musteriadi");
		$musterisoyadi = $this->input->post("musterisoyadi");
		$tel = $this->input->post("tel");
		$saat = $this->input->post("saat");
		$firmalist = $this->input->post("firmalistesi");
		
		
		
		$date = date("d.m.Y");
		
		$data = array(
		"musteri_id" => $id,
		"il" => $il,
		"ilce" => $ilce,
		"adres" => $adres,
		"tckimlikno" => $tckimlik,
		"policeno" => $police,
		"sigortasirketi" => $sigortasirketi,
		"mail" => $mail,
		"kurulum_tarih" => $tarih,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"tel" => $tel,
		"date" => $date,
		"randevu_tipi" => "alarmrandevu",
		"kurulum_saat" => $saat,
		"firma" => $firmalist,
		);
		
		$result = $this->sql->alarmrandevuekle($data);
		
		if($result){
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi !
                                     </div>');
									 redirect('yonetimpaneli/alarmrandevu');
		}
		
	}
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/alarm_randevu",$data);
	$this->load->view("yonetim/includes/footer");
}

public function nakliyatrandevu(){
	$this->load->model("sql");
	$data["iller"] = $this->sql->iller();
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	$data["firmalistcek"] = $this->sql->firmalistcek();
	
	if($_POST){
		
		$musteriadi = $this->input->post("musteriadi");
		$musterisoyadi = $this->input->post("musterisoyadi");
		$tckimlikno = $this->input->post("tckimlikno");
		$saat = $this->input->post("saat");
		$mail = $this->input->post("mail");
		$bitistarihi = $this->input->post("bitistarihi");
		$il = $this->input->post("il");
		$ilce = $this->input->post("ilce");
		$odasayisi = $this->input->post("odasayisi");
		$evkacincikatta = $this->input->post("evkacincikatta");
		$esyanasiltasinacak = $this->input->post("esyanasiltasinacak");
		$sigortasirketi = $this->input->post("sigortasirketi");
		$adres = $this->input->post("adres");
		$ilen = $this->input->post("ilen");
		$ilcen = $this->input->post("ilcen");
		$odasayisi1 = $this->input->post("odasayisi1");
		$evkacincikatta1 = $this->input->post("evkacincikatta1");
		$esyanasiltasinacak1 = $this->input->post("esyanasiltasinacak1");
		$adres1 = $this->input->post("adres1");
		$tarihbasla = $this->input->post("date");
		$tarihbitis = $this->input->post("bitistarihi");
		$musteri_id = $this->input->post("id");
		$tel = $this->input->post("tel");
		$policeno = $this->input->post("policeno");
		$date = date("d.m.Y");
		$firmalist = $this->input->post("firmalistesi");
		
		
		$data = array(
		"musteri_id" => $musteri_id,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"tckimlikno" => $tckimlikno,
		"kurulum_tarih" => $tarihbasla,
		"kurulum_saat" => $saat,
		"il" => $il,
		"mail" => $mail,
		"ilce" => $ilce,
		"sigortasirketi" => $sigortasirketi,
		"adres" => $adres,
		"tel" => $tel,
		"policeno" => $policeno,
		"date" => $date,
		"randevu_tipi" => "nakliyatrandevu",
		"firma" => $firmalist,
		);
		
		$result = $this->sql->nakliyatrandevuekle($data);
		
		
		$data2 = array(
		"odasayisi" => $odasayisi,
		"nakliyat_id" => $result,
		"evkacincikatta" => $evkacincikatta,
		"esyanasiltasinacak" => $esyanasiltasinacak,
		"il1" => $ilen,
		"ilce1" => $ilcen,
		"odasayisi1" => $odasayisi1,
		"evkacincikatta1" => $evkacincikatta1,
		"esyanasiltasinacak1" => $esyanasiltasinacak1,
		"adres1" => $adres1,
		);
		
		$result1 = $this->sql->nakliyatdevam($data2);
		
		
		if($result){
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi !
                                     </div>');
									 redirect('yonetimpaneli/nakliyatrandevu');
		}
		
		
	}
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/nakliyat_randevu",$data);
	$this->load->view("yonetim/includes/footer");
}

public function montajrandevu(){
	
	$this->load->model("sql");
	$data["iller"] = $this->sql->iller();
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	$data["firmalistcek"] = $this->sql->firmalistcek();
	
	if($_POST){
		
		$id = $this->input->post("id");
		$musteriadi = $this->input->post("musteriadi");
		$musterisoyadi = $this->input->post("musterisoyadi");
		$il = $this->input->post("il");
		$ilce = $this->input->post("ilce");
		$tckimlik = $this->input->post("tckimlik");
		$policeno = $this->input->post("policeno");
		$mail = $this->input->post("mail");
		$kurulumtarih = $this->input->post("kurulumtarih");
		$telefon = $this->input->post("telefon");
		$sigortasirketi = $this->input->post("sigortasirketi");
		$adres = $this->input->post("adres");
		$date = date("d.m.Y");
		$firmalist = $this->input->post("firmalistesi");
		
		$data = array(
		"musteri_id" => $id,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"il" => $il,
		"ilce" => $ilce,
		"tckimlikno" => $tckimlik,
		"policeno" => $policeno,
		"mail" => $mail,
		"kurulum_tarih" => $kurulumtarih,
		"tel" => $telefon,
		"sigortasirketi" => $sigortasirketi,
		"adres" => $adres,
		"date" => $date,
		"randevu_tipi" => "montajrandevu",
		"firma" => $firmalist,
		);
		
		$result = $this->sql->montajrandevuekle($data);
		
		if($result){
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi !
                                     </div>');
									 redirect('yonetimpaneli/montajrandevu');
		}
		
	}
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/montaj_randevu",$data);
	$this->load->view("yonetim/includes/footer");
}

public function haliyikamarandevu(){
	$this->load->model("sql");
	$data["iller"] = $this->sql->iller();
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	$data["firmalistcek"] = $this->sql->firmalistcek();
	if($_POST){
		
		$id = $this->input->post("id");
		$musteriadi = $this->input->post("musteriadi");
		$musterisoyadi = $this->input->post("musterisoyadi");
		$il = $this->input->post("il");
		$ilce = $this->input->post("ilce");
		$tckimlik = $this->input->post("tckimlik");
		$policeno = $this->input->post("policeno");
		$mail = $this->input->post("mail");
		$kurulumtarih = $this->input->post("kurulumtarih");
		$kurulumsaat = $this->input->post("saat");
		$telefon = $this->input->post("telefon");
		$sigortasirketi = $this->input->post("sigortasirketi");
		$adres = $this->input->post("adres");
		$date = date("d.m.Y");
		$firmalist = $this->input->post("firmalistesi");
		
		
		$data = array(
		"musteri_id" => $id,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"il" => $il,
		"ilce" => $ilce,
		"tckimlikno" => $tckimlik,
		"policeno" => $policeno,
		"mail" => $mail,
		"kurulum_tarih" => $kurulumtarih,
		"tel" => $telefon,
		"sigortasirketi" => $sigortasirketi,
		"adres" => $adres,
		"date" => $date,
		"randevu_tipi" => "haliyikamarandevu",
		"firma" => $firmalist,
		"kurulum_saat" => $kurulumsaat,
		);
		
		$result = $this->sql->haliyikamarandevuekle($data);
		
		if($result){
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi !
                                     </div>');
									 redirect('yonetimpaneli/haliyikamarandevu');
		}
		
	}
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/hali_yikama_randevu",$data);
	$this->load->view("yonetim/includes/footer");
}

public function piskomaxrandevu(){
	$this->load->model("sql");
	$data["iller"] = $this->sql->iller();
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	$data["firmalistcek"] = $this->sql->firmalistcek();
	
	if($_POST){
		
		$id = $this->input->post("id");
		$musteriadi = $this->input->post("musteriadi");
		$musterisoyadi = $this->input->post("musterisoyadi");
		$il = $this->input->post("il");
		$ilce = $this->input->post("ilce");
		$tckimlik = $this->input->post("tckimlik");
		$policeno = $this->input->post("policeno");
		$mail = $this->input->post("mail");
		$kurulumtarih = $this->input->post("kurulumtarih");
		$telefon = $this->input->post("telefon");
		$sigortasirketi = $this->input->post("sigortasirketi");
		$adres = $this->input->post("adres");
		$date = date("d.m.Y");
		$saat = $this->input->post("saat");
		$firmalist = $this->input->post("firmalistesi");
		
		$data = array(
		"musteri_id" => $id,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"il" => $il,
		"ilce" => $ilce,
		"tckimlikno" => $tckimlik,
		"policeno" => $policeno,
		"mail" => $mail,
		"kurulum_tarih" => $kurulumtarih,
		"tel" => $telefon,
		"sigortasirketi" => $sigortasirketi,
		"adres" => $adres,
		"date" => $date,
		"randevu_tipi" => "psikomaxrandevu",
		"kurulum_saat" => $saat,
		"firma" => $firmalist,
		);
		
		$result = $this->sql->piskomaxrandevu($data);
		
		if($result){
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi !
                                     </div>');
									 redirect('yonetimpaneli/piskomaxrandevu');
		}
		
	}
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/piskomax_randevu",$data);
	$this->load->view("yonetim/includes/footer");
}

public function hasarerandevu(){
	$this->load->model("sql");
	$data["iller"] = $this->sql->iller();
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	$data["firmalistcek"] = $this->sql->firmalistcek();
	$this->load->model("sql");
	$data["iller"] = $this->sql->iller();
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	$date = date("d.m.Y");
	
	if($_POST){
		
		$id = $this->input->post("id");
		$musteriadi = $this->input->post("musteriadi");
		$musterisoyadi = $this->input->post("musterisoyadi");
		$il = $this->input->post("il");
		$ilce = $this->input->post("ilce");
		$tckimlik = $this->input->post("tckimlik");
		$policeno = $this->input->post("policeno");
		$mail = $this->input->post("mail");
		$kurulumtarih = $this->input->post("kurulumtarih");
		$telefon = $this->input->post("telefon");
		$sigortasirketi = $this->input->post("sigortasirketi");
		$adres = $this->input->post("adres");
		$saat = $this->input->post("saat");
		$firmalist = $this->input->post("firmalistesi");
		
		$data = array(
		"musteri_id" => $id,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"il" => $il,
		"ilce" => $ilce,
		"tckimlikno" => $tckimlik,
		"policeno" => $policeno,
		"mail" => $mail,
		"kurulum_tarih" => $kurulumtarih,
		"tel" => $telefon,
		"sigortasirketi" => $sigortasirketi,
		"adres" => $adres,
		"date" => $date,
		"randevu_tipi" => "hasarerandevu",
		"kurulum_saat" => $saat,
		"firma" => $firmalist,
		);
		
		$result = $this->sql->hasarerandevu($data);
		
		if($result){
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi !
                                     </div>');
									 redirect('yonetimpaneli/hasarerandevu');
		}
		
	}
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/hasare_randevu",$data);
	$this->load->view("yonetim/includes/footer");
}

public function anketlistesi(){
	
	    $this->load->library("pagination");  
		$this->load->model("sql"); 
		$this->load->helper("url"); 
		$config['uri_segment'] = 4;  
		$config['per_page'] =10;   	
		$config['total_rows'] =$this->sql->anketlistesitoplam();  
		$config['base_url'] = site_url()."yonetimpaneli/anketlistesi"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;  

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page']; 
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->sql->anketrandevupage($config['per_page'],$offset);
		
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/anket_listesi",$data);
	$this->load->view("yonetim/includes/footer");
}

public function anketekle(){
	
	$this->load->model("sql");
	$data["anketler"] = $this->sql->anketlercek();
	
	
	if($_POST){
		
		$anketsorusu = $this->input->post("anketsorusu");
		$date = date("d.m.Y");
		$data = array(
		"anketsorusu" => $anketsorusu,
		"date" => $date,
		);
		
		$result = $this->sql->anketekle($data);
		
		if($result){
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi !
                                     </div>');
									 redirect('yonetimpaneli/anketekle');
		}
		
	}
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/anket_ekle",$data);
	$this->load->view("yonetim/includes/footer");
}


public function anketsorusuekle(){
	
	$this->load->model("sql");
	
	
		
		$anketsorusu = $this->input->post("anketsorusu");
		$sorutipi = $this->input->post("sorutipi");
		$date = date("d.m.Y");
		$data = array(
		"anketsorusu" => $anketsorusu,
		"date" => $date,
		"soru_tipi" => $sorutipi,
		);
		
		$result = $this->sql->anketekle($data);
		
		if($result){
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi !
                                     </div>');
									 redirect('yonetimpaneli/anketlistesi');
		}
}


public function mailgonder(){
	
	$this->load->model("sql");
	$id = $this->input->post("anketsorusu");
	
	
	
	$mailist = trim($this->input->post("mailist"));
	
	$mailist = explode(",",$mailist);
	
	unset($mailist[0]);
	
	
	
	 
	
	
	
	
	       $this->load->library('email');
	
			$confing =array(
			'protocol'=>'smtp',
			'smtp_host'=>"smtp.gmail.com",
			'smtp_port'=>465,
			'smtp_user'=>"infomaxassist@gmail.com",
			'smtp_pass'=>"frksnl0629.",
			'smtp_crypto'=>'ssl',              
			'mailtype'=>'html'  
			);
	
			$this->email->initialize($confing);
			$this->email->set_newline("\r\n");
			$this->email->from('infomaxassist@gmail.com');
			$this->email->to($mailist);
			$this->email->subject('Maxassist : Hizmet Değerlendirmesi');
			
			
			$ust = '<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<table cellspacing="0" cellpadding="0" border="0" style="color:#333;background:#fff;padding:0;margin:0;width:100%;font:15px/1.25em "Helvetica Neue",Arial,Helvetica"> <tbody><tr width="100%"> <td valign="top" align="left" style="background:#eef0f1;font:15px/1.25em "Helvetica Neue",Arial,Helvetica"> <table style="border:none;padding:0 18px;margin:50px auto;width:500px"> <tbody> <tr width="100%" height="60"> <td valign="top" align="left" style="border-top-left-radius:4px;border-top-right-radius:4px;background:#fff url(https://ci5.googleusercontent.com/proxy/EX6LlCnBPhQ65bTTC5U1NL6rTNHBCnZ9p-zGZG5JBvcmB5SubDn_4qMuoJ-shd76zpYkmhtdzDgcSArG=s0-d-e1-ft#https://trello.com/images/gradient.png) bottom left repeat-x;padding:10px 18px;text-align:center"> <img height="auto" width="" src="http://www.maxassist.com.tr/assets/images/maxassist-logo.png" title="Trello" style="font-weight:bold;font-size:18px;color:#fff;vertical-align:top" class="CToWUd"> </td> </tr> <tr width="100%"> <td valign="top" align="left" style="background:#fff;padding:18px">
<form action="http://localhost/homeguard/yonetimpaneli/degerlendirme" method="POST">
 <h1 style="font-size:20px;margin:16px 0;color:#333;text-align:center"> Anketimize Katılarak Bizi Değerlendirin.</h1>
<p style="text-align:center"> Adınız:<input type="text" name="ad" placeholder="adınız" required> Soyadınız:<input type="text" name="soyad" placeholder="soyadınız" required></p>
<p style="text-align:center"> Mail:<input name="email" type="email" placeholder="mailiniz" required></p>
 <style>
 
 .fa-star{
 color:#ffcf03;
 }
 
 
 
 </style>

 <div style="background:#f6f7f8;border-radius:3px;"> <br>
 
 ';
 
 

 $alt = '
 <p style="font:15px/1.25em "Helvetica Neue",Arial,Helvetica;margin-bottom:0;text-align:center"> <button href="#" type="submit" style="border:none;border-radius:3px;background:#3aa54c;color:#fff;display:block;font-weight:700;font-size:16px;line-height:1.25em;margin:24px auto 6px;padding:10px 18px;text-decoration:none;width:180px" target="_blank">Gönder</button> </p>

 <br><br> </div>
</form>
 <p style="font:14px/1.25em "Helvetica Neue",Arial,Helvetica;color:#333; text-align:center;"><a href="http://www.maxassist.com.tr/" style="color:#306f9c;text-decoration:none;font-weight:bold" target="_blank">Maxassist.com.tr</a> Tüm Hakları Saklıdır.</p>

 </td>

 </tr>

 </tbody> </table> </td> </tr></tbody> </table>';
 
 $say = 0;
 foreach($id as $yaz){
	 $say++;
	 
	 $anketsorusu = $this->sql->anketsorugetir($yaz);
	 
	 if($anketsorusu->soru_tipi == 1){
		$soru = '
		<span style="text-align:center;"><input type="radio" name="vote'.$say.'" value="evet">Evet</span>
        <span style="text-align:center;"><input type="radio" name="vote'.$say.'" value="hayir">Hayır</span>
		';
	}
	else{
		$soru = '
		<span style="text-align:center;"><input type="radio" name="vote'.$say.'" value="1">1</span>
        <span style="text-align:center;"><input type="radio" name="vote'.$say.'" value="2">2</span>
        <span style="text-align:center;"><input type="radio" name="vote'.$say.'" value="3">3</span>
        <span style="text-align:center;"><input type="radio" name="vote'.$say.'" value="4">4</span>
        <span style="text-align:center;"><input type="radio" name="vote'.$say.'" value="5">5</span>
		';
	}
	
	
	
	
	$ust .= '
	<p style="text-align:center;">    
 <label style="display:block; text-align:center;">'.$say.".".$anketsorusu->anketsorusu.'<input type="text" style="display:none;" value="'.$yaz.'" name="id['.$say.']"></label>
 <br>
 
 '.$soru.'
	
  </p>
	'; 
	 
		
	}
	
	$this->email->message($ust.$alt);
 

            $this->email->set_mailtype("html");
				if(!$this->email->send()) {
								echo "hata";
				}
				else{
					$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Gönderildi !
                                     </div>');
									 redirect('yonetimpaneli/anketekle');
				}
					
				
				
	
	
}


public function degerlendirme(){
	
	$this->load->model("sql");
	
	
	$id = $this->input->post("id");
	
	$email = $this->input->post("email");
	$ad = $this->input->post("ad");
	$soyad = $this->input->post("soyad");
	
	$say = 0;
	foreach(@$id as $yaz){
		$say++;
		$anketsorusu = $this->sql->anketsorugetir($yaz);
		
	   $anketcevap = $this->input->post("vote{$say}");
		
		if($anketsorusu->soru_tipi == 1){
		
		$cevap1 =  0;
		$cevap2 =  0;
		
		if($anketcevap == "evet"){
			$cevap1 = 1;
		}
		else{
			$cevap2 = 1;
		}
		
		$data = array(
		"cevap1" => $cevap1,
		"cevap2" => $cevap2,
		"musteri_mail" => $email,
		"musteriadi" => $ad,
		"musterisoyadi" => $soyad,
		"anket_id" => $yaz,
		);
		
		$anketguncelle = $this->sql->anketguncelle($data);
	}
	else{
		
		$data = array(
		"vote" => $anketcevap,
		"musteri_mail" => $email,
		"musteriadi" => $ad,
		"musterisoyadi" => $soyad,
		"anket_id" => $yaz,
		);
		
		$anketguncelle = $this->sql->anketguncelle1($data);
	}
     
   
		
	}
	
	
	 echo "Anket Başarıyla Gönderilmiştir.";
      
	
	
	
	
	
	
	
	
	
	
	
}


public function anketsonuc($id){
	$this->load->model("sql");
	
	
	$data["sorulist"] = $this->sql->sorucek1($id);
	
	
	
	if($data["sorulist"] == null){
		$data["sorulist"] = $this->sql->sorucek2($id);
	}
	
	
	$this->load->view("yonetim/anket_sonuc",$data);
}


public function musterimailgetir(){
	$this->load->model("sql");
	
	$result = $this->sql->musterimailgetir();
	
	foreach($result as $yaz){
		echo ",".$yaz->mail;
	}
	
}



public function rapor1(){
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/rapor1");
	$this->load->view("yonetim/includes/footer");
}

public function rapor2(){
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/rapor2");
	$this->load->view("yonetim/includes/footer");
}

public function uyesil($id){
	$this->load->model("sql");
	
	$result = $this->sql->uyesil($id);
	
	if($result){
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Silindi !
                                     </div>');
									 redirect('yonetimpaneli/uyelistesi');
	}
	
}

public function sigortafirmasil($id){
	$this->load->model("sql");
	
	$result = $this->sql->sigortafirmasil($id);
	
	if($result){
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Silindi !
                                     </div>');
									 redirect('yonetimpaneli/sigortafirmalistesi');
	}
	
}

public function firmasil($id){
	$this->load->model("sql");
	
	$result = $this->sql->firmasil($id);
	
	if($result){
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Silindi !
                                     </div>');
									 redirect('yonetimpaneli/firmalistesi');
	}
	
}

public function hizlibak($id){
	$this->load->model("sql");
	$result = $this->sql->hizlibak($id);
	
	echo '
	<tr>
                                            <td>'. $result->uye_id .'</td>
                                            <td>'. $result->uye_adsoyad .'</td>
                                            <td>'. $result->uye_email .'</td>
											<td>'. $result->uye_sifre .'</td>
                                            <td>'. $result->uye_telefon .'</td>
                                            <td>'. $result->uye_rank .'</td>
                                            
                                            
                                        </tr>
	';
	
}


public function hizlibak3($id){
	$this->load->model("sql");
	$result = $this->sql->hizlibak3($id);
	
	$sigortasirketi = $this->sql->sigortafirmalistesinecek($result->sigorta_sirketi);
	
	echo '
	<tr>
                                            <td>'. $result->musteri_adi . " " . $result->musteri_soyadi .'</td>
                                            <td>'. $result->tc_kimlik_no .'</td>
											<td>'. $result->mail .'</td>
											<td>'. $result->telefon .'</td>
											<td>'. $sigortasirketi->sigorta_adi .'</td>
											<td>'. $result->il .'</td>
                                            <td>'. $result->ilce .'</td>
                                            <td>'. $result->police_no .'</td>
                                            <td>'. $result->adres .'</td>
                                            
                                            
                                            
                                        </tr>
	';
	
}

public function hizlibak1($id){
	$this->load->model("sql");
	$result = $this->sql->hizlibak1($id);
	
	$il = $this->sql->iladicek($result->il);
	$ilce = $this->sql->ilceadicek($result->ilce);
	
	echo '
	<tr>
                                            <td>'. $result->musteri_adi . " ".$result->musteri_soyadi .'</td>
                                            
                                            <td>'. $result->tc_kimlik_no .'</td>
											<td>'. $result->police_no .'</td>
                                            <td>'. $result->sigorta_sirketi .'</td>
											<td>'. $result->telefon .'</td>
											<td>'. $result->mail .'</td>
                                            <td>'. $il->isim .'</td>
                                            <td>'. $ilce->isim .'</td>
                                            <td>'. $result->adres .'</td>
                                            
                                            
                                        </tr>
	';
	
}

public function uyeekle(){
	$this->load->model("sql");
	
	
	
	
	
	
	if($_POST){
		
	 $uyeadsoyad = $this->input->post("uyeadsoyad");
	 $uyemail = $this->input->post("uyeemail");
	 $uyetelefon = $this->input->post("uyetelefon");
	 $uyerutbe = $this->input->post("uyerutbe");
	 $il = $this->input->post("il");
	 $ilce = $this->input->post("ilce");
	 $sifre = $this->input->post("sifre");
	 
	 $check = $this->sql->uyekontrol($uyemail);
	 
	 if(!empty($check)){
		 $this->session->set_flashdata('alert','<div class="alert alert-danger"> Aynı mailde zaten bir kullanıcı var.
                                     </div>');
									 redirect('yonetimpaneli/uyeekle');
	 }
	 
	 $data = array(
	 "uye_adsoyad" => $uyeadsoyad,
	 "uye_email" => $uyemail,
	 "uye_telefon" => $uyetelefon,
	 "uye_rank" => $uyerutbe,
	 "uye_sifre" => $sifre,
	 );
	 
	 $result = $this->sql->uyeekle($data);
	 
	 if($result){
		 $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi !
                                     </div>');
									 redirect('yonetimpaneli/uyeekle');
	 }
		
		
	}
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/uye_ekle");
	$this->load->view("yonetim/includes/footer");
	
}

public function uyeduzenle($id){
	$this->load->model("sql");
	
	$data["uye"] = $this->sql->hizlibak($id);
	
	
	
	
	if($_POST){
		
	 $uyeadsoyad = $this->input->post("uyeadsoyad");
	 $uyemail = $this->input->post("uyeemail");
	 $uyetelefon = $this->input->post("uyetelefon");
	 $uyerutbe = $this->input->post("uyerutbe");
	 $il = $this->input->post("il");
	 $ilce = $this->input->post("ilce");
	 $sifre = $this->input->post("sifre");
	 
	 
	 
	 $data = array(
	 "uye_adsoyad" => $uyeadsoyad,
	 "uye_email" => $uyemail,
	 "uye_telefon" => $uyetelefon,
	 "uye_rank" => $uyerutbe,
	 "uye_sifre" => $sifre,
	 "il" => $il,
	 "ilce" => $ilce
	 );
	 
	 $result = $this->sql->uyeduzenle($id,$data);
	 
	 if($result){
		 $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/uyelistesi');
	 }
		
		
	}
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/uye_duzenle",$data);
	$this->load->view("yonetim/includes/footer");
	
}

public function sigortafirmasiduzenle($id){
	$this->load->model("sql");
	
	$data["uye"] = $this->sql->hizlibak1($id);
	
	
	
	if($_POST){
		
	    $sigorta_sirketi     = $this->input->post("sigortasirketi");
		
		$data = array(
	     "sigorta_adi" => $sigorta_sirketi,
	    );
	 
	 $result = $this->sql->sigortafirmasiduzenle($id,$data);
	 
	 if($result){
		 $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/sigortafirmalistesi');
	 }
		
		
	}
	
	$data["iller"] = $this->sql->iller();
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/sigorta_firmasi_duzenle",$data);
	$this->load->view("yonetim/includes/footer");
	
}

public function sigortamusteriduzenle($id){
	$this->load->model("sql");
	
	
	
	if($_POST){
		
	    $adi = $this->input->post("adi");
		$soyadi = $this->input->post("soyadi");
		$mail = $this->input->post("mail");
		$sifre = $this->input->post("sifre");
		$sirket = $this->input->post("sirket");
		
	    
			  $data = array(
			  "musteri_adi"         => $adi,
			  "musteri_soyadi"      => $soyadi,
			  "mail"                => $mail,
			  "sifre"               => $sifre,
			  "sigorta_sirketi"     => $sirket,
			  );
		
	 
	 $result = $this->sql->sigortamusteriduzenle($id,$data);
	 
	 if($result){
		 $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/sigortamusterilistesi');
	 }
		
		
	}
	
	$data["iller"] = $this->sql->iller();
	$data["uye"] = $this->sql->sigortamusterisicek($id);
	
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/sigorta_musteri_duzenle",$data);
	$this->load->view("yonetim/includes/footer");
	
}

public function cikis(){
	$this->session->unset_userdata("kontrol");
	$this->session->unset_userdata("uyebilgi");
	redirect('yonetim');
}


public function ilce($il){
	$this->load->model("sql");
	$result = $this->sql->ilce($il);
	if($result){
		foreach($result as $yaz){
			echo '<option value="'.$yaz->ilce_no.'">'.$yaz->isim.'</option>';
		}
	}
}


public function sigortamusterisil($id){
	$this->load->model("sql");
	
	$result = $this->sql->sigortamusterisil($id);
	
	if($result){
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Silindi !
                                     </div>');
									 redirect('yonetimpaneli/sigortamusterilistesi');
	}
	
}

public function alarmrandevuara($id){
	
	$this->load->model("sql");
	$result = $this->sql->alarmrandevuara($id);
	
	$sigortasirketi = $this->sql->sigortafirmalistesinecek($result->sigorta_sirketi);
	
	echo $result->id .",";
	echo $result->il .",";
	echo $result->ilce .",";
	echo $result->adres .",";
	echo $result->tc_kimlik_no .",";
	echo $result->police_no .",";
	echo $sigortasirketi->sigorta_adi .",";
	echo $result->mail .",";
	echo $result->sigorta_sirketi .",";
	echo $result->musteri_adi .",";
	echo $result->musteri_soyadi .",";
	echo $result->telefon ."";
	
}


public function nakliyatara($id){
	
	$this->load->model("sql");
	$result = $this->sql->alarmrandevuara($id);
	
	$sigortasirketi = $this->sql->sigortafirmalistesinecek($result->sigorta_sirketi);
	
	echo $result->id .",";
	echo $result->musteri_adi .",";
	echo $result->musteri_soyadi .",";
	echo $result->tc_kimlik_no .",";
	echo $result->police_no .",";
	echo $sigortasirketi->sigorta_adi .",";
	echo $result->sigorta_sirketi .",";
	echo $result->police_no .",";
	echo $result->mail .",";
	echo $result->il .",";
	echo $result->ilce .",";
	echo $result->adres .",";
	echo $result->telefon .",";
	
}

public function montajrandevuara($id){
	
	$this->load->model("sql");
	$result = $this->sql->alarmrandevuara($id);
	
	$sigortasirketi = $this->sql->sigortafirmalistesinecek($result->sigorta_sirketi);
	
	echo $result->id .",";
	echo $result->musteri_adi .",";
	echo $result->musteri_soyadi .",";
	echo $result->il .",";
	echo $result->ilce .",";
	echo $result->tc_kimlik_no .",";
	echo $result->police_no .",";
	echo $result->mail .",";
	echo $result->telefon .",";
	echo $result->adres .",";
	echo $result->sigorta_sirketi .",";
	echo $sigortasirketi->sigorta_adi;
	
	
	
}

public function haliyikamaara($id){
	
	$this->load->model("sql");
	$result = $this->sql->alarmrandevuara($id);
	
	$sigortasirketi = $this->sql->sigortafirmalistesinecek($result->sigorta_sirketi);
	
	echo $result->id .",";
	echo $result->musteri_adi .",";
	echo $result->musteri_soyadi .",";
	echo $result->il .",";
	echo $result->ilce .",";
	echo $result->tc_kimlik_no .",";
	echo $result->police_no .",";
	echo $result->mail .",";
	echo $result->telefon .",";
	echo $result->adres .",";
	echo $result->sigorta_sirketi .",";
	echo $sigortasirketi->sigorta_adi;
	
	
	
}

public function piskomaxrandevuara($id){
	
	$this->load->model("sql");
	$result = $this->sql->alarmrandevuara($id);
	
	$sigortasirketi = $this->sql->sigortafirmalistesinecek($result->sigorta_sirketi);
	
	echo $result->id .",";
	echo $result->musteri_adi .",";
	echo $result->musteri_soyadi .",";
	echo $result->il .",";
	echo $result->ilce .",";
	echo $result->tc_kimlik_no .",";
	echo $result->police_no .",";
	echo $result->mail .",";
	echo $result->telefon .",";
	echo $result->adres .",";
	echo $result->sigorta_sirketi .",";
	echo $sigortasirketi->sigorta_adi;
	
	
	
}

public function hasararandevuara($id){
	
	$this->load->model("sql");
	$result = $this->sql->alarmrandevuara($id);
	
	$sigortasirketi = $this->sql->sigortafirmalistesinecek($result->sigorta_sirketi);
	
	echo $result->id .",";
	echo $result->musteri_adi .",";
	echo $result->musteri_soyadi .",";
	echo $result->il .",";
	echo $result->ilce .",";
	echo $result->tc_kimlik_no .",";
	echo $result->police_no .",";
	echo $result->mail .",";
	echo $result->telefon .",";
	echo $result->adres .",";
	echo $result->sigorta_sirketi .",";
	echo $sigortasirketi->sigorta_adi;
	
	
	
}

public function alarmbak($id){
	$this->load->model("sql");
	$result = $this->sql->alarmbak($id);
	$result1 = $this->sql->sigortafirmalistesinecek($result->sigortasirketi);
	
	echo '
	<tr>                                    <th>Müşteri Adı</th>
                                            
                                            <td>'. $result->musteriadi .'</td>
                                            
                                            
                                            
                                        </tr>
										
										<tr>
										    <th>Müşteri Soyadi</th>
                                            
                                            <td>'. $result->musterisoyadi .'</td>
                                            
                                        </tr>
										
										
										
										<tr>
										    <th>Tarih</th>
                                            
                                            <td>'. $result->kurulum_tarih .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Sigorta Adı</th>
                                            
                                            <td>'. $result1->sigorta_adi .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Poliçe No</th>
                                            
											<td>'. $result->policeno .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Tc Kimlik No</th>
                                            
											<td>'. $result->tckimlikno .'</td>
											
                                        </tr>
										
										<tr>
										    <th>il</th>
                                            <td>'. $result->il .'</td>
											
                                        </tr>
										
										<tr>
										    <th>İlçe</th>
											<td>'. $result->ilce .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Mail</th>
                                            <td>'. $result->mail .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Tel</th>
                                            
                                            <td>'. $result->tel .'</td>
                                        </tr>
										
										<tr>
										    <th>Adres</th>
                                            
                                            <td>'. $result->adres .'</td>
											
                                        </tr>
										
										
	';
	
}

public function haliyikamabak($id){
	$this->load->model("sql");
	$result = $this->sql->haliyikamabak($id);
	$result1 = $this->sql->sigortafirmalistesinecek($result->sigortasirketi);
	
	echo '
	<tr>                                    <th>Müşteri Adı</th>
                                            
                                            <td>'. $result->musteriadi .'</td>
                                            
                                            
                                            
                                        </tr>
										
										<tr>
										    <th>Müşteri Soyadi</th>
                                            
                                            <td>'. $result->musterisoyadi .'</td>
                                            
                                        </tr>
										
										
										
										<tr>
										    <th>Tarih</th>
                                            
                                            <td>'. $result->kurulum_tarih .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Sigorta Adı</th>
                                            
                                            <td>'. $result1->sigorta_adi .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Poliçe No</th>
                                            
											<td>'. $result->policeno .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Tc Kimlik No</th>
                                            
											<td>'. $result->tckimlikno .'</td>
											
                                        </tr>
										
										<tr>
										    <th>il</th>
                                            <td>'. $result->il .'</td>
											
                                        </tr>
										
										<tr>
										    <th>İlçe</th>
											<td>'. $result->ilce .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Mail</th>
                                            <td>'. $result->mail .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Tel</th>
                                            
                                            <td>'. $result->tel .'</td>
                                        </tr>
										
										<tr>
										    <th>Adres</th>
                                            
                                            <td>'. $result->adres .'</td>
											
                                        </tr>
										
										
	';
	
}

public function psikomaxrandevubak($id){
	$this->load->model("sql");
	$result = $this->sql->psikomaxrandevubak($id);
	$result1 = $this->sql->sigortafirmalistesinecek($result->sigortasirketi);
	
	echo '
	<tr>                                    <th>Müşteri Adı</th>
                                            
                                            <td>'. $result->musteriadi .'</td>
                                            
                                            
                                            
                                        </tr>
										
										<tr>
										    <th>Müşteri Soyadi</th>
                                            
                                            <td>'. $result->musterisoyadi .'</td>
                                            
                                        </tr>
										
										
										
										<tr>
										    <th>Tarih</th>
                                            
                                            <td>'. $result->kurulum_tarih .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Sigorta Adı</th>
                                            
                                            <td>'. $result1->sigorta_adi .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Poliçe No</th>
                                            
											<td>'. $result->policeno .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Tc Kimlik No</th>
                                            
											<td>'. $result->tckimlikno .'</td>
											
                                        </tr>
										
										<tr>
										    <th>il</th>
                                            <td>'. $result->il .'</td>
											
                                        </tr>
										
										<tr>
										    <th>İlçe</th>
											<td>'. $result->ilce .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Mail</th>
                                            <td>'. $result->mail .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Tel</th>
                                            
                                            <td>'. $result->tel .'</td>
                                        </tr>
										
										<tr>
										    <th>Adres</th>
                                            
                                            <td>'. $result->adres .'</td>
											
                                        </tr>
										
										
	';
	
}

public function hasaretrandevubak($id){
	$this->load->model("sql");
	$result = $this->sql->hasaretbak($id);
	$result1 = $this->sql->sigortafirmalistesinecek($result->sigortasirketi);
	
	echo '
	<tr>                                    <th>Müşteri Adı</th>
                                            
                                            <td>'. $result->musteriadi .'</td>
                                            
                                            
                                            
                                        </tr>
										
										<tr>
										    <th>Müşteri Soyadi</th>
                                            
                                            <td>'. $result->musterisoyadi .'</td>
                                            
                                        </tr>
										
										
										
										<tr>
										    <th>Tarih</th>
                                            
                                            <td>'. $result->kurulum_tarih .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Sigorta Adı</th>
                                            
                                            <td>'. $result1->sigorta_adi .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Poliçe No</th>
                                            
											<td>'. $result->policeno .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Tc Kimlik No</th>
                                            
											<td>'. $result->tckimlikno .'</td>
											
                                        </tr>
										
										<tr>
										    <th>il</th>
                                            <td>'. $result->il .'</td>
											
                                        </tr>
										
										<tr>
										    <th>İlçe</th>
											<td>'. $result->ilce .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Mail</th>
                                            <td>'. $result->mail .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Tel</th>
                                            
                                            <td>'. $result->tel .'</td>
                                        </tr>
										
										<tr>
										    <th>Adres</th>
                                            
                                            <td>'. $result->adres .'</td>
											
                                        </tr>
										
										
	';
	
}

public function montajrandevubak($id){
	$this->load->model("sql");
	$result = $this->sql->montajrandevubak($id);
	$result1 = $this->sql->sigortafirmalistesinecek($result->sigortasirketi);
	
	echo '
	<tr>                                    <th>Müşteri Adı</th>
                                            
                                            <td>'. $result->musteriadi .'</td>
                                            
                                            
                                            
                                        </tr>
										
										<tr>
										    <th>Müşteri Soyadi</th>
                                            
                                            <td>'. $result->musterisoyadi .'</td>
                                            
                                        </tr>
										
										
										
										<tr>
										    <th>Tarih</th>
                                            
                                            <td>'. $result->kurulum_tarih .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Sigorta Adı</th>
                                            
                                            <td>'. $result1->sigorta_adi .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Poliçe No</th>
                                            
											<td>'. $result->policeno .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Tc Kimlik No</th>
                                            
											<td>'. $result->tckimlikno .'</td>
											
                                        </tr>
										
										<tr>
										    <th>il</th>
                                            <td>'. $result->il .'</td>
											
                                        </tr>
										
										<tr>
										    <th>İlçe</th>
											<td>'. $result->ilce .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Mail</th>
                                            <td>'. $result->mail .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Tel</th>
                                            
                                            <td>'. $result->tel .'</td>
                                        </tr>
										
										<tr>
										    <th>Adres</th>
                                            
                                            <td>'. $result->adres .'</td>
											
                                        </tr>
										
										
	';
	
}

public function nakliyatrandevubak($id){
	$this->load->model("sql");
	$result = $this->sql->nakliyatbak($id);
	$result2 = $this->sql->nakliyatbak1($id);
	$result1 = $this->sql->sigortafirmalistesinecek($result->sigortasirketi);
	
	echo '
	<tr>                                    <th>Müşteri Adı</th>
                                            
                                            <td>'. $result->musteriadi .'</td>
                                            
                                            
                                            
                                        </tr>
										
										<tr>
										    <th>Müşteri Soyadi</th>
                                            
                                            <td>'. $result->musterisoyadi .'</td>
                                            
                                        </tr>
										
										
										
										<tr>
										    <th>Başlangıç Tarihi</th>
                                            
                                            <td>'. $result->kurulum_tarih .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Bitiş Tarihi</th>
                                            
                                            <td>'. $result->kurulum_saat .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Sigorta Adı</th>
                                            
                                            <td>'. $result1->sigorta_adi .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Poliçe No</th>
                                            
											<td>'. $result->policeno .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Tc Kimlik No</th>
                                            
											<td>'. $result->tckimlikno .'</td>
											
                                        </tr>
										
										<tr>
										    <th>il</th>
                                            <td>'. $result->il .'</td>
											
                                        </tr>
										
										<tr>
										    <th>İlçe</th>
											<td>'. $result->ilce .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Mail</th>
                                            <td>'. $result->mail .'</td>
                                            
                                        </tr>
										
										<tr>
										    <th>Tel</th>
                                            
                                            <td>'. $result->tel .'</td>
                                        </tr>
										
										<tr>
										    <th>Adres</th>
                                            
                                            <td>'. $result->adres .'</td>
											
                                        </tr>
										<tr>
										    <th>Kurum Yapılacak İl</th>
                                            
                                            <td>'. $result2->il1 .'</td>
											
                                        </tr>
										<tr>
										    <th>Kurum Yapılacak İlçe</th>
                                            
                                            <td>'. $result2->ilce1 .'</td>
											
                                        </tr>
										<tr>
										    <th>Oda Sayısı</th>
                                            
                                            <td>'. $result2->odasayisi1 .'</td>
											
                                        </tr>
										<tr>
										    <th>Ev Kaçıncı Katta</th>
                                            
                                            <td>'. $result2->evkacincikatta1 .'</td>
											
                                        </tr>
										<tr>
										    <th>Eşya Nasıl Taşınacak</th>
                                            
                                            <td>'. $result2->esyanasiltasinacak1 .'</td>
											
                                        </tr>
										<tr>
										    <th>Adres</th>
                                            
                                            <td>'. $result2->adres1.'</td>
											
                                        </tr>
										
										
										
	';
	
}

public function alarmrandevusil($id){
	$this->load->model("sql");
	
	$result = $this->sql->alarmrandevusil($id);
	
	if($result){
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Silindi !
                                     </div>');
									 redirect('yonetimpaneli/randevulistesi/alarmrandevux');
	}
	
}

public function nakliyatrandevusil($id){
	$this->load->model("sql");
	
	$result = $this->sql->nakliyatrandevusil($id);
	
	if($result){
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Silindi !
                                     </div>');
									 redirect('yonetimpaneli/randevulistesi/nakliyatrandevux');
	}
	
}

public function haliyikamarandevusil($id){
	$this->load->model("sql");
	
	$result = $this->sql->haliyikamarandevusil($id);
	
	if($result){
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Silindi !
                                     </div>');
									 redirect('yonetimpaneli/randevulistesi/haliyikamax');
	}
	
}

public function psikomaxrandevusil($id){
	$this->load->model("sql");
	
	$result = $this->sql->psikomaxrandevusil($id);
	
	if($result){
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Silindi !
                                     </div>');
									 redirect('yonetimpaneli/randevulistesi/pskimoaxrandevux');
	}
	
}

public function hasaretrandevusil($id){
	$this->load->model("sql");
	
	$result = $this->sql->hasaretrandevusil($id);
	
	if($result){
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Silindi !
                                     </div>');
									 redirect('yonetimpaneli/randevulistesi/hasaretrandevux');
	}
	
}

public function montajrandevusil($id){
	$this->load->model("sql");
	
	$result = $this->sql->montajrandevusil($id);
	
	if($result){
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Silindi !
                                     </div>');
									 redirect('yonetimpaneli/randevulistesi/montajrandevux');
	}
	
}

public function onayrandevualarm($id){
	
	$this->load->model("sql");
	
	
	$result = $this->sql->onayrandevualarm($id);
	
	$mailing = $this->sql->alarmgetir($id);
	$sigortasirketi = $this->sql->sigortafirmalistesinecek($mailing->sigortasirketi);
	
	if($result){
		
		
		    $this->load->library('email');
	
			$confing =array(
			'protocol'=>'smtp',
			'smtp_host'=>"smtp.gmail.com",
			'smtp_port'=>465,
			'smtp_user'=>"infomaxassist@gmail.com",
			'smtp_pass'=>"frksnl0629.",
			'smtp_crypto'=>'ssl',              
			'mailtype'=>'html'  
			);
	
			$this->email->initialize($confing);
			$this->email->set_newline("\r\n");
			$this->email->from('infomaxassist@gmail.com');
			$this->email->to($mailing->mail);
			$this->email->subject('Maxassist Alarm Randevunuz Başarıyla Onaylandı!');
			$this->email->message('<div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;">Maxassist Alarm Randevunuz Onaylanmıştır. Randevu Bilgileriniz : </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Randevu Alınan Ad Soyad = '.$mailing->musteriadi . " " .$mailing->musterisoyadi .' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Randevu Alınan Sigorta Şirketi = '.$sigortasirketi->sigorta_adi.' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;">Randevu Tarihi = '. $mailing->kurulum_tarih .'</div>');

				if(!$this->email->send()) {
								echo "hata";
				}
				else{
					echo "Gönderildi.";
				}
		
		
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Onaylandı !
                                     </div>');
									 redirect('yonetimpaneli/randevulistesi/alarmrandevux');
	}
	
}


public function onayrandevuhaliyikama($id){
	
	$this->load->model("sql");
	
	
	$result = $this->sql->onayrandevuhaliyikama($id);
	
	$mailing = $this->sql->haligetir($id);
	$sigortasirketi = $this->sql->sigortafirmalistesinecek($mailing->sigortasirketi);
	
	
	
	if($result){
		
		
		    $this->load->library('email');
	
			$confing =array(
			'protocol'=>'smtp',
			'smtp_host'=>"smtp.gmail.com",
			'smtp_port'=>465,
			'smtp_user'=>"infomaxassist@gmail.com",
			'smtp_pass'=>"frksnl0629.",
			'smtp_crypto'=>'ssl',              
			'mailtype'=>'html'  
			);
	
			$this->email->initialize($confing);
			$this->email->set_newline("\r\n");
			$this->email->from('infomaxassist@gmail.com');
			$this->email->to($mailing->mail);
			$this->email->subject('Maxassist Halı Yıkama Randevunuz Başarıyla Onaylandı!');
			$this->email->message('<div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;">Maxassist Halı Yıkama Randevunuz Onaylanmıştır. Randevu Bilgileriniz : </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Randevu Alınan Ad Soyad = '.$mailing->musteriadi . " " .$mailing->musterisoyadi .' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Randevu Alınan Sigorta Şirketi = '.$sigortasirketi->sigorta_adi.' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;">Randevu Tarihi = '. $mailing->kurulum_tarih .'</div>');

				if(!$this->email->send()) {
								echo "hata";
				}
				else{
					echo "Gönderildi.";
				}
		
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Onaylandı !
                                     </div>');
									 redirect('yonetimpaneli/randevulistesi/haliyikamax');
	}
	
}

public function onayrandevuhasaret($id){
	
	$this->load->model("sql");
	
	
	$result = $this->sql->hasaretrandevu($id);
	
	$mailing = $this->sql->hasaretgetir($id);
	$sigortasirketi = $this->sql->sigortafirmalistesinecek($mailing->sigortasirketi);
	
	
	
	
	if($result){
		
		
		$this->load->library('email');
	
			$confing =array(
			'protocol'=>'smtp',
			'smtp_host'=>"smtp.gmail.com",
			'smtp_port'=>465,
			'smtp_user'=>"infomaxassist@gmail.com",
			'smtp_pass'=>"frksnl0629.",
			'smtp_crypto'=>'ssl',              
			'mailtype'=>'html'  
			);
	
			$this->email->initialize($confing);
			$this->email->set_newline("\r\n");
			$this->email->from('infomaxassist@gmail.com');
			$this->email->to($mailing->mail);
			$this->email->subject('Maxassist Haşare Randevunuz Başarıyla Onaylandı!');
			$this->email->message('<div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;">Maxassist Haşare Randevunuz Onaylanmıştır. Randevu Bilgileriniz : </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Randevu Alınan Ad Soyad = '.$mailing->musteriadi . " " .$mailing->musterisoyadi .' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Randevu Alınan Sigorta Şirketi = '.$sigortasirketi->sigorta_adi.' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;">Randevu Tarihi = '. $mailing->kurulum_tarih .'</div>');

				if(!$this->email->send()) {
								echo "hata";
				}
				else{
					echo "Gönderildi.";
				}
		
		
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Onaylandı !
                                     </div>');
									 redirect('yonetimpaneli/randevulistesi/hasaretrandevux');
	}
	
}

public function onaymontajrandevu($id){
	
	$this->load->model("sql");
	
	
	$result = $this->sql->onaymontajrandevu($id);
	
	$mailing = $this->sql->montajgetir($id);
	$sigortasirketi = $this->sql->sigortafirmalistesinecek($mailing->sigortasirketi);
	
	
	if($result){
		
		    $this->load->library('email');
	
			$confing =array(
			'protocol'=>'smtp',
			'smtp_host'=>"smtp.gmail.com",
			'smtp_port'=>465,
			'smtp_user'=>"infomaxassist@gmail.com",
			'smtp_pass'=>"frksnl0629.",
			'smtp_crypto'=>'ssl',              
			'mailtype'=>'html'  
			);
	
			$this->email->initialize($confing);
			$this->email->set_newline("\r\n");
			$this->email->from('infomaxassist@gmail.com');
			$this->email->to($mailing->mail);
			$this->email->subject('Maxassist Montaj Randevunuz Başarıyla Onaylandı!');
			$this->email->message('<div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;">Maxassist Montaj Randevunuz Onaylanmıştır. Randevu Bilgileriniz : </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Randevu Alınan Ad Soyad = '.$mailing->musteriadi . " " .$mailing->musterisoyadi .' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Randevu Alınan Sigorta Şirketi = '.$sigortasirketi->sigorta_adi.' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;">Randevu Tarihi = '. $mailing->kurulum_tarih .'</div>');

				if(!$this->email->send()) {
								echo "hata";
				}
				else{
					echo "Gönderildi.";
				}
		
		
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Onaylandı !
                                     </div>');
									 redirect('yonetimpaneli/randevulistesi/montajrandevux');
	}
	
}

public function onaynakliyatrandevu($id){
	
	$this->load->model("sql");
	
	
	$result = $this->sql->onaynakliyatrandevu($id);
	
	$mailing = $this->sql->nakliyatgetir($id);
	$sigortasirketi = $this->sql->sigortafirmalistesinecek($mailing->sigortasirketi);
	
	
	
	if($result){
		
		    $this->load->library('email');
	
			$confing =array(
			'protocol'=>'smtp',
			'smtp_host'=>"smtp.gmail.com",
			'smtp_port'=>465,
			'smtp_user'=>"infomaxassist@gmail.com",
			'smtp_pass'=>"frksnl0629.",
			'smtp_crypto'=>'ssl',              
			'mailtype'=>'html'  
			);
	
			$this->email->initialize($confing);
			$this->email->set_newline("\r\n");
			$this->email->from('infomaxassist@gmail.com');
			$this->email->to($mailing->mail);
			$this->email->subject('Maxassist Nakliyat Randevunuz Başarıyla Onaylandı!');
			$this->email->message('<div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;">Maxassist Nakliyat Randevunuz Onaylanmıştır. Randevu Bilgileriniz : </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Randevu Alınan Ad Soyad = '.$mailing->musteriadi . " " .$mailing->musterisoyadi .' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Randevu Alınan Sigorta Şirketi = '.$sigortasirketi->sigorta_adi.' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;">Randevu Tarihi = '. $mailing->kurulum_tarih .'</div>');

				if(!$this->email->send()) {
								echo "hata";
				}
				else{
					echo "Gönderildi.";
				}
		
		
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Onaylandı !
                                     </div>');
									 redirect('yonetimpaneli/randevulistesi/nakliyatrandevux');
	}
	
}

public function onaypsikomaxrandevu($id){
	
	$this->load->model("sql");
	
	
	$result = $this->sql->onaypsikomaxrandevu($id);
	
	$mailing = $this->sql->psikomaxgetir($id);
	$sigortasirketi = $this->sql->sigortafirmalistesinecek($mailing->sigortasirketi);
	
	
	if($result){
		
		$this->load->library('email');
	
			$confing =array(
			'protocol'=>'smtp',
			'smtp_host'=>"smtp.gmail.com",
			'smtp_port'=>465,
			'smtp_user'=>"infomaxassist@gmail.com",
			'smtp_pass'=>"frksnl0629.",
			'smtp_crypto'=>'ssl',              
			'mailtype'=>'html'  
			);
	
			$this->email->initialize($confing);
			$this->email->set_newline("\r\n");
			$this->email->from('infomaxassist@gmail.com');
			$this->email->to($mailing->mail);
			$this->email->subject('Maxassist Psikomax Randevunuz Başarıyla Onaylandı!');
			$this->email->message('<div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;">Maxassist Psikomax Randevunuz Onaylanmıştır. Randevu Bilgileriniz : </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Randevu Alınan Ad Soyad = '.$mailing->musteriadi . " " .$mailing->musterisoyadi .' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Randevu Alınan Sigorta Şirketi = '.$sigortasirketi->sigorta_adi.' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;">Randevu Tarihi = '. $mailing->kurulum_tarih .'</div>');

				if(!$this->email->send()) {
								echo "hata";
				}
				else{
					echo "Gönderildi.";
				}
		
		
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Onaylandı !
                                     </div>');
									 redirect('yonetimpaneli/randevulistesi/pskimoaxrandevux');
	}
	
}


public function alarmrandevuduzenle($id){
	
	$this->load->model("sql");
	
	$data["iller"] = $this->sql->iller();
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	$data["bilgi"] = $this->sql->alarmrandevuceklist($id);
	$data["resimcek"] = $this->sql->resimcek($id);
	$data["firmalistcek"] = $this->sql->firmalistcek();
	
	    $musteriadi = $this->input->post("musteriadi");
		$musterisoyadi = $this->input->post("musterisoyadi");
		$il = $this->input->post("il");
		$ilce = $this->input->post("ilce");
		$tckimlik = $this->input->post("tckimlik");
		$police = $this->input->post("police");
		$sigortasirketi = $this->input->post("sigortasirketi");
		$mail = $this->input->post("mail");
		$tel = $this->input->post("tel");
		$date3 = $this->input->post("date");
		$adres = $this->input->post("adres");
		$notekle = $this->input->post("notekle");
		$id1 = $this->input->post("id");
		$onay = $this->input->post("onay");
	    $resim = $this->sql->resimcek($id);    
	    $saat = $this->input->post("saat");
		$firmalist = $this->input->post("firmalistesi");
	    
	    $date = new DateTime();
		$date = $date->getTimeStamp();
		
	    
		
	
	if($_POST){
		
		
		
		$say = count($_FILES["images"]["name"]);
		
		if($_FILES["images"]["name"][0]){
			
			   
			  
				
				for($j=0; $j < $say; $j++){
			    $parcala = explode(".",@$_FILES["images"]["name"][$j]);
				$yeniad = rand(0,9999999).$date.".".$parcala[1];
				$yol = "./uploads/";
				
				 
				
				
				if($_FILES["images"]["size"][$j] > 900000){
					$this->session->set_flashdata('alert','<div class="alert alert-warning"> Dosya boyutu çok büyük.
                                     </div>');
									 redirect('yonetimpaneli/alarmrandevuduzenle/'.$id);
				}
				else{
					
					if($_FILES["images"]["type"][$j] == "image/jpeg" || $_FILES["images"]["type"][$j] == "image/png" || $_FILES["images"]["type"][$j] == "image/jpg" || $_FILES["images"]["type"][$j] == "application/pdf" || $_FILES["images"]["type"][$j] == "application/msword" || $_FILES["images"]["type"][$j] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || $_FILES["images"]["type"][$j] == "application/vnd.ms-excel"){
						
						$tasi = move_uploaded_file($_FILES["images"]["tmp_name"][$j],$yol.$yeniad);
						
						if($tasi){
							
							$data = array(
							"resimadi" => $yeniad,
							"gelenid" => $id,
							);
							
							$result = $this->sql->resimekle($data);
							
							if($result){
								echo "eklendi.";
							}
							
							
						}
						
					}
					else{
						
						$this->session->set_flashdata('alert','<div class="alert alert-warning"> Dosya tipi geçerli değil.
                                     </div>');
									 redirect('yonetimpaneli/alarmrandevuduzenle/'.$id);
					}
					
				}
		 }
		 
		 
		 
		 
		 $data = array(
		"musteri_id" => $id1,
		"il" => $il,
		"ilce" => $ilce,
		"adres" => $adres,
		"tckimlikno" => $tckimlik,
		"policeno" => $police,
		"sigortasirketi" => $sigortasirketi,
		"mail" => $mail,
		"kurulum_tarih" => $date3,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"tel" => $tel,
		"onay" => $onay,
		"notekle" => $notekle,
		"kurulum_saat" => $saat,
		"firma" => $firmalist,
		);
		
		$result = $this->sql->alarmduzenleupdate($id,$data);
		
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/alarmrandevuduzenle/'.$id);
		
			
			
			
		}
		
		
		else{
			
	    $data = array(
		"musteri_id" => $id1,
		"il" => $il,
		"ilce" => $ilce,
		"adres" => $adres,
		"tckimlikno" => $tckimlik,
		"policeno" => $police,
		"sigortasirketi" => $sigortasirketi,
		"mail" => $mail,
		"kurulum_tarih" => $date3,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"tel" => $tel,
		"onay" => $onay,
		"notekle" => $notekle,
		"kurulum_saat" => $saat,
		"firma" => $firmalist,
		);
		
		$result = $this->sql->alarmduzenleupdate($id,$data);
        
        if($result){
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/alarmrandevuduzenle/'.$id);
		}
			
		}
		

		
		
	}
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/alarmrandevu_duzenle",$data);
	$this->load->view("yonetim/includes/footer");
}


public function haliyikamaduzenle($id){
	
	$this->load->model("sql");
	
	$data["iller"] = $this->sql->iller();
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	$data["bilgi"] = $this->sql->haliyikamarandevuceklist($id);
	$data["resimcek"] = $this->sql->resimcek($id);
	$data["firmalistcek"] = $this->sql->firmalistcek();
	
	    $musteriadi = $this->input->post("musteriadi");
		$musterisoyadi = $this->input->post("musterisoyadi");
		$il = $this->input->post("il");
		$ilce = $this->input->post("ilce");
		$tckimlik = $this->input->post("tckimlik");
		$police = $this->input->post("police");
		$sigortasirketi = $this->input->post("sigortasirketi");
		$mail = $this->input->post("mail");
		$saat = $this->input->post("saat");
		$tel = $this->input->post("tel");
		$date3 = $this->input->post("kurulumtarih");
		$adres = $this->input->post("adres");
		$notekle = $this->input->post("notekle");
		$id1 = $this->input->post("id");
		$onay = $this->input->post("onay");
	    $resim = $this->sql->resimcek($id);    
	    $firmalist = $this->input->post("firmalistesi");
	    $date = new DateTime();
		$date = $date->getTimeStamp();
		
	
	
	if($_POST){
		
		
		
		$say = count($_FILES["images"]["name"]);
		
		
		if($_FILES["images"]["name"][0]){
			
			
				
				for($j=0; $j < $say; $j++){
			    $parcala = explode(".",@$_FILES["images"]["name"][$j]);
				$yeniad = rand(0,9999999).$date.".".$parcala[1];
				$yol = "./uploads/";
				
				
				if($_FILES["images"]["size"][$j] > 500000){
					$this->session->set_flashdata('alert','<div class="alert alert-warning"> Dosya boyutu çok büyük.
                                     </div>');
									 redirect('yonetimpaneli/haliyikamaduzenle/'.$id);
				}
				else{
					
               	if($_FILES["images"]["type"][$j] == "image/jpeg" || $_FILES["images"]["type"][$j] == "image/png" || $_FILES["images"]["type"][$j] == "image/jpg" || $_FILES["images"]["type"][$j] == "application/pdf" || $_FILES["images"]["type"][$j] == "application/msword" || $_FILES["images"]["type"][$j] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || $_FILES["images"]["type"][$j] == "application/vnd.ms-excel"){
						
						$tasi = move_uploaded_file($_FILES["images"]["tmp_name"][$j],$yol.$yeniad);
						
						if($tasi){
							
							$data = array(
							"resimadi" => $yeniad,
							"gelenid" => $id,
							);
							
							$result = $this->sql->resimekle($data);
							
							if($result){
								echo "eklendi.";
							}
							
							
						}
						
					}
					else{
						
						
						$this->session->set_flashdata('alert','<div class="alert alert-warning"> Dosya tipi geçerli değil.
                                     </div>');
									 redirect('yonetimpaneli/haliyikamaduzenle/'.$id);
					}
					
				}
		 }
		 
		 
		 
		 
		 $data = array(
		"musteri_id" => $id1,
		"il" => $il,
		"ilce" => $ilce,
		"adres" => $adres,
		"tckimlikno" => $tckimlik,
		"policeno" => $police,
		"sigortasirketi" => $sigortasirketi,
		"mail" => $mail,
		"kurulum_tarih" => $date3,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"tel" => $tel,
		"onay" => $onay,
		"notekle" => $notekle,
		"firma" => $firmalist,
		"kurulum_saat" => $saat,
		);
		
		$result = $this->sql->haliyikamaduzenleupdate($id,$data);
		
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/haliyikamaduzenle/'.$id);
		
			
			
			
		}
		
		
		else{
			
	    $data = array(
		"musteri_id" => $id1,
		"il" => $il,
		"ilce" => $ilce,
		"adres" => $adres,
		"tckimlikno" => $tckimlik,
		"policeno" => $police,
		"sigortasirketi" => $sigortasirketi,
		"mail" => $mail,
		"kurulum_tarih" => $date3,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"tel" => $tel,
		"onay" => $onay,
		"notekle" => $notekle,
		"firma" => $firmalist,
		"kurulum_saat" => $saat,
		);
		
		$result = $this->sql->haliyikamaduzenleupdate($id,$data);
        
        if($result){
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/haliyikamaduzenle/'.$id);
		}
			
		}
		

		
		
	    }
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/haliyikama_duzenle",$data);
	$this->load->view("yonetim/includes/footer");
}





public function hasaretrandevuduzenle($id){
	
	$this->load->model("sql");
	
	
	$data["iller"] = $this->sql->iller();
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	$data["bilgi"] = $this->sql->hasarerandevurandevuceklist($id);
	$data["firmalistcek"] = $this->sql->firmalistcek();
	$data["resimcek"] = $this->sql->resimcek($id);
	
	    $musteriadi = $this->input->post("musteriadi");
		$musterisoyadi = $this->input->post("musterisoyadi");
		$il = $this->input->post("il");
		$ilce = $this->input->post("ilce");
		$tckimlik = $this->input->post("tckimlik");
		$police = $this->input->post("police");
		$firmalist = $this->input->post("firmalistesi");
		$sigortasirketi = $this->input->post("sigortasirketi");
		$saat = $this->input->post("saat");
		$mail = $this->input->post("mail");
		$tel = $this->input->post("telefon");
		$date3 = $this->input->post("date");
		$adres = $this->input->post("adres");
		$notekle = $this->input->post("notekle");
		$id1 = $this->input->post("id");
		$onay = $this->input->post("onay");
	    $resim = $this->sql->resimcek($id);    
	    
	    $date = new DateTime();
		$date = $date->getTimeStamp();
		
	
	
	if($_POST){
		
		$say = count($_FILES["images"]["name"]);
		
		if($_FILES["images"]["name"][0]){
			
			   
				
				for($j=0; $j < $say; $j++){
			    $parcala = explode(".",@$_FILES["images"]["name"][$j]);
				$yeniad = rand(0,9999999).$date.".".$parcala[1];
				$yol = "./uploads/";
				
				
				if($_FILES["images"]["size"][$j] > 500000){
					$this->session->set_flashdata('alert','<div class="alert alert-warning"> Dosya boyutu çok büyük.
                                     </div>');
									 redirect('yonetimpaneli/hasaretrandevuduzenle/'.$id);
				}
				else{
					
					if($_FILES["images"]["type"][$j] == "image/jpeg" || $_FILES["images"]["type"][$j] == "image/png" || $_FILES["images"]["type"][$j] == "image/jpg" || $_FILES["images"]["type"][$j] == "application/pdf" || $_FILES["images"]["type"][$j] == "application/msword" || $_FILES["images"]["type"][$j] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || $_FILES["images"]["type"][$j] == "application/vnd.ms-excel"){
						
						$tasi = move_uploaded_file($_FILES["images"]["tmp_name"][$j],$yol.$yeniad);
						
						if($tasi){
							
							$data = array(
							"resimadi" => $yeniad,
							"gelenid" => $id,
							);
							
							$result = $this->sql->resimekle($data);
							
							if($result){
								echo "eklendi.";
							}
							
							
						}
						
					}
					else{
						
						$this->session->set_flashdata('alert','<div class="alert alert-warning"> Dosya tipi geçerli değil.
                                     </div>');
									 redirect('yonetimpaneli/hasaretrandevuduzenle/'.$id);
					}
					
				}
		 }
		 
		 
		 
		 
		$data = array(
		"musteri_id" => $id1,
		"il" => $il,
		"ilce" => $ilce,
		"adres" => $adres,
		"tckimlikno" => $tckimlik,
		"policeno" => $police,
		"sigortasirketi" => $sigortasirketi,
		"mail" => $mail,
		"kurulum_tarih" => $date3,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"tel" => $tel,
		"onay" => $onay,
		"notekle" => $notekle,
		"firma" => $firmalist,
		"kurulum_saat" => $saat
		);
		
		$result = $this->sql->hasarerandevupdate($id,$data);
		
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/hasaretrandevuduzenle/'.$id);
		
			
			
			
		}
		
		
		else{
			
	    $data = array(
		"musteri_id" => $id1,
		"il" => $il,
		"ilce" => $ilce,
		"adres" => $adres,
		"tckimlikno" => $tckimlik,
		"policeno" => $police,
		"sigortasirketi" => $sigortasirketi,
		"mail" => $mail,
		"kurulum_tarih" => $date3,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"tel" => $tel,
		"onay" => $onay,
		"notekle" => $notekle,
		"firma" => $firmalist,
		"kurulum_saat" => $saat
		);
		
		$result = $this->sql->hasarerandevupdate($id,$data);
        
        if($result){
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/hasaretrandevuduzenle/'.$id);
		}
			
		}
		

		
		
	}
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/hasarerandevu_duzenle",$data);
	$this->load->view("yonetim/includes/footer");
}



public function montajrandevuduzenle($id){
	
	$this->load->model("sql");
	
	$data["iller"] = $this->sql->iller();
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	$data["bilgi"] = $this->sql->montajrandevurandevuceklist($id);
	$data["resimcek"] = $this->sql->resimcek($id);
	$data["firmalistcek"] = $this->sql->firmalistcek();
	
	    $musteriadi = $this->input->post("musteriadi");
		$musterisoyadi = $this->input->post("musterisoyadi");
		$il = $this->input->post("il");
		$ilce = $this->input->post("ilce");
		$tckimlik = $this->input->post("tckimlik");
		$police = $this->input->post("police");
		$firmalist = $this->input->post("firmalistesi");
		$sigortasirketi = $this->input->post("sigortasirketi");
		$mail = $this->input->post("mail");
		$tel = $this->input->post("telefon");
		$saat = $this->input->post("saat");
		$date3 = $this->input->post("date");
		$adres = $this->input->post("adres");
		$notekle = $this->input->post("notekle");
		$id1 = $this->input->post("id");
		$onay = $this->input->post("onay");
	    $resim = $this->sql->resimcek($id);    
	    
	    $date = new DateTime();
		$date = $date->getTimeStamp();
		
	
	
	if($_POST){
		
		$say = count($_FILES["images"]["name"]);
		
		if($_FILES["images"]["name"][0]){
			
			   
				
				for($j=0; $j < $say; $j++){
			    $parcala = explode(".",@$_FILES["images"]["name"][$j]);
				$yeniad = rand(0,9999999).$date.".".$parcala[1];
				$yol = "./uploads/";
				
				
				if($_FILES["images"]["size"][$j] > 500000){
					$this->session->set_flashdata('alert','<div class="alert alert-warning"> Dosya boyutu çok büyük.
                                     </div>');
									 redirect('yonetimpaneli/montajrandevuduzenle/'.$id);
				}
				else{
					
					if($_FILES["images"]["type"][$j] == "image/jpeg" || $_FILES["images"]["type"][$j] == "image/png" || $_FILES["images"]["type"][$j] == "image/jpg" || $_FILES["images"]["type"][$j] == "application/pdf" || $_FILES["images"]["type"][$j] == "application/msword" || $_FILES["images"]["type"][$j] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || $_FILES["images"]["type"][$j] == "application/vnd.ms-excel"){
						
						$tasi = move_uploaded_file($_FILES["images"]["tmp_name"][$j],$yol.$yeniad);
						
						if($tasi){
							
							$data = array(
							"resimadi" => $yeniad,
							"gelenid" => $id,
							);
							
							$result = $this->sql->resimekle($data);
							
							if($result){
								echo "eklendi.";
							}
							
							
						}
						
					}
					else{
						
						$this->session->set_flashdata('alert','<div class="alert alert-warning"> Dosya tipi geçerli değil.
                                     </div>');
									 redirect('yonetimpaneli/montajrandevuduzenle/'.$id);
					}
					
				}
		 }
		 
		 
		 
		 
		 $data = array(
		"musteri_id" => $id1,
		"il" => $il,
		"ilce" => $ilce,
		"adres" => $adres,
		"tckimlikno" => $tckimlik,
		"policeno" => $police,
		"sigortasirketi" => $sigortasirketi,
		"mail" => $mail,
		"kurulum_tarih" => $date3,
		"kurulum_saat" => $saat,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"tel" => $tel,
		"onay" => $onay,
		"notekle" => $notekle,
		"firma" => $firmalist
		);
		
		$result = $this->sql->montajduzenleupdate($id,$data);
		
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/montajrandevuduzenle/'.$id);
		
			
			
			
		}
		
		
		else{
			
	    $data = array(
		"musteri_id" => $id1,
		"il" => $il,
		"ilce" => $ilce,
		"adres" => $adres,
		"tckimlikno" => $tckimlik,
		"policeno" => $police,
		"sigortasirketi" => $sigortasirketi,
		"mail" => $mail,
		"kurulum_tarih" => $date3,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"tel" => $tel,
		"onay" => $onay,
		"notekle" => $notekle,
		"firma" => $firmalist,
		"kurulum_saat" => $saat,
		);
		
		$result = $this->sql->montajduzenleupdate($id,$data);
        
        if($result){
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/montajrandevuduzenle/'.$id);
		}
			
		}
		

		
		
	}
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/montajrandevu_duzenle",$data);
	$this->load->view("yonetim/includes/footer");
}


public function nakliyatrandevuduzenle($id){
	
	$this->load->model("sql");
	
		$data["iller"] = $this->sql->iller();
		$data["liste"] = $this->sql->sigortafirmalistesicek();
		$data["bilgi"] = $this->sql->nakliyatrandevuceklist($id);
		$data["bilgi1"] = $this->sql->nakliyatrandevuceklist1($id);
		$data["resimcek"] = $this->sql->resimcek($id);
		$data["firmalistcek"] = $this->sql->firmalistcek();
	
	    $id1 = $this->input->post("id");
	    $musteriadi = $this->input->post("musteriadi");
	    $musterisoyadi = $this->input->post("musterisoyadi");
	    $tckimlikno = $this->input->post("tckimlikno");
	    $mail = $this->input->post("mail");
	    $baslangictarihi = $this->input->post("date");
	    $saat = $this->input->post("saat");
	    $il = $this->input->post("il");
	    $ilce = $this->input->post("ilce");
	    $odasayisi = $this->input->post("odasayisi");
	    $evkacincikatta = $this->input->post("evkacincikatta");
	    $esyanasiltasinacak = $this->input->post("esyanasiltasinacak");
	    $sigortasirketi = $this->input->post("sigortasirketi");
	    $policeno = $this->input->post("policeno");
	    $tel = $this->input->post("tel");
	    $adres = $this->input->post("adres");
	    $ilen = $this->input->post("ilen");
	    $ilcen = $this->input->post("ilcen");
	    $odasayisi1 = $this->input->post("odasayisi1");
	    $evkacincikatta1 = $this->input->post("evkacincikatta1");
	    $esyanasiltasinacak1 = $this->input->post("esyanasiltasinacak1");
	    $adres1 = $this->input->post("adres1");
	    $onay = $this->input->post("onay");
	    $notekle = $this->input->post("notekle");
		$firmalist = $this->input->post("firmalistesi");
		  
	    
	    $date = new DateTime();
		$date = $date->getTimeStamp();
		
	
	
	if($_POST){
		
		$say = count($_FILES["images"]["name"]);
		
		if($_FILES["images"]["name"][0]){
			
			   
				
				for($j=0; $j < $say; $j++){
			    $parcala = explode(".",@$_FILES["images"]["name"][$j]);
				$yeniad = rand(0,9999999).$date.".".$parcala[1];
				$yol = "./uploads/";
				
				
				if($_FILES["images"]["size"][$j] > 500000){
					$this->session->set_flashdata('alert','<div class="alert alert-warning"> Dosya boyutu çok büyük.
                                     </div>');
									 redirect('yonetimpaneli/nakliyatrandevuduzenle/'.$id);
				}
				else{
					
					
					
					if($_FILES["images"]["type"][$j] == "image/jpeg" || $_FILES["images"]["type"][$j] == "image/png" || $_FILES["images"]["type"][$j] == "image/jpg" || $_FILES["images"]["type"][$j] == "application/pdf" || $_FILES["images"]["type"][$j] == "application/msword" || $_FILES["images"]["type"][$j] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || $_FILES["images"]["type"][$j] == "application/vnd.ms-excel"){
						
						
						
						$tasi = move_uploaded_file($_FILES["images"]["tmp_name"][$j],$yol.$yeniad);
						
						if($tasi){
							
							$data = array(
							"resimadi" => $yeniad,
							"gelenid" => $id,
							);
							
							$result = $this->sql->resimekle($data);
							
							if($result){
								echo "eklendi.";
							}
							
							
						}
						
					}
					else{
						
						$this->session->set_flashdata('alert','<div class="alert alert-warning"> Dosya tipi geçerli değil.
                                     </div>');
									 redirect('yonetimpaneli/nakliyatrandevuduzenle/'.$id);
					}
					
				}
		 }
		 
		 
		$date3 = date("d.m.Y");
		 
		$data2 = array(
		"musteri_id" => $id1,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"tckimlikno" => $tckimlikno,
		"kurulum_tarih" => $baslangictarihi,
		"kurulum_saat" => $saat,
		"il" => $il,
		"ilce" => $ilce,
		"sigortasirketi" => $sigortasirketi,
		"adres" => $adres,
		"tel" => $tel,
		"policeno" => $policeno,
		"mail" => $mail,
		"onay" => $onay,
		"date" => $date3,
		"notekle" => $notekle,
		"firma" => $firmalist,
		);
		
		$result = $this->sql->nakliyatduzenleupdate($id,$data2);
		
		$data3 = array(
		"odasayisi" => $odasayisi,
		"evkacincikatta" => $evkacincikatta,
		"esyanasiltasinacak" => $esyanasiltasinacak,
		"il1" => $ilen,
		"ilce1" => $ilcen,
		"odasayisi1" => $odasayisi1,
		"evkacincikatta1" => $evkacincikatta1,
		"esyanasiltasinacak1" => $esyanasiltasinacak1,
		"adres1" => $adres1,
		);
		
		$result1 = $this->sql->nakliyatduzenleupdate1($id,$data3);
		
		
		if($result){
			$this->session->set_flashdata('alert','<div class="alert alert-success">Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/nakliyatrandevuduzenle/'.$id);
		}
		
			
			
		}
		
		
		else{
			
			
			
	    $date3 = date("d.m.Y");
		 
		$data2 = array(
		"musteri_id" => $id1,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"tckimlikno" => $tckimlikno,
		"kurulum_tarih" => $baslangictarihi,
		"kurulum_saat" => $saat,
		"il" => $il,
		"ilce" => $ilce,
		"sigortasirketi" => $sigortasirketi,
		"adres" => $adres,
		"tel" => $tel,
		"policeno" => $policeno,
		"mail" => $mail,
		"onay" => $onay,
		"date" => $date3,
		"notekle" => $notekle,
		"firma" => $firmalist,
		);
		
		$result = $this->sql->nakliyatduzenleupdate($id,$data2);
		
		$data3 = array(
		"odasayisi" => $odasayisi,
		"evkacincikatta" => $evkacincikatta,
		"esyanasiltasinacak" => $esyanasiltasinacak,
		"il1" => $ilen,
		"ilce1" => $ilcen,
		"odasayisi1" => $odasayisi1,
		"evkacincikatta1" => $evkacincikatta1,
		"esyanasiltasinacak1" => $esyanasiltasinacak1,
		"adres1" => $adres1,
		);
		
		$result1 = $this->sql->nakliyatduzenleupdate1($id,$data3);
		
        
        if($result){
			
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/nakliyatrandevuduzenle/'.$id);
		}
		
		
		
			
		}
		

		
		
	}
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/nakliyatrandevu_duzenle",$data);
	$this->load->view("yonetim/includes/footer");
}


public function psikomaxrandevuduzenle($id){
	
	$this->load->model("sql");
	
	$data["iller"] = $this->sql->iller();
	$data["liste"] = $this->sql->sigortafirmalistesicek();
	$data["bilgi"] = $this->sql->piskomaxrandevuceklist($id);
	$data["resimcek"] = $this->sql->resimcek($id);
	$data["firmalistcek"] = $this->sql->firmalistcek();
	
	    $musteriadi = $this->input->post("musteriadi");
		$musterisoyadi = $this->input->post("musterisoyadi");
		$il = $this->input->post("il");
		$ilce = $this->input->post("ilce");
		$tckimlik = $this->input->post("tckimlik");
		$police = $this->input->post("police");
		$sigortasirketi = $this->input->post("sigortasirketi");
		$mail = $this->input->post("mail");
		$tel = $this->input->post("tel");
		$date3 = $this->input->post("date");
		$firmalist = $this->input->post("firmalistesi");
		$adres = $this->input->post("adres");
		$notekle = $this->input->post("notekle");
		$saat = $this->input->post("saat");
		$id1 = $this->input->post("id");
		$onay = $this->input->post("onay");
	    $resim = $this->sql->resimcek($id);    
	    
	    $date = new DateTime();
		$date = $date->getTimeStamp();
		
	
	
	if($_POST){
		
		$say = count($_FILES["images"]["name"]);
		
		if($_FILES["images"]["name"][0]){
			
			   
				
				for($j=0; $j < $say; $j++){
			    $parcala = explode(".",@$_FILES["images"]["name"][$j]);
				$yeniad = rand(0,9999999).$date.".".$parcala[1];
				$yol = "./uploads/";
				
				
				if($_FILES["images"]["size"][$j] > 500000){
					$this->session->set_flashdata('alert','<div class="alert alert-warning"> Dosya boyutu çok büyük.
                                     </div>');
									 redirect('yonetimpaneli/psikomaxrandevuduzenle/'.$id);
				}
				else{
					
					if($_FILES["images"]["type"][$j] == "image/jpeg" || $_FILES["images"]["type"][$j] == "image/png" || $_FILES["images"]["type"][$j] == "image/jpg" || $_FILES["images"]["type"][$j] == "application/pdf" || $_FILES["images"]["type"][$j] == "application/msword" || $_FILES["images"]["type"][$j] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || $_FILES["images"]["type"][$j] == "application/vnd.ms-excel"){
						
						$tasi = move_uploaded_file($_FILES["images"]["tmp_name"][$j],$yol.$yeniad);
						
						if($tasi){
							
							$data = array(
							"resimadi" => $yeniad,
							"gelenid" => $id,
							);
							
							$result = $this->sql->resimekle($data);
							
							if($result){
								echo "eklendi.";
							}
							
							
						}
						
					}
					else{
						
						$this->session->set_flashdata('alert','<div class="alert alert-warning"> Dosya tipi geçerli değil.
                                     </div>');
									 redirect('yonetimpaneli/psikomaxrandevuduzenle/'.$id);
					}
					
				}
		 }
		 
		 
		 
		 
		 $data = array(
		"musteri_id" => $id1,
		"il" => $il,
		"ilce" => $ilce,
		"adres" => $adres,
		"tckimlikno" => $tckimlik,
		"policeno" => $police,
		"sigortasirketi" => $sigortasirketi,
		"mail" => $mail,
		"kurulum_tarih" => $date3,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"tel" => $tel,
		"onay" => $onay,
		"notekle" => $notekle,
		"firma" => $firmalist,
		"kurulum_saat" => $saat,
		);
		
		$result = $this->sql->psikomaxduzenleupdate($id,$data);
		
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/psikomaxrandevuduzenle/'.$id);
		
			
			
			
		}
		
		
		else{
			
	    $data = array(
		"musteri_id" => $id1,
		"il" => $il,
		"ilce" => $ilce,
		"adres" => $adres,
		"tckimlikno" => $tckimlik,
		"policeno" => $police,
		"sigortasirketi" => $sigortasirketi,
		"mail" => $mail,
		"kurulum_tarih" => $date3,
		"musteriadi" => $musteriadi,
		"musterisoyadi" => $musterisoyadi,
		"tel" => $tel,
		"onay" => $onay,
		"notekle" => $notekle,
		"firma" => $firmalist,
		"kurulum_saat" => $saat,
		);
		
		$result = $this->sql->psikomaxduzenleupdate($id,$data);
        
        if($result){
			$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Düzenlendi !
                                     </div>');
									 redirect('yonetimpaneli/psikomaxrandevuduzenle/'.$id);
		}
			
		}
		

		
		
	}
	
	
	$this->load->view("yonetim/includes/header");
	$this->load->view("yonetim/includes/sidebar");
	$this->load->view("yonetim/piskomaxrandevu_duzenle",$data);
	$this->load->view("yonetim/includes/footer");
}

public function anketsil($id){
	$this->load->model("sql");
	
	$result = $this->sql->anketsil($id);
	
	if($result){
		$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Silindi !
                                     </div>');
									 redirect('yonetimpaneli/anketlistesi');
	}
	
}

public function excelaktar(){
     
    $this->load->model("sql");	 
    
   
	
   
	
	function exportExcel($filename='ExportExcel',$columns=array(),$data=array(),$replaceDotCol=array()){
    header('Content-Encoding: UTF-8');
    header('Content-Type: text/plain; charset=utf-8'); 
    header("Content-disposition: attachment; filename=".$filename.".xls");
    echo "\xEF\xBB\xBF"; 
      
    $say=count($columns);
	
      
    echo '<table border="1"><tr>';
    foreach($columns as $v){
        echo '<th style="background-color:#FFA500">'.trim($v).'</th>';
    }
    echo '</tr>';
  
    foreach($data as $val){
        echo '<tr>';
        for($i=0; $i < $say; $i++){
  
            if(in_array($i,$replaceDotCol)){
                echo '<td>'.str_replace('.',',',@$val[$i]).'</td>';
            }else{
                echo '<td>'.@$val[$i].'</td>';
            }
        }
        echo '</tr>';
    }
}


 
$columns=array();
 
 

$replaceDotCol=array(2); 
 

$columns=array(
    'Toplam Müşteri Sayısı',
	'Toplam Sigorta Müşteri Sayısı',
	'Toplam Anket Sayısı',
    'Toplam Alarm Randevusu',
    'Toplam Nakliyat Randevusu',
	'Toplam Montaj Randevusu',
	'Toplam Halı Yıkama Randevusu',
	'Toplam Piskomax Randevusu',
	'Toplam Haşare Randevusu',
	'Toplam Randevu Sayısı',
);
 

$data1 = $this->sql->toplammusterisayisi();
$data2 = $this->sql->toplamsigortamusterisayisi();
$data3 = $this->sql->anketlistesitoplam();
$data4 = $this->sql->alarmrandevutoplam();
$data5 = $this->sql->nakliyatrandevutoplam();
$data6 = $this->sql->montajrandevutoplam();
$data7 = $this->sql->haliyikamarandevutoplam();
$data8 = $this->sql->psikomaxtoplam();
$data9 = $this->sql->hasaretoplam();
$data10 = $this->sql->totalrandevu();



	
$data[] =  array(
$data1,
$data2,
$data3,
$data4,
$data5,
$data6,
$data7,
$data8,
$data9,
$data10,
);
	





exportExcel('Toplam',$columns,$data,$replaceDotCol);
	  
	  
  }
  
  
public function alarmrandevuresimsil($id){
	$this->load->model("sql");
	
	$result = $this->sql->randevuresimsil($id);
	
}

public function piskomaxrandevuresimsil($id){
	$this->load->model("sql");
	
	$result = $this->sql->randevuresimsil($id);
	
}

public function nakliyatrandevuresimsil($id){
	$this->load->model("sql");
	
	$result = $this->sql->randevuresimsil($id);
	
}

public function haliyikamarandevuresimsil($id){
	$this->load->model("sql");
	
	$result = $this->sql->randevuresimsil($id);
	
}

public function hasarettrandevusil($id){
	$this->load->model("sql");
	
	$result = $this->sql->randevuresimsil($id);
	
}


public function reyal(){
	$this->load->model("sql");
	
	$anketid = $this->input->post("anketid");
	
	$anketsonucek = $this->sql->anketsonucek($anketid);
	$eskicevap1 = $anketsonucek->cevap1;
	$eskicevap2 = $anketsonucek->cevap2;
	
	
	$cevap1 = $this->input->post("cevap1");
	$cevap2 = $this->input->post("cevap2");
	$musterid = $this->input->post("musterid");
	
	$cevap1 += $eskicevap1;
	$cevap2 += $eskicevap2;
	
	$date = date("d.m.Y");
	$data = array(
	"cevap1" => $cevap1,
	"cevap2" => $cevap2,
	"date" => $date,
	);
	
	$result = $this->sql->anketsonucekle($data,$anketid);
	
	$result1 = $this->sql->anketgosterekle($musterid,$anketid);
	
	
	
}







	

	
}
