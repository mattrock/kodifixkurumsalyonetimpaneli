<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class yonetim extends CI_Controller {
	
	
	public function index()
	{
		$this->load->view("yonetim/giris");
		
	}
	
	public function checklogin(){
		$this->load->model("sql");
		
		$this->form_validation->set_rules("username","Kullanıcı adı","required|trim");
		$this->form_validation->set_rules("password","Şifre","required|trim");
		$this->form_validation->set_message('required',"%s Boş olamaz.");
		
		if($this->form_validation->run()){
			
			$username = $this->input->post("username",1);
			$password = $this->input->post("password",1);
			
			$result = $this->sql->logincheck($username,$password);
			
			
			
			if($result){
				
				$this->session->set_userdata("kontrol",TRUE);
				$this->session->set_userdata("uyebilgi",$result);				
                
				redirect("yonetimpaneli");
				
			}
			else{
					
					$result2 = $this->sql->sigortacheck($username,$password);
					
					if($result2){
					$result2->uyetipi = "sigortafirmasi";
					$this->session->set_userdata("kontrol",TRUE);
				    $this->session->set_userdata("uyebilgi",$result2);
					redirect("yonetimpaneli");
				    }
					else{
						$this->session->set_flashdata('hata','Email adresi veya Şifre Yanlış.');
				        redirect('yonetim');
					}
					
					
					$this->session->set_flashdata('hata','Email adresi veya Şifre Yanlış.');
				    redirect('yonetim');
				
				
				$this->session->set_flashdata('hata','Email adresi veya Şifre Yanlış.');
				redirect('yonetim');
			}
			
			
		}
		
		$this->load->view("yonetim/giris");
		
	}
	
	
	public function kayitol(){
		$this->load->model("sql");
		$data["liste"] = $this->sql->sigortafirmalistesicek();
		$data["iller"] = $this->sql->iller();
		$this->load->view("yonetim/kayitol",$data);
	}
	
	
		
		
	
	
	public function kayitislemleri(){
		$this->load->model("sql");
		
		$ad = $this->input->post("ad");
		$soyad = $this->input->post("soyad");
		$mail = $this->input->post("mail");
		$sifre = $this->input->post("sifre");
		$tel = $this->input->post("tel");
		$sigortasirketi = $this->input->post("sigortasirketi");
		$il = $this->input->post("il");
		$ilce = $this->input->post("ilce");
		$policeno = $this->input->post("policeno");
		$tckimlikno = $this->input->post("tckimlikno");
		$adres = $this->input->post("adres");
		
		$data = array(
		"musteri_adi" => $ad,
		"musteri_soyadi" => $soyad,
		"tc_kimlik_no" => $tckimlikno,
		"police_no" => $policeno,
		"sigorta_sirketi" => $sigortasirketi,
		"telefon" => $tel,
		"mail" => $mail,
		"il" => $il,
		"ilce" => $ilce,
		"adres" => $adres,
		"sifre" => $sifre
		);
		
		$result = $this->sql->hesapolustur($data);
		
		if($result){
			
			$this->load->library('email');
	
			$confing =array(
			'protocol'=>'smtp',
			'smtp_host'=>"smtp.gmail.com",
			'smtp_port'=>465,
			'smtp_user'=>"infomaxassist@gmail.com",
			'smtp_pass'=>"frksnl0629.",
			'smtp_crypto'=>'ssl',              
			'mailtype'=>'html'  
			);
	
			$this->email->initialize($confing);
			$this->email->set_newline("\r\n");
			$this->email->from('infomaxassist@gmail.com');
			$this->email->to($mail);
			$this->email->subject('Maxassist Hesabınız Başarıyla Oluşturuldu !');
			$this->email->message('<div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Tebrikler Hesabınız başarıyla oluşturuldu aşağıdaki bilgiler kullanarak hesabınıza giriş yapabilirsiniz.</div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Mail Adresiniz = '.$mail.' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;"> Şifreniz = '.$sifre.' </div> <div style="background: #eee; padding: 10px; font-size: 14px; margin-bottom:3px;">Sistemimize Giriş için <a href="http://www.maxassist.com.tr/homeguard/yonetim">buraya</a> tıklayabilirsiniz. </div>');

				if(!$this->email->send()) {
								echo "hata";
				}
				else{
					echo "Gönderildi.";
				}
					
				}
		
		
			}
	
	public function ilce($il){
	$this->load->model("sql");
	$result = $this->sql->ilce($il);
	if($result){
		foreach($result as $yaz){
			echo '<option value="'.$yaz->ilce_no.'">'.$yaz->isim.'</option>';
		}
	}
}

	

	
}
