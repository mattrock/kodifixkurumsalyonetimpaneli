<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class anasayfa extends CI_Controller {
	
	public function index()
	{
		redirect('yonetim');
	}
	
	public function urunara(){
		$this->load->model("sql");
		
		$id = $this->input->get("term");
		
		
		$result = $this->sql->urunterm($id);
		
		foreach($result as $row){
			
			$data[] = array(
			"value" => $row->isim,
			"isim" => $row->isim,
			
			);
		}
		
		echo json_encode($data);
		
	}
	
	
	
}
