<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="icon" href="favicon.ico" type="image/x-icon">
  <title>Kayıt Ol</title>
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url("assets"); ?>\assets\js\vendor\bootstrap\bootstrap.min.css">
  <!--  Fonts and icons -->
  <!-- CSS Files -->
  <link href="<?php echo base_url("assets"); ?>\assets\css\main.css" rel="stylesheet">

  <style type="text/css">
  
  .form-group{
	  position:unset;
  }
  
  </style>
  
</head>

<body id="falcon" class="authentication">
  <!-- Application Content -->
  <div class="wrapper">
    <div class="header header-filter" style="background-image: url('<?php echo base_url("assets"); ?>/assets/images/login-bg.jpg'); background-size: cover; background-position: top center;">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 col-sm-5 text-center">
            <div class="card card-signup">
              <form class="form" method="POST" action="javascript:;">
                <div class="header header-primary text-center">
                  <h4>Müşteri Hesabı Oluştur</h4>
                  
                </div>
                <h3 class="mt-0">Kayıt Ol</h3>
                <p class="help-block">Kayıt olun daha fazla özelliklerimizden yararlanın.</p>
                <div class="content">
				
                  <div class="form-group col-sm-6">
                    <input type="text" class="form-control ad" placeholder="Adınız" required>
                  </div>
				  
                  <div class="form-group col-sm-6">
                    <input type="text" class="form-control underline-input soyad" placeholder="Soyadınız" required>
                  </div>
				  
				  <div class="form-group col-sm-6">
                    <input type="text" class="form-control underline-input mail" placeholder="Mail Adresiniz" required>
                  </div>
				  
				  
                  <div class="form-group col-sm-6">
                    <input type="password" placeholder="Şifreniz" class="form-control sifre" required>
                  </div>
				  
				  <div class="form-group col-sm-6">
                    <input type="text" placeholder="Telefon No" class="form-control tel" required>
                  </div>
				  
				  <div class="form-group col-sm-6">
				  <select name="f2" class="form-control mb-10 sigortasirketi" data-parsley-trigger="change" required="" data-parsley-id="5245" required>
												<option value="0" selected disabled>Sigorta Şirketi Seçiniz</option>
												<?php foreach($liste as $yaz){ ?>
												<option value="<?php echo $yaz->id; ?>"><?php echo $yaz->sigorta_adi; ?></option>
												<?php } ?>
											</select>                                          																
                   </div>
				   
				   <div class="form-group col-sm-6">
                    <select name="il"  class="form-control mb-10 il" data-parsley-trigger="change" required="" data-parsley-id="5245" required>
												<option value="0">İl Seçiniz</option>
												<?php foreach($iller as $yaz){
												?>
												<option value="<?php echo $yaz->il_no; ?>"><?php echo $yaz->isim; ?></option>
												<?php } ?>
											</select> 
                  </div>
				  
				  <div class="form-group col-sm-6">
                    <select name="ilce" class="form-control mb-10 ilce" data-parsley-trigger="change" required="" data-parsley-id="5245" required>
												<option value="">Önce İl Seçiniz</option>
												
											</select> 
                  </div>
				  
				  <div class="form-group col-sm-6">
                    <input type="text" placeholder="Police no" class="form-control policeno" required>
                  </div>
				  
				  <div class="form-group col-sm-6">
                    <input type="text" placeholder="Tc Kimlik No" class="form-control tckimlikno" required>
                  </div>
				  
				  <div class="form-group col-sm-12">
                    <textarea placeholder="Adresiniz" class="form-control adres" required></textarea>
                  </div>
				  
				  
                  <div class="checkbox col-sm-12">
                    <label>
                      <input type="checkbox" name="optionsCheckboxes" checked=""> Kabul Ediyorum
                      <a href="javascript:;">Kullanım Koşulları</a> &amp;
                      <a href="javascript:;">Gizlilik Politikası</a>
                    </label>
                  </div>
				  
                </div>
				
                <div class="footer text-center mb-20">
                  <button href="/homeguard/yonetim/kayitol" class="btn btn-info btn-raised">Kayıt Ol</button>
                </div>
				<div class="alert alert-success success" style="display:none;"> Kayıt İşleminiz Başarıyla Gerçekleşmiştir. <br/>Bilgileriniz Mail Adresinize Gönderilmiştir.
                                     </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center mt-20">
              <a href="/homeguard/yonetim/" class="text-uppercase text-white">Geri Dön</a>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--/ Application Content -->
  <!--  Vendor JavaScripts -->
  <script src="<?php echo base_url("assets"); ?>\assets\bundles\libscripts.bundle.js"></script>
  <script src="<?php echo base_url("assets"); ?>\assets\bundles\mainscripts.bundle.js"></script>  
  <script>
  
  $("form").submit(function(){
	  
	  var ad = $(".ad").val();
	  var soyad = $(".soyad").val();
	  var mail = $(".mail").val();
	  var sifre = $(".sifre").val();
	  var tel = $(".tel").val();
	  var sigortasirketi = $(".sigortasirketi").val();
	  var il = $(".il").val();
	  var ilce = $(".ilce").val();
	  var policeno = $(".policeno").val();
	  var tckimlikno = $(".tckimlikno").val();
	  var adres = $(".adres").val();
	  
	  $.ajax({
		  url:"<?php echo base_url("/yonetim/kayitislemleri"); ?>",
		  type:"POST",
		  data:{ad:ad,soyad:soyad,mail:mail,sifre:sifre,tel:tel,sigortasirketi:sigortasirketi,il:il,ilce:ilce,policeno:policeno,tckimlikno:tckimlikno,adres:adres},
		  success:function(r){
			  $(".success").fadeIn();
		  }
	  
	  
       });
  });
 
		
		$(document).ready(function(){
			
			$(".il").change(function(){
				var il = $(".il").val();
				
				$.ajax({
					url:"/homeguard/yonetim/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilce").html(r);
					}
				});
				
				
				
			});
			
			
		});
		
		</script>
  
  
  </script>
</body>
</html>