
<section id="content">
			<div class="page profile-page">
				<!-- page content -->
				<div class="pagecontent">
					<!-- row -->
					<div class="row">
                        <div style="width:1000px; height: 1000px; margin-left:50px;">
						<div role="tabpanel" class="tab-pane" id="setting" >
                                                <div class="wrap-reset">
                                                    <form class="profile-settings" name="arama" method="POST" action="<?php echo base_url("yonetimpaneli/mailgonder"); ?>">
                                                        <div class="row">
														<?php echo $this->session->flashdata('alert'); ?>
                                                            <div class="form-group col-md-12 legend">
                                                                <h3>
                                                                    <strong>Anket Sihirbazı</strong> Formu</h3>
                                                                <p>Anket Yollama işlemlerini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
														
														<div class="form-group col-sm-12">
                                                                <label for="username">Gönderilecek Anket Sorusu</label>
                                                                <select name="anketsorusu[]" class="form-control anketsorusu" rows="5" multiple="multiple">
																<option value="0"  selected disabled>Seçiniz</option>
																<?php foreach($anketler as $yaz){ ?>
																<option value="<?php echo $yaz->id; ?>"><?php echo $yaz->anketsorusu; ?></option>
																<?php } ?>
																</select>
                                                            </div>
														
                                                            <div class="form-group col-sm-12">
                                                                <label for="username">Gönderilecek Mailler <a class="getir" style="background:#fff; padding:3px; border:1px solid #d1d1d1; border-radius:3px; cursor:pointer;">Müşteri maillerini getir.</a></label>
                                                                <textarea name="mailist" class="form-control mailist" rows="5" ></textarea>
                                                            </div>
                                                            
                                                           
                                        
                                                            

                                                            <div class="form-group col-sm-12">
                                                                <button class="btn btn-raised btn-primary">Soruyu Gönder</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                                                </div>
                                            </div> </div>
						
					</div>
				</div>
			</div>
		</section>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.js"> </script>
		<script>
		
		$(document).ready(function(){
			
			$("form").submit(function(){
				
				var a = $(".anketsorusu option:selected").val();
				var b = $(".mailist").val();
				
				if(a == "0"){
					$(".anketsorusu").css("border","1px solid red");
				}
				else{
					$(".anketsorusu").css("border","1px solid lightgreen");
				}
				
				if(b == ""){
					$(".mailist").css("border","1px solid red");
				}
				else{
					$(".mailist").css("border","1px solid lightgreen");
				}
				
				if(a == "0" || b == ""){
					return false;
				}
				
			});
			
			$(".getir").click(function(){
				
				$.ajax({
					url:"<?php echo base_url("yonetimpaneli/musterimailgetir"); ?>",
					type:"POST",
					success:function(r){
						$(".mailist").html(r);
					}
				});
				
			});
			
		});
		
		
		</script>