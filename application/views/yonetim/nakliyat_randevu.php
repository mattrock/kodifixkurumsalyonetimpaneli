<?php 

$uyebilgi = $this->session->userdata("uyebilgi");

?>
<section id="content">
      <div class="page profile-page">
        <!-- page content -->
        <div class="pagecontent">
          <!-- row -->
          <div class="row">
                        <div style="width:1000px; height: 1000px; margin-left:50px;">
            <div role="tabpanel" class="tab-pane" id="setting" >
                                                <div class="wrap-reset">
                                                    <form class="profile-settings" action="" method="POST">
                                                        <div class="row">
                                                            <div class="form-group col-md-8 legend">
                                                                <h3>
                                                                    <strong>Nakliyat Randevu</strong> Formu</h3>
                                                                <p>Nakliyat Randevu işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
														<?php echo $this->session->flashdata('alert'); ?>
														<?php if(@$uyebilgi->uyetipi == "musteri"){ ?>
														
														<div class="form-group col-sm-6" >
                                                                <a href="javascript:;" class="btn btn-raised btn-primary ara">Bilgilerimi Otomatik Getir.</a>
																<input type="hidden" name="" class="form-control" value="<?php echo @$uyebilgi->tc_kimlik_no; ?>" rows="5"  id="aratext" placeholder="Tc kimlik no ile ara">
                                                            </div>
														<?php } else { ?>
                                                          <div class="form-group col-sm-6">
                                                                <label for="username"></label>
                                                                <input type="text" name="" class="form-control" rows="5"  id="aratext" placeholder="Tc kimlik no ile ara">
                                                            </div>
														
                                                            <div class="form-group col-sm-6" >
                                                                <a href="javascript:;" class="btn btn-raised btn-primary ara">Ara</a>
                                                            </div>
														<?php } ?>
															<input type="hidden" class="id" name="id" value="">
															<div class="nereden" style="margin-top:120px;">
															<h4 style="color:red; padding-left:15px;">Nereden ?</h4>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Adı</label>
                                                                <input type="text" name="musteriadi" class="form-control musteriadi" rows="5"  id="datetime" >
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Soyadı</label>
                                                                <input type="text" name="musterisoyadi" class="form-control musterisoyadi" rows="5"  id="datetime" >
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Tc Kimlik No</label>
                                                                <input type="text" name="tckimlikno" class="form-control tckimlikno" rows="5"  id="datetime" >
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Mail Adresi</label>
                                                                <input type="text" name="mail" class="form-control mail" rows="5"  id="datetime" >
                                                            </div>
															
															
                                                             <div class="form-group col-sm-6">
                                                                <label for="username">Kurulum Saati</label>
                                                                <input type="time" name="saat" class="form-control baslangictarihi" rows="5"  id="datetime" >
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Kurulum Tarihi</label>
                                                                <input type="date" name="date" class="form-control bitistarihi" rows="5"  id="datetime" >
                                                            </div>
                                                            
                                                           <div class="form-group col-sm-6">
                                                                <label for="sehir">Kurum Yapılacak İl</label>
                                                                 <select name="il" class="form-control il">
																 <option value="0" selected disabled>Seçiniz</option>
                                                                 <?php foreach($iller as $yaz){  ?>
																 <option value="<?php echo $yaz->il_no; ?>"><?php echo $yaz->isim; ?></option>
																 <?php } ?>
                                                            </select>
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Kurulum Yapılacak İlçe</label>
                                                            <select name="ilce" class="form-control ilce">
                                                                <option value="0">Önce İl Seçiniz</option>
                                                                </select>
                                                            </div>
                                                            
                                                            
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Oda Sayısı</label>
                                                            <select name="odasayisi" class="form-control odasayisi">
															    <option value="0">Seçiniz</option>
                                                                <option>1+1</option>
                                                                <option>2+1</option>
                                                                <option>3+1</option>
                                                                <option>4+1</option>
                                                                <option>5+1</option>
                                                                <option>6+1</option>
                                                                <option>7+1</option>
                                                                <option>7+</option>
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Ev Kaçıncı Katta</label>
                                                            <select name="evkacincikatta" class="form-control evkacincikatta">
															<option value="0">Seçiniz</option>
                                                                <option>20</option>
                                                                <option>19</option>
                                                                <option>18</option>
                                                                <option>17</option>
                                                                <option>16</option>
                                                                <option>15</option>
                                                                <option>14</option>
                                                                <option>13</option>
                                                                <option>12</option>
                                                                <option>11</option>
                                                                <option>10</option>
                                                                <option>9</option>
                                                                <option>8</option>
                                                                <option>7</option>
                                                                <option>6</option>
                                                                <option>5</option>
                                                                <option>4</option>
                                                                <option>3</option>
                                                                <option>2</option>
                                                                <option>1</option>
                                                                <option>Zemin Kat</option>
                                                                <option>-1</option>
                                                                <option>-2</option>
                                                                <option>-3</option>
                                                                <option>-4</option>
                                                                <option>-5</option>
                                                                </select>
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                 <label for="ilce">Eşya Nasıl Taşınacak</label>
                                                            <select name="esyanasiltasinacak" class="form-control esyanasiltasinacak">
															<option value="0">Seçiniz</option>
                                                                <option value="bina merdiveni">bina merdiveni</option>
                                                                <option value="bina asansörü">bina asansörü</option>
                                                                <option value="asansör kurulmasını istiyorum">asansör kurulmasını istiyorum</option>
                                                                </select>
                                                            </div>
															
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Sigorta Şirketi</label>
                                                            <select name="sigortasirketi" class="form-control sigortasirketi">
																<option value="0">Seçiniz</option>
																<?php foreach($liste as $yaz){ ?>
																<option value="<?php echo $yaz->id; ?>"><?php echo $yaz->sigorta_adi; ?></option>
																<?php } ?>
																</select>
                                                            </div>
															
															
                                                            
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Poliçe No</label>
                                                                <input type="text" name="policeno" class="form-control policeno" rows="5"  id="datetime" >
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Telefon No</label>
                                                                <input type="text" name="tel" class="form-control tel" rows="5"  id="datetime" >
                                                            </div>
                                                             
                                                            <div class="form-group col-sm-12">
                                                                <label for="username">Adres</label>
                                                                <textarea type="text" name="adres" class="form-control adres" rows="5"  id="username" placeholder=" "></textarea>
                                                            </div>
															
															
															
															
															
															</div>
															
															
															<div class="nereden" style="margin-top:120px;">
															<h4 style="color:red; padding-left:15px;">Nereye ?</h4>
															
                                                             
                                                            
                                                           
                                                            <div class="form-group col-sm-6">
                                                                <label for="sehir">Kurum Yapılacak İl</label>
                                                                 <select name="ilen" class="form-control ilen">
																 <option value="0" selected disabled>Seçiniz</option>
                                                                 <?php foreach($iller as $yaz){  ?>
																 <option value="<?php echo $yaz->il_no; ?>"><?php echo $yaz->isim; ?></option>
																 <?php } ?>
                                                            </select>
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Kurulum Yapılacak İlçe</label>
                                                            <select name="ilcen" class="form-control ilcen">
                                                                <option value="0">Önce İl Seçiniz</option>
                                                                </select>
                                                            </div>
                                                            
                                                            
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Oda Sayısı</label>
                                                            <select name="odasayisi1" class="form-control odasayisi1">
															<option value="0" selected disabled>Seçiniz</option>
                                                                <option value="1+1">1+1</option>
                                                                <option value="2+1">2+1</option>
                                                                <option value="3+1">3+1</option>
                                                                <option value="4+1">4+1</option>
                                                                <option value="5+1">5+1</option>
                                                                <option value="6+1">6+1</option>
                                                                <option value="7+1">7+1</option>
                                                                <option value="7+">7+</option>
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Ev Kaçıncı Katta</label>
                                                            <select name="evkacincikatta1" class="form-control evkacincikatta1">
															<option value="0" selected disabled>Seçiniz</option>
                                                                <option value="20">20</option>
                                                                <option value="19">19</option>
                                                                <option value="18">18</option>
                                                                <option value="17">17</option>
                                                                <option value="16">16</option>
                                                                <option value="15">15</option>
                                                                <option value="14">14</option>
                                                                <option value="13">13</option>
                                                                <option value="12">12</option>
                                                                <option value="11">11</option>
                                                                <option value="10">10</option>
                                                                <option value="9">9</option>
                                                                <option value="8">8</option>
                                                                <option value="7">7</option>
                                                                <option value="6">6</option>
                                                                <option value="5">5</option>
                                                                <option value="4">4</option>
                                                                <option value="3">3</option>
                                                                <option value="2">2</option>
                                                                <option value="1">1</option>
                                                                <option value="Zemin Kat">Zemin Kat</option>
                                                                <option value="-1">-1</option>
                                                                <option value="-2">-2</option>
                                                                <option value="-3">-3</option>
                                                                <option value="-4">-4</option>
                                                                <option value="-5">-5</option>
                                                                </select>
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                 <label for="ilce">Eşya Nasıl Taşınacak</label>
                                                            <select name="esyanasiltasinacak1" class="form-control esyanasiltasinacak1">
															<option value="0" selected disabled>Seçiniz</option>
                                                                <option value="bina merdiveni">bina merdiveni</option>
                                                                <option value="bina asansörü">bina asansörü</option>
                                                                <option value="asansör kurulmasını istiyorum">asansör kurulmasını istiyorum</option>
                                                                </select>
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="sehir">Firma Listesi</label>
                                                                <select name="firmalistesi" class="form-control sigortasirketi">
																<option value="0">Seçiniz</option>
																<?php foreach($firmalistcek as $yaz){ ?>
																<option value="<?php echo $yaz->id; ?>"><?php echo $yaz->firma_ismi; ?></option>
																<?php } ?>
															 	</select>
                                                                </div>
															
                                                            
                                                            <div class="form-group col-sm-12">
                                                                <label for="username">Adres</label>
                                                                <textarea type="text" name="adres1" class="form-control adres1" rows="5"  id="username" placeholder=" "></textarea>
                                                            </div>
                                                             
                                                            
															
															</div>
                                                           
                                                            
                                                            <div class="form-group col-sm-12">
                                                                <button class="btn btn-raised btn-primary">Kaydet</button>
                                                            </div>
															
                                                        </div>
                                                      
                                                        
                                                    </form>
                                                </div>
                                            </div> </div>
            
          </div>
        </div>
      </div>
    </section>
	<script src="https://code.jquery.com/jquery-3.3.1.js"> </script>
		<script>
		
		$(document).ready(function(){
			
			$("form").submit(function(){
				
				var musteriadi = $(".musteriadi").val();
				var musterisoyadi = $(".musterisoyadi").val();
				var tckimlikno = $(".tckimlikno").val();
				var mail = $(".mail").val();
				var baslangictarihi = $(".baslangictarihi").val();
				var bitistarihi = $(".bitistarihi").val();
				var il = $(".il option:selected").val();
				var ilce = $(".ilce option:selected").val();
				var odasayisi = $(".odasayisi option:selected").val();
				var evkacincikatta = $(".evkacincikatta option:selected").val();
				var esyanasiltasinacak = $(".esyanasiltasinacak option:selected").val();
				var sigortasirketi = $(".sigortasirketi option:selected").val();
				var policeno = $(".policeno").val();
				var tel = $(".tel").val();
				var adres = $(".adres").val();
				var ilen = $(".ilen option:selected").val();
				var ilcen = $(".ilcen option:selected").val();
				var odasayisi1 = $(".odasayisi1 option:selected").val();
				var evkacincikatta1 = $(".evkacincikatta1 option:selected").val();
				var esyanasiltasinacak1 = $(".esyanasiltasinacak1 option:selected").val();
				var adres1 = $(".adres1").val();
				var kontrol = 0;
				
				if(musteriadi != ""){
					$(".musteriadi").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".musteriadi").css("border","1px solid red");
					kontrol--;
				}
				
				if(musterisoyadi != ""){
					$(".musterisoyadi").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".musterisoyadi").css("border","1px solid red");
					kontrol--;
				}
				
				if(tckimlikno != ""){
					$(".tckimlikno").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".tckimlikno").css("border","1px solid red");
					kontrol--;
				}
				
				if(mail != ""){
					$(".mail").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".mail").css("border","1px solid red");
					kontrol--;
				}
				
				if(baslangictarihi != ""){
					$(".baslangictarihi").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".baslangictarihi").css("border","1px solid red");
					kontrol--;
				}
				
				if(bitistarihi != ""){
					$(".bitistarihi").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".bitistarihi").css("border","1px solid red");
					kontrol--;
				}
				
				if(il != "0"){
					$(".il").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".il").css("border","1px solid red");
					kontrol--;
				}
				
				if(ilce != "0"){
					$(".ilce").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".ilce").css("border","1px solid red");
					kontrol--;
				}
				
				if(odasayisi != "0"){
					$(".odasayisi").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".odasayisi").css("border","1px solid red");
					kontrol--;
				}
				
				if(evkacincikatta != "0"){
					$(".evkacincikatta").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".evkacincikatta").css("border","1px solid red");
					kontrol--;
				}
				
				if(esyanasiltasinacak != "0"){
					$(".esyanasiltasinacak").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".esyanasiltasinacak").css("border","1px solid red");
					kontrol--;
				}
				
				if(sigortasirketi != "0"){
					$(".sigortasirketi").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".sigortasirketi").css("border","1px solid red");
					kontrol--;
				}
				
				if(policeno != ""){
					$(".policeno").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".policeno").css("border","1px solid red");
					kontrol--;
				}
				
				if(tel != ""){
					$(".tel").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".tel").css("border","1px solid red");
					kontrol--;
				}
				
				if(adres != ""){
					$(".adres").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".adres").css("border","1px solid red");
					kontrol--;
				}
				
				if(ilen != "0"){
					$(".ilen").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".ilen").css("border","1px solid red");
					kontrol--;
				}
				
				if(ilcen != "0"){
					$(".ilcen").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".ilcen").css("border","1px solid red");
					kontrol--;
				}
				
				if(odasayisi1 != "0"){
					$(".odasayisi1").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".odasayisi1").css("border","1px solid red");
					kontrol--;
				}
				
				if(evkacincikatta1 != "0"){
					$(".evkacincikatta1").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".evkacincikatta1").css("border","1px solid red");
					kontrol--;
				}
				
				if(esyanasiltasinacak1 != "0"){
					$(".esyanasiltasinacak1").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".esyanasiltasinacak1").css("border","1px solid red");
					kontrol--;
				}
				
				if(adres1 != ""){
					$(".adres1").css("border","1px solid lightgreen");
					kontrol++;
				}
				else{
					$(".adres1").css("border","1px solid red");
					kontrol--;
				}
				
				if(tckimlikno != "" && tckimlikno.length != 11){
					alert("Tc Kimlik Numaranız 11 Haneli olmalıdır.");
					$(".tckimlikno").css("border","1px solid red");
					return false;
				}
				
				if(kontrol == 21){
					return true;
				}
				
				return false;
				
			});
			
			
			$(".ara").click(function(){
				
				var key = $("#aratext").val();
				
				$.ajax({
					url:"/homeguard/yonetimpaneli/nakliyatara/"+key,
					type:"POST",
					success:function(r){
						var gelen = r.split(",");
						$(".id").val(gelen[0]);
						$(".musteriadi").val(gelen[1]);
						$(".musterisoyadi").val(gelen[2]);
						$(".tckimlikno").val(gelen[3]);
						$(".mail").val(gelen[8]);
						$(".policeno").val(gelen[4]);
						$(".adres").val(gelen[11]);
						$('.il option').eq(gelen[9]).prop('selected', true);
						$(".sigortasirketi [value="+gelen[6]+"]").attr("selected",true);
						$(".tel").val(gelen[12]);
						
						
						var il = gelen[9];
						
						$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilce").html(r);
						$(".ilce [value="+gelen[10]+"]").attr("selected",true);
					}
				    });
						
						
					}
				});
				
				
			});
			
			
			
			$(".il").change(function(){
				var il = $(".il").val();
				
				$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilce").html(r);
					}
				});
				
				
				
			});
			
			$(".ilen").change(function(){
				var il = $(".ilen").val();
				
				$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilcen").html(r);
					}
				});
				
				
				
			});
			
			
		});
		
		</script>