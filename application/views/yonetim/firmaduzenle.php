
<section id="content">
			<div class="page profile-page">
				<!-- page content -->
				<div class="pagecontent">
					<!-- row -->
					<div class="row">
                        <div style="width:1000px; height: 1000px; margin-left:50px;">
						<div role="tabpanel" class="tab-pane" id="setting" >
                                                <div class="wrap-reset">
                                                    <form class="profile-settings" method="POST" action="">
                                                        <div class="row">
														
                                                            <div class="form-group col-md-12 legend">
															<?php echo $this->session->flashdata('alert'); ?>
                                                                <h3>
                                                                    <strong>Sigorta Müşteri Düzenleme</strong> Formu</h3>
                                                                <p>Sigorta Müşteri Ekleme işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
														
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Firma İsmi</label>
                                                                <input type="text" name="firmaismi" class="form-control musteriadi" rows="5"  id="username" placeholder=" " value="<?php echo $uye->firma_ismi; ?>">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Firma Mail Adresi</label>
                                                                <input type="text" name="firmamail" class="form-control musterisoyadi" rows="5"  id="username" placeholder=" " value="<?php echo $uye->mail; ?>">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Firma Telefon no</label>
                                                                <input type="text" name="firmatelefon" class="form-control email" rows="5"  id="username" value="<?php echo $uye->telefon; ?>">
                                                            </div>
                                                            
                                                             
                                                   
                                                            <div class="form-group col-sm-12">
                                                                <button class="btn btn-raised btn-primary">Düzenle</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                                                </div>
                                            </div> </div>
						
					</div>
				</div>
			</div>
		</section>
		
<script src="http://code.jquery.com/jquery-3.3.1.js"></script>

<script>

$(document).ready(function(){
	
	
	$("form").submit(function(){
		
	var musteriadi = $(".musteriadi").val();
	var musterisoyadi = $(".musterisoyadi").val();
	var email = $(".email").val();
	var sifre = $(".sifre").val();
	var sigortasirketi = $(".sigortasirketi option:selected").val();
	var kontrol = 0;
	
	if(musteriadi == ""){
		$(".musteriadi").css("border","1px solid red");
		kontrol++;
	}
	else{
		$(".musteriadi").css("border","1px solid lightgreen");
		kontrol--;
	}
	
	if(musterisoyadi == ""){
		$(".musterisoyadi").css("border","1px solid red");
		kontrol++;
	}
	else{
		$(".musterisoyadi").css("border","1px solid lightgreen");
		kontrol--;
	}
	
	if(email == ""){
		$(".email").css("border","1px solid red");
		kontrol++;
	}
	else{
		$(".email").css("border","1px solid lightgreen");
		kontrol--;
	}
	
	if(sifre == ""){
		$(".sifre").css("border","1px solid red");
		kontrol++;
	}
	else{
		$(".sifre").css("border","1px solid lightgreen");
		kontrol--;
	}
	
	if(sigortasirketi == "0"){
		$(".sigortasirketi").css("border","1px solid red");
		kontrol++;
	}
	else{
		$(".sigortasirketi").css("border","1px solid lightgreen");
		kontrol--;
	}
	
	
	if(kontrol != -5){
		return false;
	}
		
		
	});
	
	
	
	
	
});

</script>