<?php 

$uyebilgi = $this->session->userdata("uyebilgi");

?>
<section id="content">
      <div class="page profile-page">
        <!-- page content -->
        <div class="pagecontent">
          <!-- row -->
          <div class="row">
                        <div style="width:1000px; height: 1000px; margin-left:50px;">
            <div role="tabpanel" class="tab-pane" id="setting" >
                                                <div class="wrap-reset">
                                                    <form class="profile-settings" action="" method="POST" enctype="multipart/form-data">
                                                        <div class="row">
                                                            <div class="form-group col-md-8 legend">
                                                                <h3>
                                                                    <strong>Nakliyat Randevu Düzenleme</strong> Formu</h3>
                                                                <p>Nakliyat Randevu işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
														<?php echo $this->session->flashdata('alert'); ?>
														
															<input type="hidden" class="id" name="id" value="<?php echo $bilgi->musteri_id; ?>">
															<div class="nereden" >
															<h4 style="color:red; padding-left:15px;">Nereden ?</h4>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Adı</label>
                                                                <input type="text" name="musteriadi" class="form-control musteriadi" rows="5"  id="datetime" value="<?php echo $bilgi->musteriadi; ?>">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Soyadı</label>
                                                                <input type="text" name="musterisoyadi" class="form-control musterisoyadi" rows="5"  id="datetime" value="<?php echo $bilgi->musterisoyadi; ?>">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Tc Kimlik No</label>
                                                                <input type="text" name="tckimlikno" class="form-control tckimlikno" rows="5"  id="datetime" value="<?php echo $bilgi->tckimlikno; ?>">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Mail Adresi</label>
                                                                <input type="text" name="mail" class="form-control mail" rows="5"  id="datetime" value="<?php echo $bilgi->mail; ?>">
                                                            </div>
															
															
                                                             <div class="form-group col-sm-6">
                                                                <label for="username">Kurulum Saati</label>
                                                                <input type="time" name="saat" class="form-control baslangictarihi" rows="5"  id="datetime" value="<?php echo $bilgi->kurulum_saat; ?>">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Kurulum Tarihi</label>
                                                                <input type="date" name="date" class="form-control bitistarihi" rows="5"  id="datetime" value="<?php echo $bilgi->kurulum_tarih; ?>">
                                                            </div>
                                                            
                                                           <div class="form-group col-sm-6">
                                                                <label for="sehir">Kurum Yapılacak İl</label>
                                                                 <select name="il" class="form-control il">
																 <option value="0" selected disabled>Seçiniz</option>
                                                                 <?php foreach($iller as $yaz){  ?>
																 <option value="<?php echo $yaz->il_no; ?>" <?php echo $bilgi->il == $yaz->il_no ? "selected":""; ?>><?php echo $yaz->isim; ?></option>
																 <?php } ?>
                                                            </select>
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Kurulum Yapılacak İlçe</label>
                                                            <select name="ilce" class="form-control ilce">
                                                                <option>Önce İl Seçiniz</option>
                                                                </select>
                                                            </div>
                                                            
                                                            
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Oda Sayısı</label>
                                                            <select name="odasayisi" class="form-control odasayisi">
															    <option value="0">Seçiniz</option>
                                                                <option <?php echo $bilgi1->odasayisi == "1+1" ? "selected":""; ?>>1+1</option>
                                                                <option <?php echo $bilgi1->odasayisi == "2+1" ? "selected":""; ?>>2+1</option>
                                                                <option <?php echo $bilgi1->odasayisi == "3+1" ? "selected":""; ?>>3+1</option>
                                                                <option <?php echo $bilgi1->odasayisi == "4+1" ? "selected":""; ?>>4+1</option>
                                                                <option <?php echo $bilgi1->odasayisi == "5+1" ? "selected":""; ?>>5+1</option>
                                                                <option <?php echo $bilgi1->odasayisi == "6+1" ? "selected":""; ?>>6+1</option>
                                                                <option <?php echo $bilgi1->odasayisi == "7+1" ? "selected":""; ?>>7+1</option>
                                                                <option <?php echo $bilgi1->odasayisi == "7+" ? "selected":""; ?>>7+</option>
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Ev Kaçıncı Katta</label>
                                                            <select name="evkacincikatta" class="form-control evkacincikatta">
															<option value="0">Seçiniz</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "20" ? "selected":""; ?>>20</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "19" ? "selected":""; ?>>19</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "18" ? "selected":""; ?>>18</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "17" ? "selected":""; ?>>17</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "16" ? "selected":""; ?>>16</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "15" ? "selected":""; ?>>15</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "14" ? "selected":""; ?>>14</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "13" ? "selected":""; ?>>13</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "12" ? "selected":""; ?>>12</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "11" ? "selected":""; ?>>11</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "10" ? "selected":""; ?>>10</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "9" ? "selected":""; ?>>9</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "8" ? "selected":""; ?>>8</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "7" ? "selected":""; ?>>7</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "6" ? "selected":""; ?>>6</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "5" ? "selected":""; ?>>5</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "4" ? "selected":""; ?>>4</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "3" ? "selected":""; ?>>3</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "2" ? "selected":""; ?>>2</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "1" ? "selected":""; ?>>1</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "Zemin Kat" ? "selected":""; ?>>Zemin Kat</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "-1" ? "selected":""; ?>>-1</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "-2" ? "selected":""; ?>>-2</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "-3" ? "selected":""; ?>>-3</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "-4" ? "selected":""; ?>>-4</option>
                                                                <option <?php echo $bilgi1->evkacincikatta == "-5" ? "selected":""; ?>>-5</option>
                                                                </select>
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                 <label for="ilce">Eşya Nasıl Taşınacak</label>
                                                            <select name="esyanasiltasinacak" class="form-control esyanasiltasinacak">
															<option value="0">Seçiniz</option>
                                                                <option value="bina merdiveni" <?php echo $bilgi1->esyanasiltasinacak == "bina merdiveni" ? "selected":""; ?>>bina merdiveni</option>
                                                                <option value="bina asansörü" <?php echo $bilgi1->esyanasiltasinacak == "bina asansörü" ? "selected":""; ?>>bina asansörü</option>
                                                                <option value="asansör kurulmasını istiyorum" <?php echo $bilgi1->esyanasiltasinacak == "asansör kurulmasını istiyorum" ? "selected":""; ?>>asansör kurulmasını istiyorum</option>
                                                                </select>
                                                            </div>
															
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Sigorta Şirketi</label>
                                                            <select name="sigortasirketi" class="form-control sigortasirketi">
																<?php foreach($liste as $yaz){ ?>
																<option value="<?php echo $yaz->id; ?>" <?php echo $bilgi->sigortasirketi == $yaz->id ? "selected":""; ?>><?php echo $yaz->sigorta_adi; ?></option>
																<?php } ?>
																</select>
                                                            </div>
                                                            
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Poliçe No</label>
                                                                <input type="text" name="policeno" class="form-control policeno" rows="5"  id="datetime" value="<?php echo $bilgi->policeno; ?>">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Telefon No</label>
                                                                <input type="text" name="tel" class="form-control tel" rows="5"  id="datetime" value="<?php echo $bilgi->tel; ?>">
                                                            </div>
															
															<div class="form-group col-sm-12">
                                                                <label for="sehir">Firma Listesi</label>
                                                                <select name="firmalistesi" class="form-control sigortasirketi">
																<option value="0">Seçiniz</option>
																<?php foreach($firmalistcek as $yaz){ ?>
																<option value="<?php echo $yaz->id; ?>" <?php echo $yaz->id == $bilgi->firma ? "selected":""; ?>><?php echo $yaz->firma_ismi; ?></option>
																<?php } ?>
															 	</select>
                                                                </div>
                                                             
                                                            <div class="form-group col-sm-12">
                                                                <label for="username">Adres</label>
                                                                <textarea type="text" name="adres" class="form-control adres" rows="5"  id="username"><?php echo $bilgi->adres; ?></textarea>
                                                            </div>
															
															
															
															
															
															</div>
															
															
															<div class="nereden" style="margin-top:120px;">
															<h4 style="color:red; padding-left:15px;">Nereye ?</h4>
															
                                                             
                                                            
                                                           
                                                            <div class="form-group col-sm-6">
                                                                <label for="sehir">Kurum Yapılacak İl</label>
                                                                 <select name="ilen" class="form-control ilen">
																 <option value="0" selected disabled>Seçiniz</option>
                                                                 <?php foreach($iller as $yaz){  ?>
																 <option value="<?php echo $yaz->il_no; ?>" <?php echo $bilgi1->il1 == $yaz->il_no ? "selected":""; ?>><?php echo $yaz->isim; ?></option>
																 <?php } ?>
                                                            </select>
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Kurulum Yapılacak İlçe</label>
                                                            <select name="ilcen" class="form-control ilcen">
                                                                <option>Önce İl Seçiniz</option>
                                                                </select>
                                                            </div>
                                                            
                                                            
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Oda Sayısı</label>
                                                            <select name="odasayisi1" class="form-control odasayisi1">
															<option value="0" selected disabled>Seçiniz</option>
                                                                <option <?php echo $bilgi1->odasayisi1 == "1+1" ? "selected":""; ?>>1+1</option>
                                                                <option <?php echo $bilgi1->odasayisi1 == "2+1" ? "selected":""; ?>>2+1</option>
                                                                <option <?php echo $bilgi1->odasayisi1 == "3+1" ? "selected":""; ?>>3+1</option>
                                                                <option <?php echo $bilgi1->odasayisi1 == "4+1" ? "selected":""; ?>>4+1</option>
                                                                <option <?php echo $bilgi1->odasayisi1 == "5+1" ? "selected":""; ?>>5+1</option>
                                                                <option <?php echo $bilgi1->odasayisi1 == "6+1" ? "selected":""; ?>>6+1</option>
                                                                <option <?php echo $bilgi1->odasayisi1 == "7+1" ? "selected":""; ?>>7+1</option>
                                                                <option <?php echo $bilgi1->odasayisi1 == "7+" ? "selected":""; ?>>7+</option>
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Ev Kaçıncı Katta</label>
                                                            <select name="evkacincikatta1" class="form-control evkacincikatta1">
															<option value="0" selected disabled>Seçiniz</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "20" ? "selected":""; ?>>20</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "19" ? "selected":""; ?>>19</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "18" ? "selected":""; ?>>18</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "17" ? "selected":""; ?>>17</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "16" ? "selected":""; ?>>16</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "15" ? "selected":""; ?>>15</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "14" ? "selected":""; ?>>14</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "13" ? "selected":""; ?>>13</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "12" ? "selected":""; ?>>12</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "11" ? "selected":""; ?>>11</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "10" ? "selected":""; ?>>10</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "9" ? "selected":""; ?>>9</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "8" ? "selected":""; ?>>8</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "7" ? "selected":""; ?>>7</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "6" ? "selected":""; ?>>6</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "5" ? "selected":""; ?>>5</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "4" ? "selected":""; ?>>4</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "3" ? "selected":""; ?>>3</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "2" ? "selected":""; ?>>2</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "1" ? "selected":""; ?>>1</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "Zemin Kat" ? "selected":""; ?>>Zemin Kat</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "-1" ? "selected":""; ?>>-1</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "-2" ? "selected":""; ?>>-2</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "-3" ? "selected":""; ?>>-3</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "-4" ? "selected":""; ?>>-4</option>
                                                                <option <?php echo $bilgi1->evkacincikatta1 == "-5" ? "selected":""; ?>>-5</option>
                                                                </select>
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                 <label for="ilce">Eşya Nasıl Taşınacak</label>
                                                            <select name="esyanasiltasinacak1" class="form-control esyanasiltasinacak1">
															<option value="0" selected disabled>Seçiniz</option>
                                                                <option value="bina merdiveni" <?php echo $bilgi1->esyanasiltasinacak1 == "bina merdiveni" ? "selected":""; ?>>bina merdiveni</option>
                                                                <option value="bina asansörü" <?php echo $bilgi1->esyanasiltasinacak1 == "bina asansörü" ? "selected":""; ?>>bina asansörü</option>
                                                                <option value="asansör kurulmasını istiyorum" <?php echo $bilgi1->esyanasiltasinacak1 == "asansör kurulmasını istiyorum" ? "selected":""; ?>>asansör kurulmasını istiyorum</option>
                                                                </select>
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                 <label for="ilce">Randevu Durumu</label>
                                                            <select name="onay" class="form-control onay">
                                                                <option value="0" <?php echo $bilgi->onay == "0" ? "selected":""; ?>>Onay Bekleniyor</option>
                                                                <option value="1" <?php echo $bilgi->onay == "1" ? "selected":""; ?>>Onaylanmış</option>
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="form-group col-sm-12">
                                                                <label for="username">Adres</label>
                                                                <textarea type="text" name="adres1" class="form-control adres1" rows="5"  id="username"><?php echo $bilgi1->adres1; ?></textarea>
                                                            </div>
                                                             
                                                            
															
															</div>
															
															<div class="form-group col-sm-12">
															
															<span class="btn btn-raised btn-success fileinput-button">
												<i class="glyphicon glyphicon-plus"></i>
												<span>Dosya Seç...</span>
												<input type="file" name="images[]" multiple="">
											</span>
											<br>
                                                                <?php foreach($resimcek as $resimcek){ ?>
																<li class="sa">
																
																<?php $explode = explode(".",$resimcek->resimadi);
                                                                       
																	   
																	   
																	   if($explode[1] == "docx" || $explode[1] == "doc" || $explode[1] == "pdf" || $explode[1] == "xls"){
																		   
																		   ?>
														<a href="<?php echo base_url("uploads/").$resimcek->resimadi; ?>" target="_blank"><img style="max-width:100px; border:1px solid #e1e1e1;" src="http://www.pvhc.net/img85/qygewvrmxzxdndvmruut.png"/></a>

																   <?php	   } else { ?>
																
																																
																<a href="<?php echo base_url("uploads/").$resimcek->resimadi; ?>" target="_blank"><img style="max-width:100px; border:1px solid #e1e1e1;" src="<?php echo base_url("uploads/").$resimcek->resimadi; ?>"/></a>
																   <?php } ?>
																
																
																
																<a href="javascript:;" class="sil" onclick="sil(<?php echo $resimcek->id; ?>)">sil</a>
																</li>
																<?php } ?>
                                                            </div>
																															
														<div class="form-group col-sm-12 is-empty">
                                                                <label for="message">Not Ekle: </label>
                                                                <textarea type="text" name="notekle" class="form-control adres" rows="5" id="message"><?php echo $bilgi->notekle; ?></textarea>
                                                            <span class="material-input"></span></div>
                                                           
                                                            
                                                            <div class="form-group col-sm-12">
                                                                <button class="btn btn-raised btn-primary">Kaydet</button>
                                                            </div>
															
                                                        </div>
                                                      
                                                        
                                                    </form>
                                                </div>
                                            </div> </div>
            
          </div>
        </div>
      </div>
    </section>
	<script src="https://code.jquery.com/jquery-3.3.1.js"> </script>
		<script>
		
		function sil(id){
			
			
				$.ajax({
					url:"/homeguard/yonetimpaneli/alarmrandevuresimsil/"+id,
					type:"POST",
					success: function(r){
						
					}
				});
			}
		
		$(document).ready(function(){
			
			$(".sil").click(function(){
				var a = $(".sil").index(this);
				$(".sa").eq(a).hide();
			});
			
			var il = $(".il option:selected").val();
			
			$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilce").html(r);
						$(".ilce [value="+gelen[10]+"]").attr("selected",true);
					}
				    });
					
					
					
					var il = $(".ilen option:selected").val();
				
				$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilcen").html(r);
					}
				});
					
			
			
			$("form").submit(function(){
				
				var tckimlik = $("#aratext").val();
				
				if(tckimlik == ""){
					alert("Tc Kimlik no boş olamaz.");
					return false;
				}
				if(tckimlik < 11){
					alert("Tc kimlik no 11 hane olmalıdır.");
					return false;
				}
				
				
			});
			
			
			$(".ara").click(function(){
				
				var key = $("#aratext").val();
				
				$.ajax({
					url:"/homeguard/yonetimpaneli/nakliyatara/"+key,
					type:"POST",
					success:function(r){
						var gelen = r.split(",");
						$(".id").val(gelen[0]);
						$(".musteriadi").val(gelen[1]);
						$(".musterisoyadi").val(gelen[2]);
						$(".tckimlikno").val(gelen[3]);
						$(".mail").val(gelen[8]);
						$(".policeno").val(gelen[4]);
						$(".adres").val(gelen[11]);
						$('.il option').eq(gelen[9]).prop('selected', true);
						$(".sigortasirketi [value="+gelen[6]+"]").attr("selected",true);
						$(".tel").val(gelen[12]);
						
						
						var il = gelen[9];
						
						$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilce").html(r);
						$(".ilce [value="+gelen[10]+"]").attr("selected",true);
					}
				    });
						
						
					}
				});
				
				
			});
			
			
			
			$(".il").change(function(){
				var il = $(".il").val();
				
				$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilce").html(r);
					}
				});
				
				
				
			});
			
			$(".ilen").change(function(){
				var il = $(".ilen").val();
				
				$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilcen").html(r);
					}
				});
				
				
				
			});
			
			
		});
		
		</script>