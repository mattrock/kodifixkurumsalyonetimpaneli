
        <section id="content">
            <div class="page profile-page">
                <!-- page content -->
                <div class="pagecontent">
                    <!-- row -->
                    <div class="row">
                        <div style="width:1000px; height: 1000px; margin-left:50px;">
                        <div role="tabpanel" class="tab-pane" id="setting" >
                                                <div class="wrap-reset">
                                                    <form class="profile-settings">
                                                        <div class="row">
                                                            <div class="form-group col-md-12 legend">
                                                                <h3>
                                                                    <strong>Randevu Ekleme</strong> Formu</h3>
                                                                <p>Randevu Ekleme işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            
                                                          <div class="form-group col-sm-4" style="margin-top: 50px">
                                                                <a href="/homeguard/yonetimpaneli/alarmrandevu" class="btn btn-raised btn-primary">Alarm Randevu</a>
                                                            </div>
                                                            <div class="form-group col-sm-4" style="margin-top: 50px">
                                                                <a href="/homeguard/yonetimpaneli/nakliyatrandevu" class="btn btn-raised btn-primary">Nakliyat Randevu</a>
                                                            </div>
                                                            <div class="form-group col-sm-4" style="margin-top: 50px">
                                                                <a href="/homeguard/yonetimpaneli/montajrandevu" class="btn btn-raised btn-primary">Montaj Randevu</a>
                                                            </div>
                                                            <div class="form-group col-sm-4" style="margin-top: 50px">
                                                                <a href="/homeguard/yonetimpaneli/haliyikamarandevu" class="btn btn-raised btn-primary">Halı Yıkama Randevu</a>
                                                            </div>
                                                            <div class="form-group col-sm-4" style="margin-top: 50px">
                                                                <a href="/homeguard/yonetimpaneli/piskomaxrandevu" class="btn btn-raised btn-primary">Piskomax Randevu</a>
                                                            </div>
                                                            <div class="form-group col-sm-4" style="margin-top: 50px">
                                                                <a href="/homeguard/yonetimpaneli/hasarerandevu" class="btn btn-raised btn-primary">Haşere Randevu</a>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                                                </div>
                                            </div> </div>
                        
                    </div>
                </div>
            </div>
        </section>
        