	<!--  CONTENT  -->
		<section id="content">
			<div class="page page-tables-footable">
				<!-- bradcome -->
				<div class="b-b mb-10">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="h3 m-0">ANKET LİSTESİ</h1>
						</div>
					</div>
				</div>

				<!-- row -->
				<div class="row">
				<?php echo $this->session->flashdata('alert'); ?>
					<div class="col-md-12">
					<button class="btn btn-raised btn-success" data-toggle="modal" data-target="#myModal">Anket Ekle<div class="ripple-container"></div></button>
						<section class="boxs ">
							<div class="boxs-header">
							</div>
							<div class="boxs-body">
								<div class="form-group">
									<label for="filter" style="padding-top: 5px">Arama:</label>
                                    <input id="filter" type="text" class="form-control rounded w-md mb-10 inline-block">
                                     <div class="btn-group" style="margin-left: 50px">
                                                <button type="button" class="btn btn-raised btn-success btn-sm" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-search"></i> </button>
                                            </div>
								</div> <br><br>

                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<thead>
										<tr>
											<th>id</th>
                                            <th>Anket Sorusu</th>
                                            <th>Anket Tipi</th>
                                            <th>Anket Sonucu</th>
                                            <th>İşlemler</th>
										</tr>
									</thead>
									<tbody>
										
										<?php foreach($veriler as $yaz){  ?>
                                        <tr>
                                            <td><?php echo $yaz->id; ?></td>
                                            <td><?php echo $yaz->anketsorusu; ?></td>
                                            <td><?php echo $yaz->soru_tipi == "1" ? "Evet / Hayır":"1 / 5"; ?></td>
                                            <td style=""><a href="<?php echo base_url("yonetimpaneli/anketsonuc/{$yaz->id}"); ?>" target="_blank" class="sonucum label label-default">Sonucu Göster</a></td>
                                            
                                            
                                            
                                            <td> <a href="javascript:;" onclick="sil(<?php echo $yaz->id; ?>);" class="label label-primary">Sil</a> </td>

                                        </tr>
                                        <?php } ?>
									</tfoot>
								</table>
							</div>
							<div class="pagination">
							<?php echo $linkler; ?>
							</div>
						</section>
					</div>
				</div>
				
			</div>
			
		</section>
		
		<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Hızlı Bak</h4>
                </div>
                <div class="modal-body">
                    <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<thead>
										<tr>
											<th>Ad Soyad</th>
											<th>Tc Kimlik No</th>
											<th>Mail</th>
											<th>Şifre</th>
											<th>Telefon</th>
											<th>Sigorta Şirketi</th>
											<th>il</th>
											<th>ilçe</th>
											<th>Poliçe No</th>
											<th>adres</th>
											
                                            
										</tr>
									</thead>
									<tbody class="yaz">
									
									
									
										
									
                                        
                                     
                                       </tbody>
                                        
									</tfoot>
								</table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>
	
	<div class="modal fade" id="myModal" role="dialog" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Müşteri Ekleme Formu</h4>
                </div>
                <div class="modal-body">
                    <form class="profile-settings" name="arama" method="POST" action="<?php echo base_url("yonetimpaneli/anketsorusuekle"); ?>">
                                                        <div class="row">
														<?php echo $this->session->flashdata('alert'); ?>
                                                            <div class="form-group col-md-12 legend">
                                                                <h3>
                                                                    <strong>Anket Ekleme</strong> Formu</h3>
                                                                <p>Anket ekleme işlemlerini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-sm-12">
                                                                <label for="username">Anket Sorusu</label>
                                                                <textarea name="anketsorusu" class="form-control" rows="5" ></textarea>
                                                            </div>
                                                            
                                                           <div class="form-group col-sm-12">
                                                                <label for="username">Soru Tipi</label>
                                                                <select class="form-control" name="sorutipi"> 
																<option value="0" selected disabled>Seçiniz</option>
																<option value="1">Evet Hayır</option>
																<option value="2">1 ve 5</option>
																</select>
                                                            </div>
                                        
                                                            

                                                            <div class="form-group col-sm-12">
                                                                <button class="btn btn-raised btn-primary">Anketi Ekle</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat<div class="ripple-container"><div class="ripple ripple-on ripple-out" style="left: 43.875px; top: 22px; background-color: rgba(0, 0, 0, 0.87); transform: scale(9.375);"></div><div class="ripple ripple-on ripple-out" style="left: 63.875px; top: 22px; background-color: rgba(0, 0, 0, 0.87); transform: scale(9.375);"></div></div></button>
                </div>
            </div>
        </div>
    </div>
	
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		
		<script>
		
		function hizlibak(id){
			
			$.ajax({
			url:"/homeguard/yonetimpaneli/hizlibak3/"+id,
				type:"POST",
			    data:{id:id},
				success:function(r){
					$(".yaz").html(r);
				}
			});
			
		}
		
	 $(document).ready(function() {
       $('.sonucum').click(function() {
        window.open($(this).attr('href'),'title', 'width=550, height=700');
        return false;
       });   
       });
		
		function sil(id){
				
				if (confirm('Silmek istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/anketsil/"+id);
                } 
                else {
                console.log("no");
                }
				
			}
		
	
		
		</script>