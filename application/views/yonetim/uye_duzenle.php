
<section id="content">
			<div class="page profile-page">
				<!-- page content -->
				<div class="pagecontent">
					<!-- row -->
					<div class="row">
                        <div style="width:1000px; height: 1000px; margin-left:50px;">
						<div role="tabpanel" class="tab-pane" id="setting" >
                                                <div class="wrap-reset">
                                                    <form class="profile-settings" name="arama" method="POST" action="">
                                                        <div class="row">
                                                            <div class="form-group col-md-12 legend">
                                                                <h3>
                                                                    <strong>Üye Düzenleme</strong> Formu</h3>
                                                                <p>Buradan istediğiniz gibi üye bilgilerini düzenleyebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Üye Ad Soyad</label>
                                                                <input type="text" name="uyeadsoyad" class="form-control uyeadsoyad" rows="5"  id="username" placeholder="" value="<?php echo $uye->uye_adsoyad; ?>">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Üye Email</label>
                                                                <input type="text" name="uyeemail" class="form-control uyeemail" rows="5"  id="username" placeholder=" " value="<?php echo $uye->uye_email; ?>">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Üye Telefon</label>
                                                                <input type="text" name="uyetelefon" class="form-control uyetelefon" rows="5"  id="username" value="<?php echo $uye->uye_telefon; ?>">
                                                            </div>
                                                              <div class="form-group col-sm-6">
                                                                 <label for="ilce">Üye Rütbe</label>
                                                            <select name="uyerutbe" class="form-control uyerutbe">
                                                                
                                                                <option value="1" <?php echo $uye->uye_rank == 1 ? "selected":""; ?>>Yönetici</option>
                                                                </select>
                                                            </div>
                                                             
                                                             <div class="form-group col-sm-12">
                                                                <label for="phone">Üye Şifre : </label>
                                                                <input type="password" name="sifre" id="phone" class="form-control uyesifre" value="<?php echo $uye->uye_sifre; ?>" placeholder="" data-parsley-trigger="change">
                                                            </div>
                                                            

                                                            <div class="form-group col-sm-12">
                                                                <button class="btn btn-raised btn-primary">Üye Düzenle</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                                                </div>
                                            </div> </div>
						
					</div>
				</div>
			</div>
		</section>
		<script src="http://code.jquery.com/jquery-3.3.1.slim.js"> </script>
		
		<script type="text/javascript">
		
		$(document).ready(function(){
			
			$("form").submit(function(){
				
				var uyeadsoyad = $(".uyeadsoyad").val();
				var uyeemail = $(".uyeemail").val();
				var uyetelefon = $(".uyetelefon").val();
				var uyerutbe = $(".uyerutbe option:selected").val();
				var uyesifre = $(".uyesifre").val();
				var kontrol = 0;
				
				if($(".uyeadsoyad").val() == ""){
					$(".uyeadsoyad").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".uyeadsoyad").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if($(".uyeemail").val() == ""){
					$(".uyeemail").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".uyeemail").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if($(".uyetelefon").val() == ""){
					$(".uyetelefon").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".uyetelefon").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if($(".uyerutbe option:selected").val() == "0"){
					$(".uyerutbe").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".uyerutbe").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if($(".uyesifre").val() == ""){
					$(".uyesifre").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".uyesifre").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if(kontrol != -5){
					return false;
				}
				
				
				
			});
			
			
		});
		
		</script>