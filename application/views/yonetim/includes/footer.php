    <script src="<?php echo base_url("assets/"); ?>assets\bundles\libscripts.bundle.js"></script>
    <script src="<?php echo base_url("assets/"); ?>assets\bundles\vendorscripts.bundle.js"></script>

    <!--/ vendor javascripts -->
    <script src="<?php echo base_url("assets/"); ?>assets\bundles\flotscripts.bundle.js"></script>    
    <script src="<?php echo base_url("assets/"); ?>assets\bundles\d3cripts.bundle.js"></script>
    <script src="<?php echo base_url("assets/"); ?>assets\bundles\sparkline.bundle.js"></script>
    <script src="<?php echo base_url("assets/"); ?>assets\bundles\raphael.bundle.js"></script>
    <script src="<?php echo base_url("assets/"); ?>assets\bundles\morris.bundle.js"></script>
    <script src="<?php echo base_url("assets/"); ?>assets\bundles\loadercripts.bundle.js"></script>

    <!-- page Js -->
    <script src="<?php echo base_url("assets/"); ?>assets\bundles\mainscripts.bundle.js"></script>
    <script src="<?php echo base_url("assets/"); ?>assets\js\page\index.js"></script>

	<style>
	
	.pagination a {
    padding: 10px;
    border: 1px solid #e1e1e1;
    margin-right: 3px;
}

.pagination strong {
    border: 1px solid #e1e1e1;
    padding: 10px;
    margin-right: 3px;
}

.pagination {
    border-radius: 0;
    width: 100%;
    text-align: center;
}


.modal-lg{
	width:1200px;
}

.sa{
																	float:left;
																	list-style:none;
																	margin-right:10px;
																}
																
																.sa > a{
																	display:block;
																	text-align:center;
																}
															

	
	</style>
	
</body>
</html>