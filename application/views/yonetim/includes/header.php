<?php $uyebilgi = $this->session->userdata("uyebilgi"); ?>
<!doctype html>
<html class="no-js " lang="tr">
<head>
<meta charset="utf-8">

<title>Sigorta Sistemi Yönetim Paneli</title>
<link rel="icon" type="image/ico" href="assets\images\favicon.ico">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- vendor css files -->
<link rel="stylesheet" href="<?php echo base_url("assets/"); ?>assets\js\vendor\bootstrap\bootstrap.min.css">    
<link rel="stylesheet" href="<?php echo base_url("assets/"); ?>assets\css\vendor\animsition.min.css">
<link rel="stylesheet" href="<?php echo base_url("assets/"); ?>assets\js\vendor\morris\morris.css">    
<!-- project main css files -->
<link rel="stylesheet" href="<?php echo base_url("assets/"); ?>assets\css\main.css">
</head>
<body id="falcon" class="main_Wrapper">
    <div id="wrap" class="animsition">
        <!-- HEADER Content -->
        <div id="header">
            <header class="clearfix">
                <!-- Branding -->
                <div class="branding">
                    <a class="brand" href="<?php echo base_url('/yonetimpaneli'); ?>">
                        <span>Sigorta Sistemi</span>
                    </a>
                    <a role="button" tabindex="0" class="offcanvas-toggle visible-xs-inline">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <!-- Branding end -->

                <!-- Left-side navigation -->
                <ul class="nav-left pull-left list-unstyled list-inline">
                    <li class="leftmenu-collapse">
                        <a role="button" tabindex="0" class="collapse-leftmenu">
                            <i class="fa fa-outdent"></i>
                        </a>
                    </li>
                   
                    
                </ul>
                

                <!-- Right-side navigation -->
                <ul class="nav-right pull-right list-inline">
                    
                    
                    
					
                    <li class="dropdown nav-profile">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo base_url("assets/"); ?>assets\images\profile-photo.jpg" alt="" class="0 size-30x30"> </a>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <div class="user-info">
                                    <div class="user-name"><?php echo @$uyebilgi->uye_adsoyad; echo @$uyebilgi->musteri_adi ." ". @$uyebilgi->musteri_soyadi; ?></div>
                                    <div class="user-position online"><?php echo @$uyebilgi->uye_adsoyad == null ? "Müşteri":"Yönetici"; ?></div>
                                </div>
                            </li>
                            
                            <li>
                                <a href="<?php echo base_url(); ?>/yonetimpaneli/cikis" role="button" tabindex="0">
                                    <i class="fa fa-sign-out"></i>Çıkış</a>
                            </li>
                        </ul>
                    </li>
                    
                </ul>
                
            </header>
        </div>