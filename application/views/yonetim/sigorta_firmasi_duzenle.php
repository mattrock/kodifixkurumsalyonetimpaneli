
<section id="content">
			<div class="page profile-page">
				<!-- page content -->
				<div class="pagecontent">
					<!-- row -->
					<div class="row">
                        <div style="width:1000px; height: 1000px; margin-left:50px;">
						<div role="tabpanel" class="tab-pane" id="setting" >
                                                <div class="wrap-reset">
                                                    <form class="profile-settings" method="POST" action="">
                                                        <div class="row">
                                                            <div class="form-group col-md-12 legend">
																<?php echo $this->session->flashdata('alert'); ?>
                                                                <h3>
                                                                    <strong>Sigorta Firması Ekleme</strong> Formu</h3>
                                                                <p>Sigorta Firması Ekleme işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Sigorta Şirket Adı</label>
                                                                <input type="text" name="sigortasirketi" class="form-control" rows="5" value="<?php echo $uye->sigorta_adi; ?>"  id="username" placeholder=" ">
                                                            </div>
                                                            
                                                            
                                                            

                                                            <div class="form-group col-sm-12">
                                                                <button type="submit" class="btn btn-raised btn-primary">Düzenle</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                                                </div>
                                            </div> </div>
						
					</div>
				</div>
			</div>
		</section>
		<script src="https://code.jquery.com/jquery-3.3.1.js"> </script>
		<script>
		
		$(document).ready(function(){
			
			
			$("form").submit(function(){
				
				var sigortasirketi = $(".sigortasirketi").val();
				var mail = $(".mail").val();
				var sifre = $(".sifre").val();
				var kontrol = 0;
				
				if(sigortasirketi == ""){
					$(".sigortasirketi").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".sigortasirketi").css("border","1px solid lightgreen");
					kontrol--;
				}
				
	
				if(kontrol != -1){
					return false;
				}
				
			});
			
			
		});
		
		</script>