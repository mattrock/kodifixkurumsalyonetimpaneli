<script src="<?php echo base_url("dist"); ?>/dropzone.js"></script>
<link href="<?php echo base_url("dist"); ?>/dropzone.css" rel="stylesheet"/>
		<section id="content">
			<div class="page page-tables-footable">
				<!-- bradcome -->
				<div class="b-b mb-10">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="h3 m-0">RANDEVU LİSTESİ</h1>
						</div>
					</div>
				</div>

				<!-- row -->
				<div class="row">
					<div class="col-md-12">
					<?php echo $this->session->flashdata("alert"); ?>
						<section class="boxs ">
							<div class="boxs-header">
							</div>
							<div class="boxs-body">
							<div class="form-group col-sm-6">
									<select  onchange="location = this.value;">
									<option value="0">Filtreleme Yap</option>
                                    
									<option value="/homeguard/yonetimpaneli/randevulistesi/alarmrandevux" <?php echo $this->uri->segment(3) == "alarmrandevux" ? "selected":""; ?>>Alarm Randevu</option>
									<option value="/homeguard/yonetimpaneli/randevulistesi/haliyikamax" <?php echo $this->uri->segment(3) == "haliyikamax" ? "selected":""; ?>>Halı Yıkama Randevu</option>
									<option value="/homeguard/yonetimpaneli/randevulistesi/hasaretrandevux" <?php echo $this->uri->segment(3) == "hasaretrandevux" ? "selected":""; ?>>Haşaret Randevu</option>
									<option value="/homeguard/yonetimpaneli/randevulistesi/montajrandevux" <?php echo $this->uri->segment(3) == "montajrandevux" ? "selected":""; ?>>Montaj Randevu</option>
									<option value="/homeguard/yonetimpaneli/randevulistesi/nakliyatrandevux" <?php echo $this->uri->segment(3) == "nakliyatrandevux" ? "selected":""; ?>>Nakliyat Randevu</option>
									<option value="/homeguard/yonetimpaneli/randevulistesi/pskimoaxrandevux" <?php echo $this->uri->segment(3) == "pskimoaxrandevux" ? "selected":""; ?>>Psikomax Randevu</option>
                                   
                                    </select>
								</div> 
								<form action="<?php echo base_url('yonetimpaneli/randevuarama'); ?>" method="POST">
								<div class="form-group col-sm-6">
									<label for="filter" style="padding-top: 5px">Arama:</label>
                                    <input id="filter" type="text" name="key" class="form-control rounded w-md mb-10 inline-block">
                                     <div class="btn-group" style="margin-left: 50px">
                                                <button type="submit" class="btn btn-raised btn-success btn-sm"> <i class="fa fa-search"></i> </button>
                                            </div>
								</div> <br><br>
                               </form>
							   
                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<?php if(@$veriler != null){ ?>
									<thead>
										<tr>
											<th>id</th>
                                            <th>Adı Soyadı</th>
											<th>Telefon</th>
											<th>Tarih</th>
                                            <th>Sigorta Şirketi</th>
                                            <th>Police no</th>
                                            <th>Randevu Tipi</th>
                                            <th>İşlemler</th>
										</tr>
									</thead>
									<tbody>
									
									
									
										<tr>
										
										<?php foreach($veriler as $yaz){ $sigortasirketi = $this->sql->sigortafirmalistesinecek($yaz->sigortasirketi);  ?>
										     
											 <?php  $now = date("Y-m-d"); $gelentarih = explode("T",$yaz->kurulum_tarih);  $before = date("Y-m-d",strtotime("-1 day"));  
											 
											 
											 
											 
											 
											 ?>
										  
                                            <td <?php echo $now == $gelentarih[0] ? "style='background-color:lightgreen; color:#fff;'; ":""; echo $before == $gelentarih[0] ? "style='background-color:lightgray; color:#fff;'; ":""; ?>><?php echo $yaz->id; ?></td>
                                            <td <?php echo $now == $gelentarih[0] ? "style='background-color:lightgreen; color:#fff;'; ":""; echo $before == $gelentarih[0] ? "style='background-color:lightgray; color:#fff;'; ":""; ?>><?php echo $yaz->musteriadi . " ".$yaz->musterisoyadi; ?></td>
                                            <td <?php echo $now == $gelentarih[0] ? "style='background-color:lightgreen; color:#fff;'; ":""; echo $before == $gelentarih[0] ? "style='background-color:lightgray; color:#fff;'; ":""; ?>><?php echo $yaz->tel; ?></td>
                                            <td <?php echo $now == $gelentarih[0] ? "style='background-color:lightgreen; color:#fff;'; ":""; echo $before == $gelentarih[0] ? "style='background-color:lightgray; color:#fff;'; ":""; ?>><?php echo str_replace("T"," -> ",$yaz->kurulum_tarih); ?></td>
                                            <td <?php echo $now == $gelentarih[0] ? "style='background-color:lightgreen; color:#fff;'; ":""; echo $before == $gelentarih[0] ? "style='background-color:lightgray; color:#fff;'; ":""; ?>><?php echo @$sigortasirketi->sigorta_adi; ?></td>
                                            <td <?php echo $now == $gelentarih[0] ? "style='background-color:lightgreen; color:#fff;'; ":""; echo $before == $gelentarih[0] ? "style='background-color:lightgray; color:#fff;'; ":""; ?>><?php echo $yaz->policeno; ?></td>
                                            <td <?php echo $now == $gelentarih[0] ? "style='background-color:lightgreen; color:#fff;'; ":""; echo $before == $gelentarih[0] ? "style='background-color:lightgray; color:#fff;'; ":""; ?>><?php echo $yaz->randevu_tipi == "alarmrandevu" ? "alarm randevu":""; echo $yaz->randevu_tipi == "haliyikamarandevu" ? "halı yıkama randevu":""; echo $yaz->randevu_tipi == "hasarerandevu" ? "haşaret Randevu":""; echo $yaz->randevu_tipi == "psikomaxrandevu" ? "psikomax randevu":""; echo $yaz->randevu_tipi == "montajrandevu" ? "montaj randevu":""; echo $yaz->randevu_tipi == "nakliyatrandevu" ? "nakliyat randevu":""; ?></td>

										<td ><a href="javascript:;" onclick="<?php echo $yaz->randevu_tipi == "psikomaxrandevu" ? "psikomaxrandevusil":" "; echo $yaz->randevu_tipi == "nakliyatrandevu" ? "nakliyatrandevusil":" "; echo $yaz->randevu_tipi == "alarmrandevu" ? "alarmrandevusil":" "; echo $yaz->randevu_tipi == "haliyikamarandevu" ? "haliyikamarandevusil":" "; echo $yaz->randevu_tipi == "montajrandevu" ? "montajrandevusil":" "; echo $yaz->randevu_tipi == "hasarerandevu" ? "hasaretrandevusil":" "; ?>(<?php echo $yaz->id; ?>);" class="label label-primary">Sil</a>  <a href="<?php echo $yaz->randevu_tipi == "psikomaxrandevu" ? "/homeguard/yonetimpaneli/psikomaxrandevuduzenle/{$yaz->id}":""; echo $yaz->randevu_tipi == "nakliyatrandevu" ? "/homeguard/yonetimpaneli/nakliyatrandevuduzenle/{$yaz->id}":""; echo $yaz->randevu_tipi == "montajrandevu" ? "/homeguard/yonetimpaneli/montajrandevuduzenle/{$yaz->id}":""; echo $yaz->randevu_tipi == "hasarerandevu" ? "/homeguard/yonetimpaneli/hasaretrandevuduzenle/{$yaz->id}":""; echo $yaz->randevu_tipi == "haliyikamarandevu" ? "/homeguard/yonetimpaneli/haliyikamaduzenle/{$yaz->id}":""; echo $yaz->randevu_tipi == "alarmrandevu" ? "/homeguard/yonetimpaneli/alarmrandevuduzenle/{$yaz->id}":""; ?>" class="label label-danger">Düzenle</a> <a href="javascript:;" data-toggle="modal" data-target="#myModal2" onclick="<?php echo $yaz->randevu_tipi == "psikomaxrandevu" ? "psikomaxrandevubak":""; echo $yaz->randevu_tipi == "nakliyatrandevu" ? "nakliyatrandevubak":""; echo $yaz->randevu_tipi == "alarmrandevu" ? "alarmbak":""; echo $yaz->randevu_tipi == "haliyikamarandevu" ? "haliyikamabak":""; echo $yaz->randevu_tipi == "hasarerandevu" ? "hasaretrandevubak":""; echo $yaz->randevu_tipi == "montajrandevu" ? "montajrandevubak":""; ?>(<?php echo $yaz->id; ?>);" class="label label-default">Hızlı Bak</a> <?php if($yaz->randevu_tipi == "alarmrandevu"){ echo $yaz->onay == 0 ? '<a href="javascript:;" onclick="onay('. $yaz->id .');" class="label label-warning">Onayla</a>':'<span class="label label-success">Onaylanmış</span>';  } ?> <?php if($yaz->randevu_tipi == "haliyikamarandevu"){ echo $yaz->onay == 0 ? '<a href="javascript:;" onclick="onay1('. $yaz->id .');" class="label label-warning">Onayla</a>':'<span class="label label-success">Onaylanmış</span>';  } ?> <?php if($yaz->randevu_tipi == "hasarerandevu"){ echo $yaz->onay == 0 ? '<a href="javascript:;" onclick="onay2('. $yaz->id .');" class="label label-warning">Onayla</a>':'<span class="label label-success">Onaylanmış</span>';  } ?> <?php if($yaz->randevu_tipi == "montajrandevu"){ echo $yaz->onay == 0 ? '<a href="javascript:;" onclick="onay3('. $yaz->id .');" class="label label-warning">Onayla</a>':'<span class="label label-success">Onaylanmış</span>';  } ?> <?php if($yaz->randevu_tipi == "nakliyatrandevu"){ echo $yaz->onay == 0 ? '<a href="javascript:;" onclick="onay4('. $yaz->id .');" class="label label-warning">Onayla</a>':'<span class="label label-success">Onaylanmış</span>';  } ?> <?php if($yaz->randevu_tipi == "psikomaxrandevu"){ echo $yaz->onay == 0 ? '<a href="javascript:;" onclick="onay5('. $yaz->id .');" class="label label-warning">Onayla</a>':'<span class="label label-success">Onaylanmış</span>';  } ?>   </td>
                                        </tr>
										<?php  } } else { echo "&nbsp;&nbsp;&nbsp;&nbsp;Görmek İstediğiniz Randevu Tipini Belirtin."; } ?>
                                       
                                     
                                       
                                        
									</tfoot>
								</table>
							</div>
							<div class="pagination">
							<?php echo @$linkler; ?>
							</div>
						</section>
					</div>
				</div>
			</div>
		</section>
		
		<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Hızlı Bak</h4>
                </div>
                <div class="modal-body">
                    <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<thead>
									
									</thead>
									<tbody class="yaz">
									
									
									
										
									
                                        
                                     
                                       </tbody>
                                        
									</tfoot>
								</table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>
		
		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		
		<script>
		
		function alarmbak(id){
			
			$.ajax({
			url:"/homeguard/yonetimpaneli/alarmbak/"+id,
				type:"POST",
			    data:{id:id},
				success:function(r){
					$(".yaz").html(r);
				}
			});
			
		}
		
		function haliyikamabak(id){
			
			$.ajax({
			url:"/homeguard/yonetimpaneli/haliyikamabak/"+id,
				type:"POST",
			    data:{id:id},
				success:function(r){
					$(".yaz").html(r);
				}
			});
			
		}
		
		function hasaretrandevubak(id){
			
			$.ajax({
			url:"/homeguard/yonetimpaneli/hasaretrandevubak/"+id,
				type:"POST",
			    data:{id:id},
				success:function(r){
					$(".yaz").html(r);
				}
			});
			
		}
		
		function montajrandevubak(id){
			
			$.ajax({
			url:"/homeguard/yonetimpaneli/montajrandevubak/"+id,
				type:"POST",
			    data:{id:id},
				success:function(r){
					$(".yaz").html(r);
				}
			});
			
		}
		
		function psikomaxrandevubak(id){
			
			$.ajax({
			url:"/homeguard/yonetimpaneli/psikomaxrandevubak/"+id,
				type:"POST",
			    data:{id:id},
				success:function(r){
					$(".yaz").html(r);
				}
			});
			
		}
		
		function nakliyatrandevubak(id){
			
			$.ajax({
			url:"/homeguard/yonetimpaneli/nakliyatrandevubak/"+id,
				type:"POST",
			    data:{id:id},
				success:function(r){
					$(".yaz").html(r);
				}
			});
			
		}
		
		
		function alarmrandevusil(id){
				
				if (confirm('Silmek istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/alarmrandevusil/"+id);
                } 
                else {
                console.log("no");
                }
				
			}
			
			function psikomaxrandevusil(id){
				
				if (confirm('Silmek istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/psikomaxrandevusil/"+id);
                } 
                else {
                console.log("no");
                }
				
			}
			
			function nakliyatrandevusil(id){
				
				if (confirm('Silmek istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/nakliyatrandevusil/"+id);
                } 
                else {
                console.log("no");
                }
				
			}
			
			function montajrandevusil(id){
				
				if (confirm('Silmek istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/montajrandevusil/"+id);
                } 
                else {
                console.log("no");
                }
				
			}
			
		function haliyikamarandevusil(id){
				
				if (confirm('Silmek istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/haliyikamarandevusil/"+id);
                } 
                else {
                console.log("no");
                }
				
			}
			
			function hasaretrandevusil(id){
				
				if (confirm('Silmek istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/hasaretrandevusil/"+id);
                } 
                else {
                console.log("no");
                }
				
			}
			
			function onay(id){
				if (confirm('Onaylamak istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/onayrandevualarm/"+id);
                } 
                else {
                console.log("no");
                }
			}
			
			function onay1(id){
				if (confirm('Onaylamak istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/onayrandevuhaliyikama/"+id);
                } 
                else {
                console.log("no");
                }
			}
			
			function onay2(id){
				if (confirm('Onaylamak istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/onayrandevuhasaret/"+id);
                } 
                else {
                console.log("no");
                }
			}
			
			function onay3(id){
				if (confirm('Onaylamak istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/onaymontajrandevu/"+id);
                } 
                else {
                console.log("no");
                }
			}
			
			function onay4(id){
				if (confirm('Onaylamak istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/onaynakliyatrandevu/"+id);
                } 
                else {
                console.log("no");
                }
			}
			
			function onay5(id){
				if (confirm('Onaylamak istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/onaypsikomaxrandevu/"+id);
                } 
                else {
                console.log("no");
                }
			}
		
	
		
		</script>
		
		<!--/ CONTENT -->