
<section id="content">
			<div class="page profile-page">
				<!-- page content -->
				<div class="pagecontent">
					<!-- row -->
					<div class="row">
                        <div style="width:1000px; height: 1000px; margin-left:50px;">
						<div role="tabpanel" class="tab-pane" id="setting" >
                                                <div class="wrap-reset">
                                                    <form class="profile-settings" name="arama" method="POST" action="">
                                                        <div class="row">
                                                            <div class="form-group col-md-12 legend">
                                                                <h3>
                                                                    <strong>Müşteri Düzenleme</strong> Formu</h3>
                                                                <p>Müşteri düzenleme işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
														<?php echo $this->session->flashdata('alert'); ?>
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Adı</label>
                                                                <input type="text" name="musteriadi" class="form-control musteriadi" rows="5"  id="username" value="<?php echo $musteri->musteri_adi; ?>">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Soyadı</label>
                                                                <input type="text" name="musterisoyadi" class="form-control musterisoyadi" rows="5"  id="username" value="<?php echo $musteri->musteri_soyadi; ?>">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">TC Kimlik No</label>
                                                                <input type="text" name="tckimlikno" class="form-control tckimlikno" rows="5"  id="username" value="<?php echo $musteri->tc_kimlik_no; ?>">
                                                            </div>
                                                             <div class="form-group col-sm-6">
                                                                <label for="username">Poliçe No</label>
                                                                <input type="text" name="policeno" class="form-control policeno" rows="5"  id="username" value="<?php echo $musteri->police_no; ?>">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="phone">E-Mail : </label>
                                                                <input type="text" name="mail" id="phone" class="form-control email" placeholder="email adresiniz" data-parsley-trigger="change" value="<?php echo $musteri->mail; ?>"  required="">
                                                            </div>
                                                             
															
															
															<div class="form-group col-sm-6">
                                                                <label for="phone">Telefon : </label>
                                                                <input type="text" name="telefon" id="phone" class="form-control telefon" placeholder="(XXX) XXXX XXX" value="<?php echo $musteri->telefon; ?>" data-parsley-trigger="change"  required="">
                                                            </div>
															
															<div class="form-group col-sm-12">
                                                                <label for="ilce">Sigorta Şirketi</label>
																<select name="sigortasirketi" class="form-control sigortasirketi">
                                                                <?php foreach($liste as $yaz){ ?>
																<option value="<?php echo $yaz->id; ?>"><?php echo $yaz->sigorta_adi; ?></option>
																<?php } ?>
																</select>
                                                            </div>
                                                           
														   <div class="form-group col-sm-6" style="clear: both">
                                                                <label for="sehir">Kurum Yapılacak İl</label>
                                                                 <select name="il" class="form-control il">
																 <option value="0" selected disabled>Seçiniz</option>
                                                                 <?php foreach($iller as $yaz){  ?>
																 <option value="<?php echo $yaz->il_no; ?>" <?php echo $musteri->il == $yaz->il_no ? "selected":""; ?>><?php echo $yaz->isim; ?></option>
																 <?php } ?>
                                                            </select>
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Kurulum Yapılacak İlçe</label>
                                                            <select name="ilce" class="form-control ilce">
                                                                <option value="0">Önce İl Seçiniz</option>
                                                                </select>
                                                            </div>
                                                            
															
                                                            <div class="form-group col-sm-12">
                                                                <label for="message">Adres Detay: </label>
                                                                <textarea type="text" name="adres" class="form-control adres" rows="5" name="message" id="message"><?php echo $musteri->adres; ?></textarea>
                                                            </div>
                                                            

                                                            <div class="form-group col-sm-12">
                                                                <button class="btn btn-raised btn-primary">Düzenle</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                                                </div>
                                            </div> </div>
						
					</div>
				</div>
			</div>
		</section>
		<script src="http://code.jquery.com/jquery-3.3.1.js"> </script>
		<script>
		
		$(document).ready(function(){
			
			var il = $(".il").val();
				
				$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilce").html(r);
						$(".ilce [value=<?php echo $musteri->ilce; ?>]").attr("selected",true);
					}
				});
			
			$("form").submit(function(){
				
				var musteriadi = $(".musteriadi").val();
				var musterisoyadi = $(".musterisoyadi").val();
				var tckimlikno = $(".tckimlikno").val();
				var policeno = $(".policeno").val();
				var email = $(".email").val();
				var telefon = $(".telefon").val();
				var sigortasirketi = $(".sigortasirketi option:selected").val();
				var il = $(".il option:selected").val();
				var ilce = $(".ilce option:selected").val();
				var adres = $(".adres").val();
				
				var kontrol = 0;

				if(musteriadi == ""){
					$(".musteriadi").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".musteriadi").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if(musterisoyadi == ""){
					$(".musterisoyadi").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".musterisoyadi").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if(tckimlikno == ""){
					$(".tckimlikno").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".tckimlikno").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if(policeno == ""){
					$(".policeno").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".policeno").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if(email == ""){
					$(".email").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".email").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if(telefon == ""){
					$(".telefon").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".telefon").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if(sigortasirketi == "0"){
					$(".sigortasirketi").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".sigortasirketi").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if(il == "0"){
					$(".il").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".il").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if(ilce == "0"){
					$(".ilce").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".ilce").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if(adres == ""){
					$(".adres").css("border","1px solid red");
					kontrol++;
				}
				else{
					$(".adres").css("border","1px solid lightgreen");
					kontrol--;
				}
				
				if(tckimlikno != "" && tckimlikno.length < 11){
					alert("Tc Kimlik Numaranız 11 Haneli olmalıdır.");
					$(".tckimlikno").css("border","1px solid red");
					return false;
				}
				
				
				if(kontrol != -10){
					console.log("geçemezsin.");
					return false;
					
				}
				
				
				
				
			});
			
		
		$(".il").change(function(){
				var il = $(".il").val();
				
				$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilce").html(r);
					}
				});
				
				
				
			});
			});
		
		
		</script>