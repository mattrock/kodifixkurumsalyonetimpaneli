

		<section id="content">
		
			<div class="page page-tables-footable">
				
				<div class="b-b mb-10">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="h3 m-0">SİGORTA FİRMA LİSTESİ</h1>
						</div>
					</div>
				</div>

				
				<div class="row">
				
				
					<div class="col-md-12">
					<?php echo $this->session->flashdata('alert'); ?>
					
					<button class="btn btn-raised btn-success" data-toggle="modal" data-target="#myModal">Firma Ekle<div class="ripple-container"></div></button>

					
					
					
						<section class="boxs ">
							<div class="boxs-header">
							</div>
							<div class="boxs-body">
							<form action="<?php echo base_url("yonetimpaneli/firmaara"); ?>" method="POST">
								<div class="form-group">
									<label for="filter" style="padding-top: 5px">Arama:</label>
                                    <input id="filter" type="text" name="key" class="form-control rounded w-md mb-10 inline-block">
                                     <div class="btn-group" style="margin-left: 50px">
                                                <button type="submit" class="btn btn-raised btn-success btn-sm"> <i class="fa fa-search"></i> </button>
                                            </div>
								</div> <br><br>
                                </form>
								
							<?php if($veriler != null){ ?>	
                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<thead>
										<tr>
											<th>İd</th>
                                            <th>Firma Adı</th>
											<th>Mail</th>
											<th>Telefon</th>
                                            <th>İşlemler</th>
										</tr>
									</thead>
									
									<tbody>
									
									<?php foreach($veriler as $yazdir) { ?>
										<tr>
                                            <td><?php echo $yazdir->id; ?></td>
                                            <td><?php echo $yazdir->firma_ismi; ?></td>
                                            <td><?php echo $yazdir->mail; ?></td>
                                            <td><?php echo $yazdir->telefon; ?></td>
                                            <td> <a href="javascript:;" onclick="sil(<?php echo $yazdir->id; ?>)" class="label label-primary">Sil</a>  <a href="/homeguard/yonetimpaneli/firmaduzenle/<?php echo $yazdir->id; ?>" class="label label-danger">Düzenle</a> </td>
                                        </tr>
										  <?php  } ?>
                                      
                                        
									</tfoot>
									
								</table>
							<?php } else { ?>
							
							<h5>Aradığınız Firma adıyla ilgili kayıt bulunamadı.</h5>
							<?php } ?>
									
							</div>
							<div class="pagination">
							<?php echo $linkler; ?>
							</div>
							
						</section>
					</div>
					
				</div>
			</div>
		</section>
		<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Hızlı Bak</h4>
                </div>
                <div class="modal-body">
                    <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<thead>
										<tr>
											<th>Ad-Soyad</th>
                                            <th>TC Kimlik No</th>
											<th>Poliçe No</th>
											<th>Sigorta Şirketi</th>
                                            <th>Telefon</th>
                                            <th>mail</th>
                                            <th>il</th>
                                            <th>ilçe</th>
                                            <th>Adres</th>
                                            
										</tr>
									</thead>
									<tbody class="yaz">
									
									
									
										
									
                                        
                                     
                                       </tbody>
                                        
									</tfoot>
								</table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>
	
	<div class="modal fade" id="myModal" role="dialog" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Firma Ekleme Formu</h4>
                </div>
                <div class="modal-body">
                    <form class="profile-settings" method="POST" action="<?php echo base_url("yonetimpaneli/firmalistesi"); ?>">
                                                        <div class="row">
                                                            <div class="form-group col-md-12 legend">
																                                                                <h3>
                                                                    <strong>Firma Ekleme</strong> Formu</h3>
                                                                <p>Firma Ekleme işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-sm-12 is-empty">
                                                                <label for="username">Firma İsmi</label>
                                                                <input type="text" name="firmaismi" class="sigortasirketi form-control" rows="5" id="username" placeholder=" ">
                                                            <span class="material-input"></span></div>
                                                            
															<div class="form-group col-sm-12 is-empty">
                                                                <label for="username">Firma Mail Adresi</label>
                                                                <input type="text" name="firmamail" class="firmamail form-control" rows="5" id="username" placeholder=" ">
                                                            <span class="material-input"></span></div>
                                                           
														   <div class="form-group col-sm-12 is-empty">
                                                                <label for="username">Firma Telefon No</label>
                                                                <input type="text" name="firmatelefon" class="firmamail form-control" rows="5" id="username" placeholder=" ">
                                                            <span class="material-input"></span></div>
															
															
															
                                                            

                                                            <div class="form-group col-sm-12">
                                                                <button type="submit" class="btn btn-raised btn-primary">Ekle</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat<div class="ripple-container"><div class="ripple ripple-on ripple-out" style="left: 43.875px; top: 22px; background-color: rgba(0, 0, 0, 0.87); transform: scale(9.375);"></div><div class="ripple ripple-on ripple-out" style="left: 63.875px; top: 22px; background-color: rgba(0, 0, 0, 0.87); transform: scale(9.375);"></div></div></button>
                </div>
            </div>
        </div>
    </div>
		
		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		
		<script>
		
		function hizlibak(id){
			
			$.ajax({
			url:"/homeguard/yonetimpaneli/hizlibak1/"+id,
				type:"POST",
			    data:{id:id},
				success:function(r){
					$(".yaz").html(r);
				}
			});
			
		}
		
		
		function sil(id){
				
				if (confirm('Silmek istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/firmasil/"+id);
                } 
                else {
                console.log("no");
                }
				
			}
		
	
		
		</script>