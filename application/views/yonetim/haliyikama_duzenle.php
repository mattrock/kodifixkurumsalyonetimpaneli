<?php 

$uyebilgi = $this->session->userdata("uyebilgi");

?>
<section id="content">
			<div class="page profile-page">
				<!-- page content -->
				<div class="pagecontent">
					<!-- row -->
					<div class="row">
                        <div style="width:1000px; height: 1000px; margin-left:50px;">
						<div role="tabpanel" class="tab-pane" id="setting" >
                                                <div class="wrap-reset">
                                                    <form class="profile-settings" name="arama" method="POST" action="" enctype="multipart/form-data">
                                                        <div class="row">
                                                            <div class="form-group col-md-12 legend">
                                                                <h3>
                                                                    <strong>Halı Yıkama Randevu</strong> Düzenleme Formu</h3>
                                                                <p>Halı Yıkama Randevu işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
														<?php echo $this->session->flashdata('alert'); ?>
                                                          
														
															<input type="hidden" name="id" class="id" value="<?php echo $bilgi->musteri_id; ?>">
															<div class="form-group col-sm-6" style="clear: both">
                                                                <label for="username">Müşteri Adı</label>
                                                                <input type="text" name="musteriadi" class="form-control musteriadi" rows="5"  id="username" value="<?php echo $bilgi->musteriadi; ?>">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Soyadı</label>
                                                                <input type="text" name="musterisoyadi" class="form-control musterisoyadi" rows="5"  id="username" value="<?php echo $bilgi->musterisoyadi; ?>">
                                                            </div>
                                                            
                                                           <div class="form-group col-sm-6" style="clear: both">
                                                                <label for="sehir">İl</label>
                                                                 <select name="il" class="form-control il">
                                                            <option selected="selected" value="0" style="background-color:#FFCC00;">İl Seçiniz</option>
                                                           <?php foreach($iller as $yaz){  													   
														    ?>
														   <option value="<?php echo $yaz->il_no; ?>" <?php echo $yaz->il_no == $bilgi->il ? "selected":""; ?>> <?php echo $yaz->isim; ?></option>
														   <?php } ?>
															</select>
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">İlçe</label>
                                                            <select name="ilce" class="form-control ilce">
                                                                <option>Önce İl Seçiniz</option>
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">TC Kimlik No</label>
                                                                <input type="text" name="tckimlik" class="form-control tckimlik" rows="5"  id="username" value="<?php echo $bilgi->tckimlikno; ?>">
                                                            </div>
                                                             <div class="form-group col-sm-6">
                                                                <label for="username">Poliçe No</label>
                                                                <input type="text" name="police" class="form-control policeno" rows="5"  id="username"  value="<?php echo $bilgi->policeno; ?>">
                                                            </div>
                                                             
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Mail Adresi</label>
                                                                <input type="text" name="mail" class="form-control mail" rows="5"  id="username" value="<?php echo $bilgi->mail; ?>">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Telefon</label>
                                                                <input type="tel" name="tel" class="form-control telefon" rows="5"  id="username" value="<?php echo $bilgi->tel; ?>">
                                                            </div>
															
                                                               <div class="form-group col-sm-6">
                                                                <label for="username">Kurulum Tarihi</label>
                                                                <input type="date" name="kurulumtarih" class="form-control kurulumtarih" rows="5"  id="datetime" value="<?php echo $bilgi->kurulum_tarih; ?>">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Kurulum Saat</label>
                                                                <input type="time" name="saat" class="form-control saat" rows="5"  id="datetime" value="<?php echo $bilgi->kurulum_saat; ?>">
                                                            </div>
															
															
															<div class="form-group col-sm-6">
                                                                 <label for="ilce">Sigorta Şirketi</label>
                                                            <select name="sigortasirketi" class="form-control sigortasirketi">
                                                                <?php foreach($liste as $yaz){ ?>
			                                                   <option value="<?php echo $yaz->id; ?>" <?php echo $yaz->id == $bilgi->sigortasirketi ? "selected":"" ?>><?php echo $yaz->sigorta_adi; ?></option>
																<?php } ?>
                                                                </select>
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                 <label for="ilce">Randevu Durumu</label>
                                                            <select name="onay" class="form-control onay">
                                                                <option value="0" <?php echo $bilgi->onay == "0" ? "selected":""; ?>>Onay Bekleniyor</option>
                                                                <option value="1" <?php echo $bilgi->onay == "1" ? "selected":""; ?>>Onaylanmış</option>
                                                                </select>
                                                            </div>
															
															<div class="form-group col-sm-12">
                                                                <label for="sehir">Firma Listesi</label>
                                                                <select name="firmalistesi" class="form-control sigortasirketi">
																<option value="0">Seçiniz</option>
																<?php foreach($firmalistcek as $yaz){ ?>
																<option value="<?php echo $yaz->id; ?>" <?php echo $yaz->id == $bilgi->firma ? "selected":""; ?>><?php echo $yaz->firma_ismi; ?></option>
																<?php } ?>
															 	</select>
                                                                </div>
															
															<div class="form-group col-sm-12">
                                                                <label for="message">Adres Detay: </label>
                                                                <textarea name="adres" class="form-control adres" rows="5" name="message" id="message"><?php echo $bilgi->adres; ?></textarea>
                                                            </div>
															
															
															
															<div class="form-group col-sm-12">
															
															<span class="btn btn-raised btn-success fileinput-button">
												<i class="glyphicon glyphicon-plus"></i>
												<span>Dosya Seç...</span>
												<input type="file" name="images[]" multiple="">
											</span>
											<br>
                                                                <?php foreach($resimcek as $resimcek){ ?>
																<li class="sa">
																<a href="<?php echo base_url("uploads/").$resimcek->resimadi; ?>" target="_blank"><img style="max-width:100px; border:1px solid #e1e1e1;" src="<?php echo base_url("uploads/").$resimcek->resimadi; ?>"/></a>
																
																<a href="javascript:;" class="sil" onclick="sil(<?php echo $resimcek->id; ?>)">sil</a>
																</li>
																<?php } ?>
                                                            </div>
															
															<div class="form-group col-sm-12">
                                                                <label for="message">Not Ekle: </label>
                                                                <textarea type="text" name="notekle" class="form-control" rows="5" name="message" id="message"><?php echo $bilgi->notekle; ?></textarea>
                                                            </div>
															
															
                                                            <div class="form-group col-sm-6">
                                                                <button class="btn btn-raised btn-primary">Kaydet</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                                                </div>
                                            </div> </div>
						
					</div>
				</div>
			</div>
		</section>
		<script src="https://code.jquery.com/jquery-3.3.1.js"> </script>
		<script>
		
		function sil(id){
			
			
				$.ajax({
					url:"/homeguard/yonetimpaneli/haliyikamarandevuresimsil/"+id,
					type:"POST",
					success: function(r){
						
					}
				});
			}
		
		$(document).ready(function(){
			
			$(".sil").click(function(){
				var a = $(".sil").index(this);
				$(".sa").eq(a).hide();
			});
			
			var il = $(".il").val();
			
			if(il != 0){
				$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilce").html(r);
					}
				});
			}
			
			
			$(".ara").click(function(){
				
				var key = $("#aratext").val();
				
				$.ajax({
					url:"/homeguard/yonetimpaneli/haliyikamaara/"+key,
					type:"POST",
					success:function(r){
						var gelen = r.split(",");
						$(".id").val(gelen[0]);
						$(".musteriadi").val(gelen[1]);
						$(".musterisoyadi").val(gelen[2]);
						$(".il option:selected").html(gelen[3]);
						$(".il option:selected").val(gelen[3]);
						$(".ilce option:selected").html(gelen[4]);
						$(".ilce option:selected").val(gelen[4]);
						$(".tckimlik").val(gelen[5]);
						$(".policeno").val(gelen[6]);
						$(".mail").val(gelen[7]);
						$(".telefon").val(gelen[8]);
						$(".adres").val(gelen[9]);
						$(".sigortasirketi option:selected").html(gelen[11]);
						$(".sigortasirketi option:selected").val(gelen[10]);
						$(".sigortasirketi option:not(:selected)").hide();
					}
				});
				
				
			});
			
			
			
			$(".il").change(function(){
				var il = $(".il").val();
				
				$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilce").html(r);
					}
				});
				
				
				
			});
			
			
			
			
		});
		
		</script>