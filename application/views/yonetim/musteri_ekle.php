
<section id="content">
			<div class="page profile-page">
				<!-- page content -->
				<div class="pagecontent">
					<!-- row -->
					<div class="row">
                        <div style="width:1000px; height: 1000px; margin-left:50px;">
						<div role="tabpanel" class="tab-pane" id="setting" >
                                                <div class="wrap-reset">
                                                    <form class="profile-settings" name="arama" method="POST" action="">
                                                        <div class="row">
                                                            <div class="form-group col-md-12 legend">
                                                                <h3>
                                                                    <strong>Müşteri Ekleme</strong> Formu</h3>
                                                                <p>Müşteri Ekleme işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
														<?php echo $this->session->flashdata('alert'); ?>
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Adı</label>
                                                                <input type="text" name="musteriadi" class="form-control musteriadi" rows="5"  id="username">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Soyadı</label>
                                                                <input type="text" name="musterisoyadi" class="form-control musterisoyadi" rows="5"  id="username">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">TC Kimlik No</label>
                                                                <input type="text" name="tckimlikno" class="form-control tckimlikno" rows="5"  id="username">
                                                            </div>
                                                             <div class="form-group col-sm-6">
                                                                <label for="username">Poliçe No</label>
                                                                <input type="text" name="policeno" class="form-control policeno" rows="5"  id="username">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="phone">E-Mail : </label>
                                                                <input type="text" name="mail" id="phone" class="form-control email" placeholder="email adresiniz" data-parsley-trigger="change">
                                                            </div>
                                                             
															
															
															<div class="form-group col-sm-6">
                                                                <label for="phone">Telefon : </label>
                                                                <input type="text" name="telefon" id="phone" class="form-control telefon" placeholder="(XXX) XXXX XXX" data-parsley-trigger="change">
                                                            </div>
															
															<div class="form-group col-sm-12">
                                                                <label for="ilce">Sigorta Şirketi</label>
																<select class="form-control sigortasirketi" name="sigortasirketi">
																<option value="0" selected disabled>Seçiniz</option>
                                                                <?php foreach($liste as $yaz){ ?>
																<option value="<?php echo $yaz->id; ?>"><?php echo $yaz->sigorta_adi; ?></option>
																<?php } ?>
																</select>
                                                            </div>
                                                           
														   
														   <div class="form-group col-sm-6" style="clear: both">
                                                                <label for="sehir">Kurum Yapılacak İl</label>
                                                                 <select name="il" class="form-control il">
																 <option value="0" selected disabled>Seçiniz</option>
                                                                 <?php foreach($iller as $yaz){  ?>
																 <option value="<?php echo $yaz->il_no; ?>"><?php echo $yaz->isim; ?></option>
																 <?php } ?>
                                                            </select>
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">Kurulum Yapılacak İlçe</label>
                                                            <select name="ilce" class="form-control ilce">
                                                                <option value="0">Önce İl Seçiniz</option>
                                                                </select>
                                                            </div>
                                                            
                                                            
                                                            <div class="form-group col-sm-12">
                                                                <label for="message">Adres Detay: </label>
                                                                <textarea type="text" name="adres" class="form-control adres" rows="5" name="message" id="message"></textarea>
                                                            </div>
                                                            

                                                            <div class="form-group col-sm-12">
                                                                <button class="btn btn-raised btn-primary">Müşterİ Ekle</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                                                </div>
                                            </div> </div>
						
					</div>
				</div>
			</div>
		</section>
		<script src="http://code.jquery.com/jquery-3.3.1.js"> </script>
		<script>
		
		$(document).ready(function(){
			$("form").submit(function(){
				
				var musteriadi = $(".musteriadi").val();
				var musterisoyadi = $(".musterisoyadi").val();
				var tckimlikno = $(".tckimlikno").val();
				var policeno = $(".policeno").val();
				var email = $(".email").val();
				var telefon = $(".telefon").val();
				var sigortasirketi = $(".sigortasirketi option:selected").val();
				var il = $(".il option:selected").val();
				var ilce = $(".ilce option:selected").val();
				var adres = $(".adres").val();
				
				

				if(musteriadi == ""){
					$(".musteriadi").css("border","1px solid red");
					
				}
				else{
					$(".musteriadi").css("border","1px solid lightgreen");
					
				}
				
				if(musterisoyadi == ""){
					$(".musterisoyadi").css("border","1px solid red");
					
				}
				else{
					$(".musterisoyadi").css("border","1px solid lightgreen");
					
				}
				
				if(tckimlikno == ""){
					$(".tckimlikno").css("border","1px solid red");
					
				}
				else{
					$(".tckimlikno").css("border","1px solid lightgreen");
					
				}
				
				if(policeno == ""){
					$(".policeno").css("border","1px solid red");
					
				}
				else{
					$(".policeno").css("border","1px solid lightgreen");
					
				}
				
				if(email == ""){
					$(".email").css("border","1px solid red");
					
				}
				else{
					$(".email").css("border","1px solid lightgreen");
					
				}
				
				if(telefon == ""){
					$(".telefon").css("border","1px solid red");
					
				}
				else{
					$(".telefon").css("border","1px solid lightgreen");
					
				}
				
				if(sigortasirketi == "0"){
					$(".sigortasirketi").css("border","1px solid red");
					
				}
				else{
					$(".sigortasirketi").css("border","1px solid lightgreen");
					
				}
				
				if(il == "0"){
					$(".il").css("border","1px solid red");
					
				}
				else{
					$(".il").css("border","1px solid lightgreen");
					
				}
				
				if(ilce == "0"){
					$(".ilce").css("border","1px solid red");
					
				}
				else{
					$(".ilce").css("border","1px solid lightgreen");
					
				}
				
				if(adres == ""){
					$(".adres").css("border","1px solid red");
					
				}
				else{
					$(".adres").css("border","1px solid lightgreen");
					
				}
				
				if(tckimlikno != "" && tckimlikno.length != 11){
					alert("Tc Kimlik Numaranız 11 Haneli olmalıdır.");
					$(".tckimlikno").css("border","1px solid red");
					
				}
				
				if(musteriadi == "" || musterisoyadi == "" || tckimlikno.length != 11 || tckimlikno == "" || policeno == "" || email == "" || telefon == "" || sigortasirketi == "" || il == "" || ilce == "" || adres == ""){
					return false
				}
				
				
				
				
				
				
			});
			
		
		$(".il").change(function(){
				var il = $(".il").val();
				
				$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilce").html(r);
					}
				});
				
				
				
			});
			});
		
		
		</script>