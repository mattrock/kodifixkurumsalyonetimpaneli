<?php 

$uyebilgi = $this->session->userdata("uyebilgi");

?>
<section id="content">
			<div class="page profile-page">
				<!-- page content -->
				<div class="pagecontent">
					<!-- row -->
					<div class="row">
                        <div style="width:1000px; height: 1000px; margin-left:50px;">
						<div role="tabpanel" class="tab-pane" id="setting" >
                                                <div class="wrap-reset">
                                                    <form class="profile-settings" name="arama" method="POST" action="">
                                                        <div class="row">
                                                            <div class="form-group col-md-12 legend">
                                                                <h3>
                                                                    <strong>Haşere Randevu</strong> Formu</h3>
                                                                <p>Haşere Randevu işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
														<?php echo $this->session->flashdata('alert'); ?>
                                                          <?php if(@$uyebilgi->uyetipi == "musteri"){ ?>
														
														<div class="form-group col-sm-6" >
                                                                <a href="javascript:;" class="btn btn-raised btn-primary ara">Bilgilerimi Otomatik Getir.</a>
																<input type="hidden" name="" class="form-control" value="<?php echo @$uyebilgi->tc_kimlik_no; ?>" rows="5"  id="aratext" placeholder="Tc kimlik no ile ara">
                                                            </div>
														<?php } else { ?>
                                                          <div class="form-group col-sm-6">
                                                                <label for="username"></label>
                                                                <input type="text" name="" class="form-control" rows="5"  id="aratext" placeholder="Tc kimlik no ile ara">
                                                            </div>
														
                                                            <div class="form-group col-sm-6" >
                                                                <a href="javascript:;" class="btn btn-raised btn-primary ara">Ara</a>
                                                            </div>
														<?php } ?>
															<input type="hidden" name="id" class="id" value="">
															<div class="form-group col-sm-6" style="clear: both">
                                                                <label for="username">Müşteri Adı</label>
                                                                <input type="text" name="musteriadi" class="form-control musteriadi" rows="5"  id="username" >
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Soyadı</label>
                                                                <input type="text" name="musterisoyadi" class="form-control musterisoyadi" rows="5"  id="username" >
                                                            </div>
                                                            
                                                           <div class="form-group col-sm-6" style="clear: both">
                                                                <label for="sehir">Kurum Yapılacak İl</label>
                                                                 <select name="il" class="form-control il">
                                                            <option selected="selected" value="0" style="background-color:#FFCC00;">İl Seçiniz</option>
                                                           <?php foreach($iller as $yaz){  													   
														    ?>
														   <option value="<?php echo $yaz->il_no; ?>" > <?php echo $yaz->isim; ?></option>
														   <?php } ?>
															</select>
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                 <label for="ilce">İlçe</label>
                                                            <select name="ilce" class="form-control ilce">
                                                                <option value="0">Önce İl Seçiniz</option>
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">TC Kimlik No</label>
                                                                <input type="text" name="tckimlik" class="form-control tckimlik" rows="5"  id="username" >
                                                            </div>
                                                             <div class="form-group col-sm-6">
                                                                <label for="username">Poliçe No</label>
                                                                <input type="text" name="policeno" class="form-control policeno" rows="5"  id="username" >
                                                            </div>
                                                             
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Mail Adresi</label>
                                                                <input type="text" name="mail" class="form-control mail" rows="5"  id="username" >
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Telefon</label>
                                                                <input type="tel" name="telefon" class="form-control telefon" rows="5"  id="username" >
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Randevu Saati</label>
                                                                <input type="time" name="saat" class="form-control saat" rows="5"  id="datetime" >
                                                            </div>
															
                                                               <div class="form-group col-sm-6">
                                                                <label for="username">Randevu Tarihi</label>
                                                                <input type="date" name="kurulumtarih" class="form-control date" rows="5"  id="datetime" >
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                 <label for="ilce">Sigorta Şirketi</label>
                                                            <select name="sigortasirketi" class="form-control sigortasirketi">
															    <option value="0">Seçiniz</option>
                                                                <?php foreach($liste as $yaz){ ?>
			                                                   <option value="<?php echo $yaz->id; ?>" ><?php echo $yaz->sigorta_adi; ?></option>
																<?php } ?>
                                                                </select>
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="sehir">Firma Listesi</label>
                                                                <select name="firmalistesi" class="form-control sigortasirketi">
																<option value="0">Seçiniz</option>
																<?php foreach($firmalistcek as $yaz){ ?>
																<option value="<?php echo $yaz->id; ?>"><?php echo $yaz->firma_ismi; ?></option>
																<?php } ?>
															 	</select>
                                                                </div>
															
															<div class="form-group col-sm-12">
                                                                <label for="message">Adres Detay: </label>
                                                                <textarea name="adres" class="form-control adres" rows="5" name="message" id="message"></textarea>
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <button class="btn btn-raised btn-primary">Kaydet</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                                                </div>
                                            </div> </div>
						
					</div>
				</div>
			</div>
		</section>
		<script src="https://code.jquery.com/jquery-3.3.1.js"> </script>
		<script>
		
		$(document).ready(function(){
			
			$("form").submit(function(){
				
				var musteriadi = $(".musteriadi").val();
				var musterisoyadi = $(".musterisoyadi").val();
				var il = $(".il option:selected").val();
				var ilce = $(".ilce option:selected").val();
				var tckimlik = $(".tckimlik").val();
				var policeno = $(".policeno").val();
				var mail = $(".mail").val();
				var telefon = $(".telefon").val();
				var saat = $(".saat").val();
				var date = $(".date").val();
				var sigortasirketi = $(".sigortasirketi").val();
				var adres = $(".adres").val();
				var kontrol = 0;
				
				if(musteriadi == ""){
					$(".musteriadi").css("border","1px solid red");
					kontrol--;
				}
				else{
					$(".musteriadi").css("border","1px solid lightgreen");
					kontrol++;
				}
				
				if(musterisoyadi == ""){
					$(".musterisoyadi").css("border","1px solid red");
					kontrol--;
				}
				else{
					$(".musterisoyadi").css("border","1px solid lightgreen");
					kontrol++;
				}
				
				if(il == 0){
					$(".il").css("border","1px solid red");
					kontrol--;
				}
				else{
					$(".il").css("border","1px solid lightgreen");
					kontrol++;
				}
				
				if(ilce == 0){
					$(".ilce").css("border","1px solid red");
					kontrol--;
				}
				else{
					$(".ilce").css("border","1px solid lightgreen");
					kontrol++;
				}
				
				if(tckimlik == "" || tckimlik < 11){
					$(".tckimlik").css("border","1px solid red");
					kontrol--;
				}
				else{
					$(".tckimlik").css("border","1px solid lightgreen");
					kontrol++;
				}
				
				if(policeno == ""){
					$(".policeno").css("border","1px solid red");
					kontrol--;
				}
				else{
					$(".policeno").css("border","1px solid lightgreen");
					kontrol++;
				}
				
				if(mail == ""){
					$(".mail").css("border","1px solid red");
					kontrol--;
				}
				else{
					$(".mail").css("border","1px solid lightgreen");
					kontrol++;
				}
				
				if(telefon == ""){
					$(".telefon").css("border","1px solid red");
					kontrol--;
				}
				else{
					$(".telefon").css("border","1px solid lightgreen");
					kontrol++;
				}
				
				if(saat == ""){
					$(".saat").css("border","1px solid red");
					kontrol--;
				}
				else{
					$(".saat").css("border","1px solid lightgreen");
					kontrol++;
				}
				
				if(date == ""){
					$(".date").css("border","1px solid red");
					kontrol--;
				}
				else{
					$(".date").css("border","1px solid lightgreen");
					kontrol++;
				}
				
				if(sigortasirketi == "0"){
					$(".sigortasirketi").css("border","1px solid red");
					kontrol--;
				}
				else{
					$(".sigortasirketi").css("border","1px solid lightgreen");
					kontrol++;
				}
				
				if(adres == ""){
					$(".adres").css("border","1px solid red");
					kontrol--;
				}
				else{
					$(".adres").css("border","1px solid lightgreen");
					kontrol++;
				}
				
				if(tckimlik != "" && tckimlik.length != 11){
					alert("Tc Kimlik Numaranız 11 Haneli olmalıdır.");
					$(".tckimlik").css("border","1px solid red");
					return false;
				}
				
				
				
				if(kontrol == 12){
					console.log(kontrol);
					return true;
				}
					
					
					
					
					
				return false;
				
				
			});
			
			
			$(".ara").on("click",function(){
				
				var key = $("#aratext").val();
				
				$.ajax({
					url:"/homeguard/yonetimpaneli/alarmrandevuara/"+key,
					type:"POST",
					success:function(r){
						var gelen = r.split(",");
						$(".id").val(gelen[0]);
						$('.il option').eq(gelen[1]).prop('selected', true);
						$(".adres").html(gelen[3]);
						$(".tckimlik").val(gelen[4]);
						$(".policeno").val(gelen[5]);
						$(".sigortasirketi [value="+gelen[8]+"]").attr("selected",true);
						$(".mail").val(gelen[7]);
						$(".musteriadi").val(gelen[9]);
						$(".musterisoyadi").val(gelen[10]);
						$(".telefon").val(gelen[11]);
						
						var il = gelen[1];
						
						$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilce").html(r);
						$(".ilce [value="+gelen[2]+"]").attr("selected",true);
					}
				    });
					
					
						
					}
				});
				
				
			});
			
			
			
			$(".il").change(function(){
				var il = $(".il").val();
				
				$.ajax({
					url:"/homeguard/yonetimpaneli/ilce/"+il,
					type:"POST",
					data:{il:il},
					success: function(r){
						$(".ilce").html(r);
					}
				});
				
				
				
			});
			
			
		});
		
		</script>