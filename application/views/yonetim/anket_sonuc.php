<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.js"> </script>

<div class="chart-container" style="position: relative; height:45vh; width:45vw">
<canvas id="myChart" width="100" height="100"></canvas>
</div>

<table>

<th>
id
</th>
<th>
Müşteri Ad
</th>
<th>
Müşteri Soyad
</th>
<th>
Müşteri Mail
</th>
<th>
Müşteri Oy
</th>

<?php 

$evet = 0; 
$hayir = 0; 

$vote1 = 0;
$vote2 = 0;
$vote3 = 0;
$vote4 = 0;
$vote5 = 0;

?>

<?php foreach($sorulist as $yaz) {  $sorutipi = $this->sql->anketsorugetir($yaz->anket_id);  ?>
        
<tr>
<td><?php echo $yaz->id; ?></td>
<td><?php echo $yaz->musteriadi; ?></td>
<td><?php echo $yaz->musterisoyadi; ?></td>
<td><?php echo $yaz->musteri_mail; ?></td>
<td><?php if($sorutipi->soru_tipi == 1) { $ankettoplam = $this->sql->toplamanketsay($yaz->anket_id,$yaz->musteri_mail); if($yaz->cevap1 != 1) { echo "hayır"; $hayir++; } else { echo "evet"; $evet++; } } else { echo $yaz->vote;  if($yaz->vote == 1){ $vote1++;  } if($yaz->vote == 2){ $vote2++; } if($yaz->vote == 3){ $vote3++; } if($yaz->vote == 4){ $vote4++; } if($yaz->vote == 5){ $vote5++; } $ankettoplam = $this->sql->toplamanketsay1($yaz->anket_id,$yaz->musteri_mail);   }  ?></td>
</tr>



<?php  } ?>

<th colspan="5">Toplam Ankete Katılan Kişi = <?php echo $ankettoplam; ?></th>

</table>
<style>

table ,tr,td,th{
	border:1px solid #e1e1e1;
	padding:5px;
}

</style>

<script>

<?php if($sorutipi->soru_tipi == 1){ ?>

var ctx = document.getElementById('myChart');

var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ["Hayır","Evet"],
        datasets: [{
            label: '# of Votes',
            data: [<?php echo $hayir; ?>,<?php echo $evet; ?>],
            backgroundColor: [
                'rgb(220, 220, 220)',
                'rgb(0, 255, 0)',
            ],
            borderColor: [
                'rgb(220, 220, 220)',
                'rgb(0, 255, 0)',
            ],
            borderWidth: 1
        }]
    },
    options: { }
});

<?php } else { ?>

var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["1", "2", "3", "4", "5"],
        datasets: [{
            label: 'Grafik',
            data: [<?php echo $vote1.",".$vote2.",".$vote3.",".$vote4.",".$vote5; ?>],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
               
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
               
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

<?php } ?>


</script>