


		<section id="content">
			<div class="page page-tables-footable" >
				<!-- bradcome -->
				<div class="b-b mb-10">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="h3" >SİGORTA MÜŞTERİ LİSTESİ</h1>
						</div>
					</div>
				</div>

				<div class="row">
				

				
					<div class="col-md-12">
					

					
					<?php echo $this->session->flashdata('alert'); ?>
					
									<button class="btn btn-raised btn-success" data-toggle="modal" data-target="#myModal">Müşteri Ekle<div class="ripple-container"></div></button>

					
						<section class="boxs">
						
						
						
							<div class="boxs-header">
							</div>
							<div class="boxs-body">
							<form action="<?php echo base_url("yonetimpaneli/sigortamusteriara"); ?>" method="post">
								<div class="form-group">
									<label for="filter" style="margin-right: 20px">Arama:</label>
                                    <input id="filter" type="text" name="key" class="form-control rounded w-md mb-10 inline-block" placeholder="Müşteri adı soyadı">
                                     <div class="btn-group" style="margin-left: 50px">
                                                <button type="submit" class="btn btn-raised btn-success btn-sm"> <i class="fa fa-search"></i> </button>
                                            </div>
								</div> <br><br>
								</form>

							<?php if($veriler != null){ ?>
                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<thead>
										<tr>
											<th>Adı Soyadı</th>
                                            <th>Sigorta Şirketi</th>
											
                                            <th>İşlemler</th>
										  
                                           
										</tr>
									</thead>
									<tbody>
									<?php foreach($veriler as $yazdir) { $sigortasirketi = $this->sql->sigortafirmalistesinecek($yazdir->sigorta_sirketi);  ?>
										
										<tr>
                                            <td><?php echo $yazdir->musteri_adi . " " . $yazdir->musteri_soyadi; ?></td>
                                            <td><?php echo @$sigortasirketi->sigorta_adi; ?></td>
                                            
                                            <td> <a href="javascript:;" onclick="sil(<?php echo $yazdir->id; ?>)" class="label label-primary">Sil</a>  <a href="/homeguard/yonetimpaneli/sigortamusteriduzenle/<?php echo $yazdir->id; ?>" class="label label-danger">Düzenle</a> </td>

                                        </tr>
									<?php } ?>
									</tfoot>
								</table>
							<?php } else { ?>
								
								<h5>Aradığınız müşteri adı'yla ilgili kayıt bulunamadı.</h5>
							<?php } ?>
							</div>
						</section>
					</div>
				</div>
			</div>
		</section>
		
		
		<div class="modal fade" id="myModal" role="dialog" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Müşteri Ekleme Formu</h4>
                </div>
                <div class="modal-body">
                    <form class="profile-settings" method="POST" action="<?php echo base_url("yonetimpaneli/sigortamusterisiekle"); ?>">
                                                        <div class="row">
														
                                                            <div class="form-group col-md-12 legend">
															                                                                <h3>
                                                                    <strong>Sigorta Müşteri Ekleme</strong> Formu</h3>
                                                                <p>Sigorta Müşteri Ekleme işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
														
                                                            <div class="form-group col-sm-6 is-empty">
                                                                <label for="username">Müşteri Adı</label>
                                                                <input type="text" name="adi" class="form-control musteriadi" rows="5" id="username" placeholder=" ">
                                                            <span class="material-input"></span></div>
                                                            <div class="form-group col-sm-6 is-empty">
                                                                <label for="username">Müşteri Soyadı</label>
                                                                <input type="text" name="soyadi" class="form-control musterisoyadi" rows="5" id="username" placeholder=" ">
                                                            <span class="material-input"></span></div>
                                                            <div class="form-group col-sm-6 is-empty">
                                                                <label for="username">Email</label>
                                                                <input type="text" name="mail" class="form-control email" rows="5" id="username">
                                                            <span class="material-input"></span></div>
                                                             <div class="form-group col-sm-6 is-empty">
                                                                <label for="username">Şifre</label>
                                                                <input type="password" name="sifre" class="form-control sifre" rows="5" id="username">
                                                            <span class="material-input"></span></div>
                                                             <div class="form-group col-sm-12 is-empty">
                                                                <label for="ilce">Sigorta Şirketi</label>
                                                                <select name="sirket" class="form-control sigortasirketi">
																<option value="0" selected="" disabled="">Seçiniz</option>
                                                                																<option value="13">Anadolu Sigorta</option>
																																<option value="14">Güneş Sigorta</option>
																                                                                </select>
                                                            <span class="material-input"></span></div>
                                                           
                                                           
                                                            <div class="form-group col-sm-12">
                                                                <button class="btn btn-raised btn-primary">Müşterİ Ekle</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat<div class="ripple-container"><div class="ripple ripple-on ripple-out" style="left: 43.875px; top: 22px; background-color: rgba(0, 0, 0, 0.87); transform: scale(9.375);"></div><div class="ripple ripple-on ripple-out" style="left: 63.875px; top: 22px; background-color: rgba(0, 0, 0, 0.87); transform: scale(9.375);"></div></div></button>
                </div>
            </div>
        </div>
    </div>
		
		
		
		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		
		<script>
		
		function hizlibak(id){
			
			$.ajax({
			url:"/homeguard/yonetimpaneli/hizlibak1/"+id,
				type:"POST",
			    data:{id:id},
				success:function(r){
					$(".yaz").html(r);
				}
			});
			
		}
		
		
		function sil(id){
				
				if (confirm('Silmek istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/sigortamusterisil/"+id);
                } 
                else {
                console.log("no");
                }
				
			}
		
	
		
		</script>