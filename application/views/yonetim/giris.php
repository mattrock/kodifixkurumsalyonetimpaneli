<!doctype html>
<html class="no-js" lang="tr">

<head>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="icon" href="favicon.ico" type="image/x-icon">
  <title>Sigorta Sistemi Yönetim Panel</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url("assets/"); ?>assets\js\vendor\bootstrap\bootstrap.min.css">
  <!-- CSS Files -->
  <link href="<?php echo base_url("assets/"); ?>assets\css\main.css" rel="stylesheet">
</head>

<body id="falcon" class="authentication">
  <div class="wrapper">
    <div class="header header-filter" style="background-image: url('<?php echo base_url("assets/"); ?>assets/images/login-bg.jpg'); background-size: cover; background-position: top center;">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center">
            <div class="card card-signup">
              <form class="form" action="<?php echo base_url(); ?>/yonetim/checklogin/" method="POST">
                <div class="header header-primary text-center">
                  <h4>Sigorta Sistemi Yönetim Paneli</h4>
                </div>
                <h3 class="mt-0">Sisteme Giriş</h3>
				<?php echo validation_errors(); ?>
				<?php echo $this->session->flashdata('hata'); ?>
                <div class="content">
                  <div class="form-group">
                    <input type="text" name="username" class="form-control underline-input" placeholder="Email Adresiniz">
                  </div>
                  <div class="form-group">
                    <input type="password" name="password" placeholder="Şifre" class="form-control underline-input">
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="optionsCheckboxes"> Beni Hatırla</label>
                  </div>
                </div>
                <div class="footer text-center">
                  <button class="btn btn-info btn-raised">Giriş</button>
                </div>
                <a href="forgotpass.html" class="btn btn-wd">Şifremi Unuttum</a>
              </form>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer mt-20">
        <div class="container">
          <div class="col-lg-12 text-center">
            
            <div class="copyright text-white mt-20">
              
              <p><a href="javascript:;" target="_blank">Stark Reklam</a> © 2018</p>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--  Vendor JavaScripts -->
  <script src="<?php echo base_url("assets/"); ?>assets\bundles\libscripts.bundle.js"></script>
  <script src="<?php echo base_url("assets/"); ?>assets\bundles\mainscripts.bundle.js"></script>
  <!-- Custom Js -->
</body>
</html>