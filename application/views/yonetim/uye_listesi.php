	<!--  CONTENT  -->
		<section id="content">
			<div class="page page-tables-footable">
				<!-- bradcome -->
				<div class="b-b mb-10">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="h3 m-0">ÜYE LİSTESİ</h1>
						</div>
					</div>
				</div>

				<!-- row -->
				<div class="row">
					<div class="col-md-12">
					<?php echo $this->session->flashdata("alert"); ?>
						<section class="boxs ">
							<div class="boxs-header">
							</div>
							<div class="boxs-body">
								
                            
                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<thead>
										<tr>
											<th>Üye id</th>
                                            <th>Üye Ad Soyad</th>
											<th>Üye Email</th>
											<th>Üye Telefon</th>
                                            <th>Üye Rütbesi</th>
                                            <th>İşlemler</th>
										</tr>
									</thead>
									<tbody>
									
									
									<?php foreach($veriler as $yaz){ ?>
										<tr>
                                            <td><?php echo $yaz->uye_id; ?></td>
                                            <td><?php echo $yaz->uye_adsoyad; ?></td>
                                            <td><?php echo $yaz->uye_email; ?></td>
                                            <td><?php echo $yaz->uye_telefon; ?></td>
                                            <td><?php echo $yaz->uye_rank == 0 ? "Üye":"Yönetici"; ?></td>
                                            <td> <a href="javascript:;" onclick="sil(<?php echo $yaz->uye_id; ?>);" class="label label-primary">Sil</a>  <a href="/homeguard/yonetimpaneli/uyeduzenle/<?php echo $yaz->uye_id; ?>" class="label label-danger">Düzenle</a> <a href="javascript:;" data-toggle="modal" data-target="#myModal2" onclick="hizlibak(<?php echo $yaz->uye_id; ?>);" class="label label-default">Hızlı Bak</a> </td>
                                        </tr>
									<?php } ?>
                                        
                                     
                                       
                                        
									</tfoot>
								</table>
							</div>
							<div class="pagination">
							<?php echo $linkler; ?>
							</div>
						</section>
					</div>
				</div>
			</div>
		</section>
		
		<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Hızlı Bak</h4>
                </div>
                <div class="modal-body">
                    <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<thead>
										<tr>
											<th>Üye id</th>
                                            <th>Üye Ad Soyad</th>
											<th>Üye Email</th>
											<th>Üye Şifre</th>
											<th>Üye Telefon</th>
                                            <th>Üye Rütbesi</th>
                                            
										</tr>
									</thead>
									<tbody class="yaz">
									
									
									
										
									
                                        
                                     
                                       </tbody>
                                        
									</tfoot>
								</table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>
		
		<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		
		<script>
		
		function hizlibak(id){
			
			$.ajax({
			url:"/homeguard/yonetimpaneli/hizlibak/"+id,
				type:"POST",
			    data:{id:id},
				success:function(r){
					$(".yaz").html(r);
				}
			});
			
		}
		
		
		function sil(id){
				
				if (confirm('Silmek istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/uyesil/"+id);
                } 
                else {
                console.log("no");
                }
				
			}
		
	
		
		</script>
		
		<!--/ CONTENT -->