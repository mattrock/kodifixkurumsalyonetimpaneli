	<!--  CONTENT  -->
		<section id="content">
			<div class="page page-tables-footable">
				<!-- bradcome -->
				<div class="b-b mb-10">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="h3 m-0">MÜŞTERİ LİSTESİ</h1>
						</div>
					</div>
				</div>

				<!-- row -->
				<div class="row">
				<?php echo $this->session->flashdata('alert'); ?>
					<div class="col-md-12">
						<section class="boxs ">
							<div class="boxs-header">
							</div>
							<div class="boxs-body">
							<form action="<?php echo base_url("/yonetimpaneli/musteriarama"); ?>" method="POST">
								<div class="form-group">
									<label for="filter" style="margin-right:20px;">Arama:</label>
                                    <input id="filter" type="text" name="key" placeholder="tc no veya police no giriniz." class="searchbox form-control rounded w-md mb-10 inline-block">
                                     <div class="btn-group" style="margin-left: 50px">
                                                <button type="submit" class="btn btn-raised btn-success btn-sm"> <i class="fa fa-search"></i> </button>
                                            </div>
								</div> <br><br>
								</form>
								
								
								<?php if($musterilistesi != null){ ?>

                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<thead>
										<tr>
											<th>Ad-Soyad</th>
                                            <th>TC Kimlik No</th>
											<th>Poliçe No</th>
											<th>Sigorta Şirketi</th>
                                            <th>E-Mail</th>
                                            <th>Telefon</th>
                                            
                                            <th>İşlemler</th>
										</tr>
									</thead>
									<tbody>
									
									
										
										
										<?php foreach($musterilistesi as $yaz){ $sigortasirketi = $this->sql->sigortafirmalistesinecek($yaz->sigorta_sirketi); ?>
                                        
		                                
										
										<tr>
                                            <td><?php echo $yaz->musteri_adi . " ". $yaz->musteri_soyadi; ?></td>
                                            <td><?php echo $yaz->tc_kimlik_no; ?></td>
                                            <td><?php echo $yaz->police_no; ?></td>
                                            <td><?php echo @$sigortasirketi->sigorta_adi; ?></td>
                                            <td><?php echo $yaz->mail; ?></td>
                                            <td><?php echo $yaz->telefon; ?></td>
                                            
                                            <td> <a href="javascript:;" onclick="sil(<?php echo $yaz->id; ?>);" class="label label-primary">Sil</a>  <a href="/homeguard/yonetimpaneli/musteriduzenle/<?php echo $yaz->id; ?>" class="label label-danger">Düzenle</a> <a href="javascript:;" data-toggle="modal" data-target="#myModal2" onclick="hizlibak(<?php echo $yaz->id; ?>);" class="label label-default">Hızlı Bak</a> </td>

                                        </tr>
                                        <?php } ?>
									</tfoot>
								</table>
								<?php } else { ?>
								
								<h5>Aradığınız tc no'ya veya poliçe no'ya ait kayıt bulunamadı.</h5>
								<?php } ?>
							</div>
							<div class="pagination">
							<?php echo $linkler; ?>
							</div>
						</section>
					</div>
				</div>
				
			</div>
			
		</section>
		
		<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Hızlı Bak</h4>
                </div>
                <div class="modal-body">
                    <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<thead>
										<tr>
											<th>Ad Soyad</th>
											<th>Tc Kimlik No</th>
											<th>Mail</th>
											
											<th>Telefon</th>
											<th>Sigorta Şirketi</th>
											<th>il</th>
											<th>ilçe</th>
											<th>Poliçe No</th>
											<th>adres</th>
											
                                            
										</tr>
									</thead>
									<tbody class="yaz">
									
									
									
										
									
                                        
                                     
                                       </tbody>
                                        
									</tfoot>
								</table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>
	
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
		
		<script>
		
		function hizlibak(id){
			
			$.ajax({
			url:"/homeguard/yonetimpaneli/hizlibak3/"+id,
				type:"POST",
			    data:{id:id},
				success:function(r){
					$(".yaz").html(r);
				}
			});
			
		}
		
		
		function sil(id){
				
				if (confirm('Silmek istediğinize emin misiniz ?')) {
                    location.replace("/homeguard/yonetimpaneli/musterisilen/"+id);
                } 
                else {
                console.log("no");
                }
				
			}
			
			
			$(document).ready(function(){
				
				$("form").submit(function(){
					
					var search = $(".searchbox").val();
					
					if(search == ""){
						alert("Boş olamaz.");
						return false;
					}
					
				});
				
			});
		
	
		
		</script>