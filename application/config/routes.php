<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'anasayfa';
$route['yonetimpaneli/m/(:any)'] = 'yonetimpaneli/index/$1';
$route['urunara'] = 'anasayfa/urunara';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
