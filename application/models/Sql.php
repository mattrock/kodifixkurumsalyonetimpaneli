<?php

class sql extends CI_Model{
	
	function logincheck($username,$password){
		$result = $this->db->select("*")
		->from("uyeler")
		->where("uye_email",$username)
		->where("uye_sifre",$password)
		->get()
		->row();
		return $result;
	}
	
	function mustericheck($username,$password){
		$result = $this->db->select("*")
		->from("musteriler")
		->where("mail",$username)
		->where("sifre",$password)
		->get()
		->row();
		return $result;
	}
	
	function sigortacheck($username,$password){
		$result = $this->db->select("*")
		->from("sigortafirmalistesi")
		->where("mail",$username)
		->where("sifre",$password)
		->get()
		->row();
		return $result;
	}
	
	function toplamuyesayisi(){
		$result = $this->db->select("*")
		->from("uyeler")
		->get()
		->result();
		return count($result);
	}
	
	function uyeceklistele($limit, $start)   
	{
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("uyeler"); 
		return $veriler->result();
	}
	
	function uyesil($id){
		$result = $this->db->delete("uyeler",array("uye_id" => $id));
		return $result;
	}
	
    function sigortafirmasil($id){
		$result = $this->db->delete("sigortafirmalistesi",array("id" => $id));
		return $result;
	}
	
	 function firmasil($id){
		$result = $this->db->delete("firmalar",array("id" => $id));
		return $result;
	}
	
	function idiyegorefirma($id){
		$result = $this->db->select("*")
		->from("firmalar")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	function idiyegorefirmaupdate($id,$data){
		$this->db->where("id",$id);
		$result = $this->db->update("firmalar",$data);
		return $result;
	}
	
	function firmaaracount($id){
		$this->db->like('firma_ismi',$id);
		$result = $this->db->select("*")
		->from("firmalar")
		->get()
		->result();
		return count($result);
	}
	
	function firmaaralist($limit,$start,$id){
		$result = $this->db->query("SELECT * FROM firmalar WHERE firma_ismi LIKE '%{$id}' LIMIT {$start},{$limit}  ");
		return $result->result();
	}
	
	function firmalistcek(){
		$result = $this->db->select("id,firma_ismi")
		->from("firmalar")
		->get()
		->result();
		return $result;
	}
	
	function hizlibak($id){
		$result = $this->db->select("*")
		->from("uyeler")
		->where("uye_id",$id)
		->get()
		->row();
		return $result;
	}
	
	function hizlibak3($id){
		$result = $this->db->select("*")
		->from("musteriler")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	function uyeduzenle($id,$data){
		$this->db->where("uye_id",$id);
		$result = $this->db->update("uyeler",$data);
		return $result;
	}
	
	function uyeekle($data){
		$result = $this->db->insert("uyeler",$data);
		return $result;
	}
	
	function musteriekle($data){
		
		$result = $this->db->insert("sigortafirmalari",$data);
		return $result;
		
		
	}
	function toplamfirma(){
		$result = $this->db->select("*")
		->from("sigortafirmalistesi")
		->get()
		->result();
		return count($result);
	}
	
	function sigortafirmaramacount($id){
		
		$this->db->like('sigorta_adi', $id);
		$result = $this->db->select("*")
		->from("sigortafirmalistesi")
		->get()
		->result();
		return count($result);
	}
	
		function firmalistele($limit, $start)   
	{
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("sigortafirmalistesi"); 
		return $veriler->result();
	}
	
	
	function sigortafirmarama($limit, $start,$key)   
	{
		$this->db->like('sigorta_adi', $key);
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("sigortafirmalistesi"); 
		return $veriler->result();
	}

	
		function sigortamusterisiekle($data){
		
		$result = $this->db->insert("sigortamusterileri",$data);
		return $result;
		
		
	    }
		
		function sigortamusterisicek($id){
		 $result = $this->db->select("*")
		 ->from("sigortamusterileri")
		 ->where("id",$id)
		 ->get()
		 ->row();
		 return $result;
		}
		
		function sigortamusteriduzenle($id,$data){
			$this->db->where("id",$id);
			$result = $this->db->update("sigortamusterileri",$data);
			return $result;
		}

	function hizlibak1($id){
		$result = $this->db->select("*")
		->from("sigortafirmalistesi")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	
	function musteriilistesi($limit, $start)   
	{
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("sigortamusterileri"); 
		return $veriler->result();
	}
	
	function sigortamusterilist($limit, $start,$id)   
	{
		 
		$veriler = $this->db->query("SELECT * FROM `sigortamusterileri` WHERE musteri_adi LIKE '%".$id."' UNION SELECT * FROM `sigortamusterileri` WHERE musteri_soyadi LIKE '%".$id."%' COLLATE utf8_turkish_ci ");
		
		return $veriler->result();
	}
	
	
	function sigortamustericount($id)   
	{
		 
		$veriler = $this->db->query("SELECT * FROM `sigortamusterileri` WHERE musteri_adi LIKE '%".$id."' UNION SELECT * FROM `sigortamusterileri` WHERE musteri_soyadi LIKE '%".$id."%' COLLATE utf8_turkish_ci ");
		
		return count($veriler->result());
	}
	
	function musterimiz($limit, $start)   
	{
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("musteriler");		
		return $veriler->result();
	}
	
	function musteriaramalist($limit, $start,$id){
		$this->db->where("tc_kimlik_no",$id);
		$this->db->or_where("police_no",$id);
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("musteriler");		
		return $veriler->result();
	}
	
	
	function toplamusterii(){
		$result = $this->db->select("*")
		->from("sigortamusterileri")
		->get()
		->result();
		return count($result);
	}
	
	function iller(){
		$result = $this->db->select("*")
		->from("iller")
		->get()
		->result();
		return $result;
	}
	
	function ilce($il){
		$result = $this->db->select("*")
		->from("ilceler")
		->where("il_no",$il)
		->get()
		->result();
		return $result;
	}
	
	
	
	function sigortafirmasiduzenle($id,$data){
		$this->db->where("id",$id);
		$result = $this->db->update("sigortafirmalistesi",$data);
		return $result;
	}
	
	function sigortasirketicek(){
		$result = $this->db->select("sigorta_sirketi")
		->from("sigortafirmalistesi")
		->get()
		->result();
		return $result;
	}
	
	function sigortasirketlistesinekle($data){
		$this->db->insert("sigortafirmalistesi",$data);
		$result = $this->db->insert_id();
		return $result;
	}
	
	function sigortafirmalistesinecek($id){
		$result = $this->db->select("*")
		->from("sigortafirmalistesi")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	function iladicek($id){
		$result = $this->db->select("*")
		->from("iller")
		->where("il_no",$id)
		->get()
		->row();
		return $result;
	}
	
	function ilceadicek($id){
		$result = $this->db->select("*")
		->from("ilceler")
		->where("ilce_no",$id)
		->get()
		->row();
		return $result;
	}
	
	function sigortafirmalistesicek(){
		$result = $this->db->select("*")
		->from("sigortafirmalistesi")
		->get()
		->result();
		return $result;
	}
	
	function sigortamusterisil($id){
		$result = $this->db->delete("sigortamusterileri",array("id" => $id));
		return $result;
	}
	
    function musteriekledb($data){
		$result = $this->db->insert("musteriler",$data);
		return $result;
	}
	
	function musterlistesicek(){
		$result = $this->db->select("*")
		->from("musteriler")
		->get()
		->result();
		return count($result);
	}
	
	function musteriaramacek($id){
		$result = $this->db->select("*")
		->from("musteriler")
		->where("tc_kimlik_no",$id)
		->or_where("police_no",$id)
		->get()
		->result();
		return count($result);
	}
	
	
	function musteriuyecek($id){
		$result = $this->db->select("*")
		->from("musteriler")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	function musteriduzenledb($id,$data){
		$this->db->where("id",$id);
		$result = $this->db->update("musteriler",$data);
		return $result;
	}
	
	function musterisilen($id){
	$result = $this->db->delete("musteriler",array("id" => $id));
	return $result;
}

   function alarmrandevuara($id){
	   $result = $this->db->select("*")
	   ->from("musteriler")
	   ->where("tc_kimlik_no",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function alarmrandevuekle($data){
	   $result = $this->db->insert("alarmrandevular",$data);
	   return $result;
   }
   
   function nakliyatrandevuekle($data){
	   $this->db->insert("nakliyatrandevular",$data);
	   $result = $this->db->insert_id();
	   return $result;
   }
   
    function nakliyatdevam($data){
	   $result = $this->db->insert("nakliyatdevam",$data);
	   return $result;
   }
   
   function montajrandevuekle($data){
	   $result = $this->db->insert("montajrandevular",$data);
	   return $result;
   }
   
   function haliyikamarandevuekle($data){
	   $result = $this->db->insert("haliyikamarandevular",$data);
	   return $result;
   }
   
    function piskomaxrandevu($data){
	   $result = $this->db->insert("piskomaxrandevular",$data);
	   return $result;
   }
   
   function hasarerandevu($data){
	   $result = $this->db->insert("hasarerandevular",$data);
	   return $result;
   }
   
   function alarmrandevutoplam(){
	   $result = $this->db->select("*")
	   ->from("alarmrandevular")
	   ->get()
	   ->result();
	   return count($result);
   }
   
   function tumrandevulartoplamarama($id){
	   
	   $this->db->like('tckimlikno',$id);
	   $this->db->or_like('policeno',$id);
	   $result1 = $this->db->select("*")
	   ->from("alarmrandevular")
	   ->get()
	   ->result();
	   
	   
	   $this->db->like('tckimlikno',$id);
	   $this->db->or_like('policeno',$id);
	   $result2 = $this->db->select("*")
	   ->from("haliyikamarandevular")
	   ->get()
	   ->result();
	   
	   $this->db->like('tckimlikno',$id);
	   $this->db->or_like('policeno',$id);
	   $result3 = $this->db->select("*")
	   ->from("hasarerandevular")
	   ->get()
	   ->result();
	   
	   $this->db->like('tckimlikno',$id);
	   $this->db->or_like('policeno',$id);
	   $result4 = $this->db->select("*")
	   ->from("montajrandevular")
	   ->get()
	   ->result();
	   
	   $this->db->like('tckimlikno',$id);
	   $this->db->or_like('policeno',$id);
	   $result5 = $this->db->select("*")
	   ->from("nakliyatrandevular")
	   ->get()
	   ->result();
	   
	   $this->db->like('tckimlikno',$id);
	   $this->db->or_like('policeno',$id);
	   $result6 = $this->db->select("*")
	   ->from("piskomaxrandevular")
	   ->get()
	   ->result();
	  
	   $allcount = count($result1) + count($result2) + count($result3) + count($result4) + count($result5) + count($result6);
	   return $allcount;
	   
   }
   
   function alarmrandevutoplam1($id){
	   $result = $this->db->select("*")
	   ->from("alarmrandevular")
	   ->where("musteri_id",$id)
	   ->get()
	   ->result();
	   return count($result);
   }
   
   function montajrandevutoplam(){
	   $result = $this->db->select("*")
	   ->from("montajrandevular")
	   ->get()
	   ->result();
	   return count($result);
   }
   
   function montajrandevutoplam1($id){
	   $result = $this->db->select("*")
	   ->from("montajrandevular")
	   ->where("musteri_id",$id)
	   ->get()
	   ->result();
	   return count($result);
   }
   
   function haliyikamarandevutoplam(){
	   $result = $this->db->select("*")
	   ->from("haliyikamarandevular")
	   ->get()
	   ->result();
	   return count($result);
   }
   
   function haliyikamarandevutoplam1($id){
	   $result = $this->db->select("*")
	   ->from("haliyikamarandevular")
	   ->where("musteri_id",$id)
	   ->get()
	   ->result();
	   return count($result);
   }
   
   function hasaretoplam(){
	   $result = $this->db->select("*")
	   ->from("hasarerandevular")
	   ->get()
	   ->result();
	   return count($result);
   }
   
   function hasaretoplam1($id){
	   $result = $this->db->select("*")
	   ->from("hasarerandevular")
	   ->where("musteri_id",$id)
	   ->get()
	   ->result();
	   return count($result);
   }
   
   function psikomaxtoplam(){
	   $result = $this->db->select("*")
	   ->from("piskomaxrandevular")
	   ->get()
	   ->result();
	   return count($result);
   }
   
   
   function musterimailgetir(){
	   $result = $this->db->select("*")
	   ->from("musteriler")
	   ->get()
	   ->result();
	   return $result;
   }
   
   
   function psikomaxtoplam1($id){
	   $result = $this->db->select("*")
	   ->from("piskomaxrandevular")
	   ->where("musteri_id",$id)
	   ->get()
	   ->result();
	   return count($result);
   }
   
    function nakliyatrandevutoplam(){
	   $result = $this->db->select("*")
	   ->from("nakliyatrandevular")
	   ->get()
	   ->result();
	   return count($result);
   }
   
   function nakliyatrandevutoplam1($id){
	   $result = $this->db->select("*")
	   ->from("nakliyatrandevular")
	   ->where("musteri_id",$id)
	   ->get()
	   ->result();
	   return count($result);
   }
   
   function toplammusteri(){
	   $result = $this->db->select("*")
	   ->from("musteriler")
	   ->get()
	   ->result();
	   
	   
	   $result1 = $this->db->select("*")
	   ->from("sigortamusterileri")
	   ->get()
	   ->result();
	   
	   return count($result) + count($result1);
	   
   }
   
   function musterianalitik(){
	   $result = $this->db->query("SELECT * FROM musteriler ORDER BY id DESC LIMIT 0,10");
	   
	   return $result->result();
   }
   
   
   function alarmrandevupage($limit, $start)   
	{
		$this->db->order_by("onay ASC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("alarmrandevular"); 
		return $veriler->result();
	}
	
	function tumrandevuarama($limit, $start,$id)   
	{
		$this->db->like('tckimlikno',$id);
	    $this->db->or_like('policeno',$id);
		$this->db->limit($limit, $start); 
		$result1 = $this->db->get("alarmrandevular"); 
		$result1->result();
		
		$this->db->like('tckimlikno',$id);
	    $this->db->or_like('policeno',$id);
		$this->db->limit($limit, $start); 
		$result2 = $this->db->get("haliyikamarandevular"); 
		$result2->result();
		
		$this->db->like('tckimlikno',$id);
	    $this->db->or_like('policeno',$id);
		$this->db->limit($limit, $start); 
		$result3 = $this->db->get("hasarerandevular"); 
		$result3->result();
		
		$this->db->like('tckimlikno',$id);
	    $this->db->or_like('policeno',$id);
		$this->db->limit($limit, $start); 
		$result4 = $this->db->get("montajrandevular"); 
		$result4->result();
		
		$this->db->like('tckimlikno',$id);
	    $this->db->or_like('policeno',$id);
		$this->db->limit($limit, $start); 
		$result5 = $this->db->get("nakliyatrandevular"); 
		$result5->result();
		
		$this->db->like('tckimlikno',$id);
	    $this->db->or_like('policeno',$id);
		$this->db->limit($limit, $start); 
		$result6 = $this->db->get("piskomaxrandevular"); 
		$result6->result();
		
		
		$son = array_merge($result1->result(),$result2->result(),$result3->result(),$result4->result(),$result5->result(),$result6->result());
		return $son;
	}
	
	function alarmrandevupage1($limit, $start,$id)   
	{
		$this->db->where("musteri_id",$id);
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("alarmrandevular"); 
		return $veriler->result();
	}
	
	function psikomaxrandevupage($limit, $start)   
	{
		$this->db->order_by("onay ASC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("piskomaxrandevular"); 
		return $veriler->result();
	}
	
	function psikomaxrandevupage1($limit, $start,$id)   
	{
		$this->db->where("musteri_id",$id);
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("piskomaxrandevular"); 
		return $veriler->result();
	}
	
	function montajrandevupage($limit, $start)   
	{
		$this->db->order_by("onay ASC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("montajrandevular"); 
		return $veriler->result();
	}
	
	function montajrandevupage1($limit, $start,$id)   
	{
		$this->db->where("musteri_id",$id);
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("montajrandevular"); 
		return $veriler->result();
	}
	
	function haliyikamarandevupage($limit, $start)   
	{
		$this->db->order_by("onay ASC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("haliyikamarandevular"); 
		return $veriler->result();
	}
	
	function haliyikamarandevupage1($limit, $start,$id)   
	{
		$this->db->where("musteri_id",$id);
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("haliyikamarandevular"); 
		return $veriler->result();
	}
	
	function hasaretrandevupage($limit, $start)   
	{
		$this->db->order_by("onay ASC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("hasarerandevular"); 
		return $veriler->result();
	}
	
	function hasaretrandevupage1($limit, $start,$id)   
	{
		$this->db->where("musteri_id",$id);
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("hasarerandevular"); 
		return $veriler->result();
	}
	
	function nakliyatrandevupage($limit, $start)   
	{
		$this->db->order_by("onay ASC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("nakliyatrandevular"); 
		return $veriler->result();
	}
	
	function nakliyatrandevupage1($limit, $start,$id)   
	{
		$this->db->where("musteri_id",$id);
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("nakliyatrandevular"); 
		return $veriler->result();
	}
   
   function alarmbak($id){
	   $result = $this->db->select("*")
	   ->from("alarmrandevular")
	   ->where("id",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function nakliyatbak($id){
	   $result = $this->db->select("*")
	   ->from("nakliyatrandevular")
	   ->where("id",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function nakliyatbak1($id){
	   $result = $this->db->select("*")
	   ->from("nakliyatdevam")
	   ->where("nakliyat_id",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function psikomaxrandevubak($id){
	   $result = $this->db->select("*")
	   ->from("piskomaxrandevular")
	   ->where("id",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function haliyikamabak($id){
	   $result = $this->db->select("*")
	   ->from("haliyikamarandevular")
	   ->where("id",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function montajrandevubak($id){
	   $result = $this->db->select("*")
	   ->from("montajrandevular")
	   ->where("id",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function hasaretbak($id){
	   $result = $this->db->select("*")
	   ->from("hasarerandevular")
	   ->where("id",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function alarmrandevusil($id){
	   $result = $this->db->delete("alarmrandevular",array("id" => $id));
	   return $result;
   }
   
   function haliyikamarandevusil($id){
	   $result = $this->db->delete("haliyikamarandevular",array("id" => $id));
	   return $result;
   }
   
   function hasaretrandevusil($id){
	   $result = $this->db->delete("hasarerandevular",array("id" => $id));
	   return $result;
   }
   
   function montajrandevusil($id){
	   $result = $this->db->delete("montajrandevular",array("id" => $id));
	   return $result;
   }
   
   function nakliyatrandevusil($id){
	   $result = $this->db->delete("nakliyatrandevular",array("id" => $id));
	   return $result;
   }
   
   function psikomaxrandevusil($id){
	   $result = $this->db->delete("piskomaxrandevular",array("id" => $id));
	   return $result;
   }
   
   function hesapolustur($data){
	   $result = $this->db->insert("musteriler",$data);
	   return $result;
   }
   
   function onayrandevualarm($id){
	   $this->db->where("id",$id);
	   $result = $this->db->update("alarmrandevular",array("onay" => 1));
	   return $result;
   }
   
   function onayrandevuhaliyikama($id){
	   $this->db->where("id",$id);
	   $result = $this->db->update("haliyikamarandevular",array("onay" => 1));
	   return $result;
   }
   
   function hasaretrandevu($id){
	   $this->db->where("id",$id);
	   $result = $this->db->update("hasarerandevular",array("onay" => 1));
	   return $result;
   }
   
    function onaymontajrandevu($id){
	   $this->db->where("id",$id);
	   $result = $this->db->update("montajrandevular",array("onay" => 1));
	   return $result;
   }
   
   function onaynakliyatrandevu($id){
	   $this->db->where("id",$id);
	   $result = $this->db->update("nakliyatrandevular",array("onay" => 1));
	   return $result;
   }
   
   function onaypsikomaxrandevu($id){
	   $this->db->where("id",$id);
	   $result = $this->db->update("piskomaxrandevular",array("onay" => 1));
	   return $result;
   }
   
   function alarmrandevuceklist($id){
	   $result = $this->db->select("*")
	   ->from("alarmrandevular")
	   ->where("id",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function piskomaxrandevuceklist($id){
	   $result = $this->db->select("*")
	   ->from("piskomaxrandevular")
	   ->where("id",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function nakliyatrandevuceklist($id){
	   $result = $this->db->select("*")
	   ->from("nakliyatrandevular")
	   ->where("id",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function nakliyatrandevuceklist1($id){
	   $result = $this->db->select("*")
	   ->from("nakliyatdevam")
	   ->where("nakliyat_id",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function haliyikamarandevuceklist($id){
	   $result = $this->db->select("*")
	   ->from("haliyikamarandevular")
	   ->where("id",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function hasarerandevurandevuceklist($id){
	   $result = $this->db->select("*")
	   ->from("hasarerandevular")
	   ->where("id",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function montajrandevurandevuceklist($id){
	   $result = $this->db->select("*")
	   ->from("montajrandevular")
	   ->where("id",$id)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function alarmduzenleupdate($id,$data){
	   $this->db->where("id",$id);
	   $result = $this->db->update("alarmrandevular",$data);
	   return $result;
   }
   
   function psikomaxduzenleupdate($id,$data){
	   $this->db->where("id",$id);
	   $result = $this->db->update("piskomaxrandevular",$data);
	   return $result;
   }
   
   function nakliyatduzenleupdate($id,$data){
	   $this->db->where("id",$id);
	   $result = $this->db->update("nakliyatrandevular",$data);
	   return $result;
   }
   
   function nakliyatduzenleupdate1($id,$data){
	   $this->db->where("nakliyat_id",$id);
	   $result = $this->db->update("nakliyatdevam",$data);
	   return $result;
   }
   
   function montajduzenleupdate($id,$data){
	   $this->db->where("id",$id);
	   $result = $this->db->update("montajrandevular",$data);
	   return $result;
   }
   
   function haliyikamaduzenleupdate($id,$data){
	   $this->db->where("id",$id);
	   $result = $this->db->update("haliyikamarandevular",$data);
	   return $result;
   }
   
   function hasarerandevupdate($id,$data){
	   $this->db->where("id",$id);
	   $result = $this->db->update("hasarerandevular",$data);
	   return $result;
   }
   
   
   
   function resimekle($data){
	   $result = $this->db->insert("resimler",$data);
	   return $result;
   }
   
   function resimcek($id){
	   $result = $this->db->select("*")
	   ->from("resimler")
	   ->where("gelenid",$id)
	   ->get()
	   ->result();
	   return $result;
   }
   
   function resimupdate($id,$data){
	   $this->db->where("id",$id);
	   $result = $this->db->update("resimler",$data);
	   return $result;
   }
   
   function aylikrandevusayisi($d1,$d2){
	   
	   $result1 = $this->db->select("*")
	   ->from("alarmrandevular")
	   ->where("kurulum_tarih >=",$d1)
	   ->where("kurulum_tarih <=",$d2)
	   ->get()
	   ->result();
	   
	   $result2 = $this->db->select("*")
	   ->from("haliyikamarandevular")
	   ->where("kurulum_tarih >=",$d1)
	   ->where("kurulum_tarih <=",$d2)
	   ->get()
	   ->result();
	   
	   $result3 = $this->db->select("*")
	   ->from("hasarerandevular")
	   ->where("kurulum_tarih >=",$d1)
	   ->where("kurulum_tarih <=",$d2)
	   ->get()
	   ->result();
	   
	   $result4 = $this->db->select("*")
	   ->from("montajrandevular")
	   ->where("kurulum_tarih >=",$d1)
	   ->where("kurulum_tarih <=",$d2)
	   ->get()
	   ->result();
	   
	   $result5 = $this->db->select("*")
	   ->from("nakliyatrandevular")
	   ->where("kurulum_tarih >=",$d1)
	   ->where("kurulum_tarih <=",$d2)
	   ->get()
	   ->result();
	   
	   $result6 = $this->db->select("*")
	   ->from("piskomaxrandevular")
	   ->where("kurulum_tarih >=",$d1)
	   ->where("kurulum_tarih <=",$d2)
	   ->get()
	   ->result();
	   
	  return count($result1) + count($result2) + count($result3) + count($result4) + count($result5);
	   
   }
   
   function anketekle($data){
	   $result = $this->db->insert("anketler",$data);
	   return $result;
   }
   
   function anketlistesitoplam(){
	   $result = $this->db->select("*")
	   ->from("anketler")
	   ->get()
	   ->result();
	   return count($result);
   }
   
   function anketrandevupage($limit, $start)   
	{
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("anketler"); 
		return $veriler->result();
	}
	
	function anketsil($id){
		$result = $this->db->delete("anketler",array("id" => $id));
		return $result;
	}
	
	function toplammusterisayisi(){
		$result = $this->db->select("*")
		->from("musteriler")
		->get()
		->result();
		
		$result1 = $this->db->select("*")
		->from("sigortamusterileri")
		->get()
		->result();
		
		return count($result) + count($result1);
	}
	
	function toplamsigortamusterisayisi(){

		$result = $this->db->select("*")
		->from("sigortamusterileri")
		->get()
		->result();
		
		return count($result);
	}
	
	function totalrandevu(){
		
		$result = $this->db->select("*")
		->from("alarmrandevular")
		->get()
		->result();
		
		$result1 = $this->db->select("*")
		->from("haliyikamarandevular")
		->get()
		->result();
		
		$result2 = $this->db->select("*")
		->from("hasarerandevular")
		->get()
		->result();
		
		$result3 = $this->db->select("*")
		->from("montajrandevular")
		->get()
		->result();
		
		$result4 = $this->db->select("*")
		->from("nakliyatrandevular")
		->get()
		->result();
		
		$result5 = $this->db->select("*")
		->from("piskomaxrandevular")
		->get()
		->result();
		
		return count($result) + count($result1) + count($result2) + count($result3) + count($result4) + count($result5);
	}


	function randevuresimsil($id){
		$result = $this->db->delete("resimler",array("id" => $id));
		return $result;
	}
	
	
	function alarmgetir($id){
		$result = $this->db->select("*")
		->from("alarmrandevular")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	function haligetir($id){
		$result = $this->db->select("*")
		->from("haliyikamarandevular")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	function hasaretgetir($id){
		$result = $this->db->select("*")
		->from("hasarerandevular")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	function montajgetir($id){
		$result = $this->db->select("*")
		->from("montajrandevular")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	function nakliyatgetir($id){
		$result = $this->db->select("*")
		->from("nakliyatrandevular")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	function psikomaxgetir($id){
		$result = $this->db->select("*")
		->from("piskomaxrandevular")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	function toplamsigortamusterittt($id){
		$result = $this->db->select("*")
		->from("sigortamusterileri")
		->where("sigorta_sirketi",$id)
		->get()
		->result();
		return count($result);
	}
	
	function toplamsigortamusteriliste($limit, $start,$id)   
	{
		$this->db->where("sigorta_sirketi",$id);
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("sigortamusterileri"); 
		return $veriler->result();
	}
	
	
	
	function anketcek(){
		$result = $this->db->select("*")
		->from("anketler")
		->get()
		->result();
		return $result;
	}
	
	function anketsonucekle($data,$id){
		$this->db->where("id",$id);
		$result = $this->db->update("anketler",$data);
		return $result;
	}
	
	function anketsonucek($id){
		$result = $this->db->select("*")
		->from("anketler")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	
	
	function anketlercek(){
		$result = $this->db->select("*")
		->from("anketler")
		->get()
		->result();
		return $result;
	}
	
	function urunterm($id){
		$result = $this->db->query("SELECT * FROM iller WHERE isim LIKE '$id%' ");
		return $result->result();
	}
	
	function firmalistesi(){
		$result = $this->db->select("*")
		->from("firmalar")
		->get()
		->result();
		return $result;
	}
	
	function firmalistesicount(){
		$result = $this->db->select("*")
		->from("firmalar")
		->get()
		->result();
		return count($result);
	}
	
	function firmaekle($data){
		$result = $this->db->insert("firmalar",$data);
		return $result;
	}
	
	function anketsorugetir($id){
		$result = $this->db->select("*")
		->from("anketler")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	function anketguncelle($data){
		$result = $this->db->insert("sorutipi1",$data);
		return $result;
	}
	
	function anketguncelle1($data){
		$result = $this->db->insert("sorutipi2",$data);
		return $result;
	}
	
	function degerara($id,$email){
		
		$result = $this->db->select("*")
		->from("sorutipi2")
		->where("anket_id",$id)
		->where("musteri_mail",$email)
		->get()
		->row();
		
		return $result;
	}
	
	function degerara1($id,$email){
		
		$result = $this->db->select("*")
		->from("sorutipi1")
		->where("anket_id",$id)
		->where("musteri_mail",$email)
		->get()
		->row();
		
		return $result;
	}
    
	function sorucek1($id){
		$result = $this->db->select("*")
		->from("sorutipi1")
		->where("anket_id",$id)
		->get()
		->result();
		return $result;
	}
	
	function sorucek2($id){
		$result = $this->db->select("*")
		->from("sorutipi2")
		->where("anket_id",$id)
		->get()
		->result();
		return $result;
	}
	
	function toplamanketsay($id,$email){
		 $this->db->group_by('musteri_mail'); 
		$result = $this->db->select("*")
		->from("sorutipi1")
		->where("anket_id",$id)
		->where("musteri_mail",$email)
		->get()
		->result();
		return count($result);
	}
	
	function toplamanketsay1($id,$email){
		$this->db->group_by('musteri_mail');
		$result = $this->db->select("*")
		->from("sorutipi2")
		->where("anket_id",$id)
		->where("musteri_mail",$email)
		->get()
		->result();
		return count($result);
	}
   
   
   function uyekontrol($mail){
	   $result = $this->db->select("*")
	   ->from("uyeler")
	   ->where("uye_email",$mail)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function musterikontrol($mail,$tc){
	   $result = $this->db->select("*")
	   ->from("musteriler")
	   ->where("mail",$mail)
	   ->or_where("tc_kimlik_no",$tc)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function sigortamustericheck($mail){
	   $result = $this->db->select("*")
	   ->from("sigortamusterileri")
	   ->where("mail",$mail)
	   ->get()
	   ->row();
	   return $result;
   }
   
   function sigortafirmacheck($mail){
	   $result = $this->db->select("*")
	   ->from("firmalar")
	   ->where("mail",$mail)
	   ->get()
	   ->row();
	   return $result;
   }
   
   
   
   
   
   
 
   
   
   
	
	 
	
	
}